-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 09, 2020 at 03:47 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jaya`
--

-- --------------------------------------------------------

--
-- Table structure for table `tabel_agent`
--

CREATE TABLE `tabel_agent` (
  `id_agent` int(200) NOT NULL,
  `user` varchar(64) NOT NULL,
  `tgl` varchar(64) NOT NULL,
  `browser` varchar(128) NOT NULL,
  `os` varchar(64) NOT NULL,
  `ip` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tabel_agent`
--

INSERT INTO `tabel_agent` (`id_agent`, `user`, `tgl`, `browser`, `os`, `ip`) VALUES
(2, 'manajer', '2020-06-09 04:27:43', 'Chrome 83.0.4103.61', 'Windows 10', '::1');

-- --------------------------------------------------------

--
-- Table structure for table `tabel_bank`
--

CREATE TABLE `tabel_bank` (
  `id_bank` int(11) NOT NULL,
  `nm_bank` varchar(25) NOT NULL,
  `no_rek` varchar(35) NOT NULL,
  `ket` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tabel_bank`
--

INSERT INTO `tabel_bank` (`id_bank`, `nm_bank`, `no_rek`, `ket`) VALUES
(1, 'BCA', '1', '1'),
(2, 'Mandiri', '2', '2'),
(3, 'BNI', '3', '3'),
(4, 'BRI', '4', '4');

-- --------------------------------------------------------

--
-- Table structure for table `tabel_barang`
--

CREATE TABLE `tabel_barang` (
  `kd_barang` varchar(15) NOT NULL,
  `nm_barang` varchar(64) NOT NULL,
  `kd_satuan` varchar(10) NOT NULL,
  `kd_kategori` varchar(10) NOT NULL,
  `kd_supplier` varchar(16) NOT NULL,
  `hrg_jual` int(11) NOT NULL,
  `hrg_beli` int(11) NOT NULL,
  `kode_virtual` varchar(16) NOT NULL,
  `estimasi_stok` int(4) NOT NULL,
  `modal_per_porsi` int(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tabel_barang`
--

INSERT INTO `tabel_barang` (`kd_barang`, `nm_barang`, `kd_satuan`, `kd_kategori`, `kd_supplier`, `hrg_jual`, `hrg_beli`, `kode_virtual`, `estimasi_stok`, `modal_per_porsi`) VALUES
('T001', 'Tutup Pentil', 'S005', 'K003', '', 0, 80000, '', 1, 80000),
('T002', 'Karet', 'S007', 'K001', '', 0, 35000, '', 60, 583),
('T003', 'Tubless Set', 'S007', 'K002', '', 0, 65000, '', 1, 65000),
('T006', 'bukaan pentil', 'S005', 'K002', '', 0, 5000, '', 1, 5000),
('T007', 'Relay CPU 4 kaki', 'S005', 'K002', '', 0, 40000, '', 1, 40000),
('T008', 'Relay CPU 5 Kaki', 'S005', 'K002', '', 0, 50000, '', 1, 50000),
('T009', 'valve 5 lubang', 'S005', 'K002', '', 0, 650000, '', 1, 650000),
('T010', 'Selang 8x5 Mili', 'S009', 'K002', '', 0, 10000, '', 1, 10000),
('T011', 'niple T Ukuran 4', 'S005', 'K002', '', 0, 60000, '', 1, 60000),
('T012', 'niple T Ukuran 6', 'S005', 'K002', '', 0, 60000, '', 1, 60000),
('T013', 'niple T Ukuran 8', 'S005', 'K002', '', 0, 60000, '', 1, 60000),
('T014', 'niple T Ukuran 10', 'S005', 'K002', '', 0, 60000, '', 1, 60000),
('T015', 'niple L Ukuran 4', 'S005', 'K002', '', 0, 30000, '', 1, 30000),
('T016', 'niple L Ukuran 6', 'S005', 'K002', '', 0, 30000, '', 1, 30000),
('T017', 'niple L Ukuran 8', 'S005', 'K002', '', 0, 30000, '', 1, 30000),
('T018', 'niple L Ukuran 10', 'S005', 'K002', '', 0, 30000, '', 1, 30000),
('T019', 'niple L drat Ukuran 4', 'S005', 'K002', '', 0, 30000, '', 1, 30000),
('T020', 'niple L drat Ukuran 6', 'S005', 'K002', '', 0, 30000, '', 1, 30000),
('T021', 'niple L drat Ukuran 8', 'S005', 'K002', '', 0, 30000, '', 1, 30000),
('T022', 'niple L drat Ukuran 10', 'S005', 'K002', '', 0, 30000, '', 1, 30000),
('T023', 'niple T drat Ukuran 4', 'S005', 'K002', '', 0, 30000, '', 1, 30000),
('T024', 'niple T drat Ukuran 6', 'S005', 'K002', '', 0, 30000, '', 1, 30000),
('T025', 'niple T drat Ukuran 8', 'S005', 'K002', '', 0, 30000, '', 1, 30000),
('T026', 'niple T drat Ukuran 10', 'S005', 'K002', '', 0, 30000, '', 1, 30000),
('T027', 'Selang nipple Ukuran 4', 'S009', 'K002', '', 0, 10000, '', 1, 10000),
('T028', 'Selang nipple Ukuran 6', 'S009', 'K002', '', 0, 10000, '', 1, 10000),
('T029', 'Selang nipple Ukuran 8', 'S009', 'K002', '', 0, 10000, '', 1, 10000),
('T030', 'Selang nipple Ukuran 10', 'S009', 'K002', '', 0, 10000, '', 1, 10000),
('T031', 'Dongkrak', 'S005', 'K002', '', 0, 1500000, '', 1, 1500000),
('T032', 'Capasitor 300UF', 'S005', 'K002', '', 0, 100000, '', 1, 100000),
('T033', 'Otomatis kompresor', 'S005', 'K002', '', 0, 120000, '', 1, 120000),
('T034', 'Otomatis Mesin Nitroman', 'S005', 'K002', '', 0, 450000, '', 1, 450000),
('T035', 'Dinamo', 'S005', 'K002', '', 0, 1500000, '', 1, 1500000),
('T036', 'Panbel Kompresor', 'S005', 'K002', '', 0, 80000, '', 1, 80000),
('T037', 'cpu mesin nitroman', 'S005', 'K002', '', 0, 1600000, '', 1, 1600000),
('T038', 'Obeng Tespen', 'S005', 'K002', '', 0, 20000, '', 1, 20000),
('T039', 'Plang besi ', 'S005', 'K002', '', 0, 300000, '', 1, 300000),
('T040', 'Oli ', 'S010', 'K001', '', 0, 42000, '', 1, 42000),
('T041', 'Selenoid', 'S005', 'K002', '', 0, 150000, '', 1, 150000),
('T042', 'Bangku ', 'S005', 'K002', '', 0, 25000, '', 1, 25000),
('T043', 'VALVE MESIN', 'S005', 'K002', '', 0, 150000, '', 1, 150000),
('T044', 'Kanebo', 'S005', 'K002', '', 0, 20000, '', 1, 20000),
('T045', 'tang potong', 'S005', 'K002', '', 0, 30000, '', 1, 30000),
('T046', 'Obeng ', 'S005', 'K004', '', 0, 20000, '', 1, 20000),
('T047', 'gunting', 'S005', 'K002', '', 0, 25000, '', 1, 25000),
('T048', 'Cuter', 'S005', 'K002', '', 0, 20000, '', 1, 20000),
('T049', 'Mesin Nitroman', 'S005', 'K002', '', 0, 18000000, '', 1, 18000000),
('T050', 'Kompressor', 'S005', 'K002', '', 0, 9000000, '', 1, 9000000);

-- --------------------------------------------------------

--
-- Table structure for table `tabel_bayar`
--

CREATE TABLE `tabel_bayar` (
  `id_bayar` int(11) NOT NULL,
  `tgl_bayar` date NOT NULL,
  `nm_bank` varchar(30) NOT NULL,
  `no_rek` varchar(30) NOT NULL,
  `kd_supplier` varchar(15) NOT NULL,
  `nominal` varchar(35) NOT NULL,
  `ket` varchar(45) NOT NULL,
  `kliring` int(1) NOT NULL,
  `tgl_update` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tabel_biaya`
--

CREATE TABLE `tabel_biaya` (
  `id` int(10) NOT NULL,
  `tgl` date NOT NULL,
  `id_user` varchar(10) NOT NULL,
  `biaya` int(11) NOT NULL,
  `no_spbu` varchar(11) NOT NULL,
  `jenis` varchar(35) NOT NULL,
  `ket` text NOT NULL,
  `sumber` int(1) NOT NULL,
  `sebelum` int(15) NOT NULL,
  `saldo` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tabel_kartu_stok`
--

CREATE TABLE `tabel_kartu_stok` (
  `idkartu` int(11) NOT NULL,
  `kode_toko` varchar(15) NOT NULL,
  `kode_barang` varchar(15) NOT NULL,
  `waktu` varchar(32) NOT NULL,
  `jam` varchar(32) NOT NULL,
  `sebelumnya` int(6) NOT NULL,
  `masuk` int(6) NOT NULL,
  `keluar` int(6) NOT NULL,
  `saldo` int(6) NOT NULL,
  `keterangan` text NOT NULL,
  `user` varchar(10) NOT NULL,
  `publish` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tabel_kategori_barang`
--

CREATE TABLE `tabel_kategori_barang` (
  `kd_kategori` varchar(10) NOT NULL,
  `nm_kategori` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tabel_kategori_barang`
--

INSERT INTO `tabel_kategori_barang` (`kd_kategori`, `nm_kategori`) VALUES
('K001', 'Barang Persediaan'),
('K002', 'Barang Aset'),
('K003', 'Biaya'),
('K004', 'Persediaan Aset Nilai Kecil');

-- --------------------------------------------------------

--
-- Table structure for table `tabel_kliring`
--

CREATE TABLE `tabel_kliring` (
  `id_kliring` varchar(30) NOT NULL,
  `no_faktur_penjualan` varchar(16) NOT NULL,
  `tgl_penjualan` date NOT NULL,
  `id_setor` int(11) NOT NULL,
  `tgl_setor` date NOT NULL,
  `tgl_kliring` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `no_spbu` varchar(11) NOT NULL,
  `total_penjualan` int(16) NOT NULL,
  `total_setor` int(16) NOT NULL,
  `selisih` int(16) NOT NULL,
  `sisah` int(16) NOT NULL,
  `id_k_s` varchar(30) NOT NULL,
  `kliring` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tabel_kliring_bayar`
--

CREATE TABLE `tabel_kliring_bayar` (
  `id_kliring` varchar(35) NOT NULL,
  `no_faktur_pembelian` varchar(17) NOT NULL,
  `tgl_pembelian` date NOT NULL,
  `id_bayar` int(11) NOT NULL,
  `tgl_bayar` date NOT NULL,
  `tgl_kliring` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `kd_supplier` varchar(15) NOT NULL,
  `total_pembelian` int(16) NOT NULL,
  `total_bayar` int(16) NOT NULL,
  `selisih` int(16) NOT NULL,
  `sisah` int(16) NOT NULL,
  `pot_pph` int(16) NOT NULL,
  `biaya_bank` int(16) NOT NULL,
  `id_k_s` varchar(30) NOT NULL,
  `kliring` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tabel_menu`
--

CREATE TABLE `tabel_menu` (
  `id_menu` int(64) NOT NULL,
  `kode_menu` varchar(16) NOT NULL,
  `nama_menu` varchar(32) NOT NULL,
  `harga_jual` int(16) NOT NULL,
  `harga_modal` int(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tabel_menu`
--

INSERT INTO `tabel_menu` (`id_menu`, `kode_menu`, `nama_menu`, `harga_jual`, `harga_modal`) VALUES
(6, 'J001', 'Isi angin Baru Motor', 5000, 800),
(7, 'J002', 'Tambah angin Motor', 3000, 0),
(10, 'J003', 'Tambal ban motor', 15000, 583),
(12, 'J004', 'Isi angin  baru mobil', 10000, 800),
(13, 'J005', 'Tambah angin mobil', 5000, 0),
(14, 'J006', 'Tambal ban mobil', 22000, 583),
(15, 'J007', 'Tambal ban mobil km39', 25000, 583),
(16, 'J008', 'Ganti Oli Motor', 50000, 42000);

-- --------------------------------------------------------

--
-- Table structure for table `tabel_mutasi`
--

CREATE TABLE `tabel_mutasi` (
  `id` int(10) NOT NULL,
  `mutasi` varchar(6) NOT NULL,
  `kd_barang` varchar(15) NOT NULL,
  `jumlah` int(10) NOT NULL,
  `ket` mediumtext NOT NULL,
  `tgl` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tabel_operator`
--

CREATE TABLE `tabel_operator` (
  `id_operator` varchar(15) NOT NULL,
  `nm_operator` varchar(25) NOT NULL,
  `almt_operator` varchar(150) NOT NULL,
  `tlp_operator` varchar(15) NOT NULL,
  `status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tabel_operator`
--

INSERT INTO `tabel_operator` (`id_operator`, `nm_operator`, `almt_operator`, `tlp_operator`, `status`) VALUES
('OP001', 'Dalih', 'Jakarta', '080889', 1),
('OP002', 'Tarmin', 'Jakarta', '08099', 1),
('OP003', 'Ari', 'Jakarta', '0808', 1),
('OP004', 'Yoto', 'Jakarta', '0806', 1),
('OP005', 'Tarma ', 'A', '08', 1),
('OP006', 'Aby', 'A', '08', 1),
('OP007', 'Lukman', 'A', '08', 1),
('OP008', 'Wawan', 'A', '08', 1),
('OP009', 'Riko', 'A', '08', 1),
('OP010', 'Ruslino', 'A', '08', 1),
('OP0100', 'Abc', 'Jakarta', '0888', 1),
('OP011', 'Zaenudin', 'A', '08', 1),
('OP012', 'Bakhrul Ulum', 'A', '08', 1),
('OP013', 'Janah ', 'A', '08', 1),
('OP014', 'Masroni', 'A', '08', 1),
('OP015', 'Anton Maulana', 'A', '08', 1),
('OP016', 'Rubianto', 'A', '08', 1),
('OP017', 'yusuf', 'A', '08', 1),
('OP018', 'Wahyu', 'A', '08', 1),
('OP019', 'Bukhori', 'A', '08', 1),
('OP020', 'Aka', 'A', '08', 1),
('OP021', 'Taufik', 'A', '08', 1),
('OP022', 'Akmal', 'A', '08', 1),
('OP023', 'Yadi', 'A', '08', 1),
('OP024', 'Umar', 'A', '08', 1),
('OP025', 'Bagus', 'A', '08', 1),
('OP026', 'Mastur', 'A', '08', 1),
('OP027', 'Iif', 'A', '08', 1),
('OP028', 'Iwang', 'A', '08', 1),
('OP029', 'Kasdi', 'A', '08', 1),
('OP030', 'ardia', 'A', '08', 1),
('OP031', 'Ulum', 'A', '08', 1),
('OP032', 'Jamal', 'A', '08', 1),
('OP033', 'nana', 'A', '08', 1),
('OP034', 'Saeful imam', 'A', '08', 1),
('OP035', 'dede', 'A', '08', 1),
('OP036', 'Nurlali', 'A', '08', 1),
('OP037', 'sono', 'A', '08', 1),
('OP038', 'nunung', 'A', '08', 1),
('OP039', 'rudi', 'A', '08', 1),
('OP040', 'Danton', 'A', '08', 1),
('OP041', 'Deby', 'A', '08', 1),
('OP042', 'didi', 'A', '08', 1),
('OP043', 'amin', 'A', '08', 1),
('OP044', 'Hatim', 'A', '08', 1),
('OP045', 'Ucup', 'A', '08', 1),
('OP046', 'Boni', 'A', '08', 1),
('OP047', 'Nimin', 'A', '08', 1),
('OP048', 'Adin', 'A', '08', 1),
('OP049', 'Napin', 'A', '08', 1),
('OP050', 'Abu', 'A', '08', 1),
('OP051', 'Iwan', 'A', '08', 1),
('OP052', 'Saepudin', 'A', '08', 1),
('OP053', 'Rian', 'A', '08', 1),
('OP054', 'Fadli', 'A', '08', 1),
('OP055', 'wanda', 'A', '08', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tabel_pembelian`
--

CREATE TABLE `tabel_pembelian` (
  `no_faktur_pembelian` varchar(32) NOT NULL,
  `kd_supplier` varchar(15) NOT NULL,
  `tgl_pembelian` date NOT NULL,
  `id_user` varchar(10) NOT NULL,
  `total_pembelian` int(11) NOT NULL,
  `selesai` int(1) NOT NULL,
  `kliring` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tabel_penjualan`
--

CREATE TABLE `tabel_penjualan` (
  `no_faktur_penjualan` varchar(16) NOT NULL,
  `tgl_penjualan` date NOT NULL,
  `waktu` time NOT NULL,
  `id_user` varchar(10) NOT NULL,
  `total_penjualan` int(11) NOT NULL,
  `diskon` int(11) NOT NULL,
  `total_penjualan_sdiskon` int(11) NOT NULL,
  `ket_diskon` varchar(30) NOT NULL,
  `cash` int(11) NOT NULL,
  `debet` int(11) NOT NULL,
  `ket` mediumtext NOT NULL,
  `selesai` int(1) NOT NULL,
  `no_spbu` varchar(11) NOT NULL,
  `kliring` int(1) NOT NULL,
  `shift` int(1) NOT NULL,
  `id_operator` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tabel_retur`
--

CREATE TABLE `tabel_retur` (
  `id` int(10) NOT NULL,
  `no_faktur_penjualan` varchar(16) NOT NULL,
  `kd_barang` varchar(15) NOT NULL,
  `nm_barang` varchar(30) NOT NULL,
  `jumlah` varchar(11) NOT NULL,
  `total_retur` varchar(11) NOT NULL,
  `ket` varchar(128) NOT NULL,
  `tgl` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tabel_rinci_kliring`
--

CREATE TABLE `tabel_rinci_kliring` (
  `id` int(11) NOT NULL,
  `id_kliring` varchar(30) NOT NULL,
  `no_faktur_penjualan` varchar(16) NOT NULL,
  `id_setor` int(11) NOT NULL,
  `id_k_s` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tabel_rinci_kliringbayar`
--

CREATE TABLE `tabel_rinci_kliringbayar` (
  `id` int(11) NOT NULL,
  `id_kliring` varchar(30) NOT NULL,
  `no_faktur_pembelian` varchar(17) NOT NULL,
  `id_bayar` int(11) NOT NULL,
  `id_k_s` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tabel_rinci_menu`
--

CREATE TABLE `tabel_rinci_menu` (
  `id_rinci_menu` int(16) NOT NULL,
  `kode_menu` varchar(16) NOT NULL,
  `kode_bahan` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tabel_rinci_menu`
--

INSERT INTO `tabel_rinci_menu` (`id_rinci_menu`, `kode_menu`, `kode_bahan`) VALUES
(18, 'J003', 'T002'),
(33, 'J008', 'T040'),
(34, 'J006', 'T002'),
(35, 'J007', 'T002');

-- --------------------------------------------------------

--
-- Table structure for table `tabel_rinci_pembelian`
--

CREATE TABLE `tabel_rinci_pembelian` (
  `id` int(10) NOT NULL,
  `no_faktur_pembelian` varchar(16) NOT NULL,
  `kd_barang` varchar(15) NOT NULL,
  `nm_barang` varchar(30) NOT NULL,
  `satuan` varchar(25) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  `no_spbu` varchar(11) NOT NULL,
  `sub_total_beli` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tabel_rinci_penjualan`
--

CREATE TABLE `tabel_rinci_penjualan` (
  `id` int(10) NOT NULL,
  `no_faktur_penjualan` varchar(16) NOT NULL,
  `kd_barang` varchar(15) NOT NULL,
  `nm_barang` varchar(30) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `harga_modal` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  `diskonpersen` int(6) NOT NULL,
  `diskonrp` int(11) NOT NULL,
  `sub_total_jual` int(11) NOT NULL,
  `retur` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tabel_rinci_unit`
--

CREATE TABLE `tabel_rinci_unit` (
  `id_rinci_unit` int(15) NOT NULL,
  `no_spbu` varchar(15) NOT NULL,
  `id_operator` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tabel_rinci_unit`
--

INSERT INTO `tabel_rinci_unit` (`id_rinci_unit`, `no_spbu`, `id_operator`) VALUES
(10, '34-17403', 'OP001'),
(11, '34-17403', 'OP002'),
(17, '34-17404', 'OP003'),
(18, '34-17404', 'OP004'),
(19, '34-13409', 'OP005'),
(20, '34-13409', 'OP006'),
(21, '34-13901', 'OP007'),
(22, '34-13901', 'OP008'),
(23, '34-12609', 'OP009'),
(24, '34-12609', 'OP010'),
(25, '34-16937', 'OP011'),
(26, '34-16937', 'OP012'),
(27, '34-15401', 'OP013'),
(28, '34-15401', 'OP014'),
(54, '34-17520', 'OP040'),
(55, '34-17520', 'OP041'),
(56, '34-17549', 'OP042'),
(57, '34-17549', 'OP043'),
(58, '34-17514', 'OP044'),
(59, '34-17514', 'OP045'),
(60, '34-17509', 'OP046'),
(61, '34-17509', 'OP047'),
(62, '34-17533', 'OP048'),
(63, '34-17533', 'OP049'),
(64, '34-17551', 'OP050'),
(65, '34-17551', 'OP051'),
(66, '34-17548', 'OP052'),
(67, '34-17548', 'OP053'),
(68, '33-17801', 'OP054'),
(69, '34-17701', 'OP055'),
(71, '34-16720', 'OP015'),
(72, '34-16414', 'OP016'),
(73, '34-16414', 'OP017'),
(74, '34-17113', 'OP018'),
(75, '34-17113', 'OP019'),
(76, '34-17145', 'OP020'),
(77, '34-17145', 'OP021'),
(78, '34-17124', 'OP022'),
(79, '34-17124', 'OP023'),
(80, '34-17106', 'OP024'),
(81, '34-17106', 'OP025'),
(82, '34-17144', 'OP026'),
(83, '34-17144', 'OP027'),
(84, '34-17139', 'OP028'),
(85, '34-17139', 'OP029'),
(86, '34-17603', 'OP030'),
(87, '34-17603', 'OP031'),
(88, '34-17604', 'OP032'),
(89, '34-17604', 'OP033'),
(90, '34-17150', 'OP034'),
(91, '34-14106', 'OP035'),
(92, '34-14106', 'OP036'),
(93, '34-17531', 'OP037'),
(94, '34-17531', 'OP038'),
(95, '34-17531', 'OP039'),
(96, '11-34345', 'OP001'),
(97, '11-34345', 'OP002');

-- --------------------------------------------------------

--
-- Table structure for table `tabel_satuan_barang`
--

CREATE TABLE `tabel_satuan_barang` (
  `kd_satuan` varchar(10) NOT NULL,
  `nm_satuan` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tabel_satuan_barang`
--

INSERT INTO `tabel_satuan_barang` (`kd_satuan`, `nm_satuan`) VALUES
('S001', 'KG'),
('S002', 'Kotak'),
('S003', 'Kaleng'),
('S004', 'BOX'),
('S005', 'PCS'),
('S006', 'Buah'),
('S007', 'Pack'),
('S008', 'Buku'),
('S009', 'Meter'),
('S010', 'Botol');

-- --------------------------------------------------------

--
-- Table structure for table `tabel_setor`
--

CREATE TABLE `tabel_setor` (
  `id_setor` int(11) NOT NULL,
  `tgl_setor` date NOT NULL,
  `no_spbu` varchar(11) NOT NULL,
  `nominal` varchar(35) NOT NULL,
  `ket` varchar(45) NOT NULL,
  `shift` int(1) NOT NULL,
  `nm_bank` varchar(30) NOT NULL,
  `no_rek` varchar(30) NOT NULL,
  `tgl_update` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `kliring` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tabel_stok_cabang`
--

CREATE TABLE `tabel_stok_cabang` (
  `idstok` int(11) NOT NULL,
  `no_spbu` varchar(11) NOT NULL,
  `kode_barang` varchar(15) NOT NULL,
  `waktu` varchar(32) NOT NULL,
  `jam` varchar(32) NOT NULL,
  `sebelumnya` int(6) NOT NULL,
  `pembelian` int(6) NOT NULL,
  `tf_barang` int(6) NOT NULL,
  `penjualan` int(6) NOT NULL,
  `stok` int(6) NOT NULL,
  `keterangan` text NOT NULL,
  `user` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tabel_stok_toko`
--

CREATE TABLE `tabel_stok_toko` (
  `id` int(10) NOT NULL,
  `kd_toko` varchar(15) NOT NULL,
  `kd_barang` varchar(15) NOT NULL,
  `stok` int(11) NOT NULL,
  `stok_min` int(11) NOT NULL,
  `tgl_perubahan` varchar(64) NOT NULL,
  `publish` int(1) NOT NULL,
  `ket` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tabel_stok_toko`
--

INSERT INTO `tabel_stok_toko` (`id`, `kd_toko`, `kd_barang`, `stok`, `stok_min`, `tgl_perubahan`, `publish`, `ket`) VALUES
(10, 'SS001', 'T002', 0, 5, '19-02-2020 10:58:35', 0, ''),
(11, 'SS001', 'T003', 0, 5, '19-02-2020 10:58:35', 0, ''),
(14, 'SS001', 'T006', 0, 5, '19-02-2020 10:58:35', 0, ''),
(15, 'SS001', 'T007', 0, 5, '19-02-2020 10:58:35', 0, ''),
(16, 'SS001', 'T008', 0, 5, '19-02-2020 10:58:35', 0, ''),
(17, 'SS001', 'T009', 0, 5, '19-02-2020 10:58:35', 0, ''),
(18, 'SS001', 'T010', 0, 5, '19-02-2020 10:58:35', 0, ''),
(19, 'SS001', 'T011', 0, 5, '19-02-2020 10:58:35', 0, ''),
(20, 'SS001', 'T012', 0, 5, '19-02-2020 10:58:35', 0, ''),
(21, 'SS001', 'T013', 0, 5, '19-02-2020 10:58:35', 0, ''),
(22, 'SS001', 'T014', 0, 5, '19-02-2020 10:58:35', 0, ''),
(23, 'SS001', 'T015', 0, 5, '19-02-2020 10:58:35', 0, ''),
(24, 'SS001', 'T016', 0, 5, '19-02-2020 10:58:35', 0, ''),
(25, 'SS001', 'T017', 0, 5, '19-02-2020 10:58:35', 0, ''),
(26, 'SS001', 'T018', 0, 5, '19-02-2020 10:58:35', 0, ''),
(27, 'SS001', 'T019', 0, 5, '19-02-2020 10:58:35', 0, ''),
(28, 'SS001', 'T020', 0, 5, '19-02-2020 10:58:35', 0, ''),
(29, 'SS001', 'T021', 0, 5, '19-02-2020 10:58:35', 0, ''),
(30, 'SS001', 'T022', 0, 5, '19-02-2020 10:58:35', 0, ''),
(31, 'SS001', 'T023', 0, 5, '19-02-2020 10:58:35', 0, ''),
(32, 'SS001', 'T024', 0, 5, '19-02-2020 10:58:35', 0, ''),
(33, 'SS001', 'T025', 0, 5, '19-02-2020 10:58:35', 0, ''),
(34, 'SS001', 'T026', 0, 5, '19-02-2020 10:58:35', 0, ''),
(35, 'SS001', 'T027', 0, 5, '19-02-2020 10:58:35', 0, ''),
(36, 'SS001', 'T028', 0, 5, '19-02-2020 10:58:35', 0, ''),
(37, 'SS001', 'T029', 0, 5, '19-02-2020 10:58:35', 0, ''),
(38, 'SS001', 'T030', 0, 5, '19-02-2020 10:58:35', 0, ''),
(39, 'SS001', 'T031', 0, 5, '19-02-2020 10:58:35', 0, ''),
(40, 'SS001', 'T032', 0, 5, '19-02-2020 10:58:35', 0, ''),
(41, 'SS001', 'T033', 0, 5, '19-02-2020 10:58:35', 0, ''),
(42, 'SS001', 'T034', 0, 5, '19-02-2020 10:58:35', 0, ''),
(43, 'SS001', 'T035', 0, 5, '19-02-2020 10:58:35', 0, ''),
(44, 'SS001', 'T036', 0, 5, '19-02-2020 10:58:35', 0, ''),
(45, 'SS001', 'T037', 0, 5, '19-02-2020 10:58:35', 0, ''),
(46, 'SS001', 'T038', 0, 5, '19-02-2020 10:58:35', 0, ''),
(47, 'SS001', 'T039', 0, 5, '19-02-2020 10:58:35', 0, ''),
(48, 'SS001', 'T040', 0, 5, '19-02-2020 10:58:35', 0, ''),
(49, 'SS001', 'T041', 0, 5, '19-02-2020 10:58:35', 0, ''),
(50, 'SS001', 'T042', 0, 5, '19-02-2020 10:58:35', 0, ''),
(51, 'SS001', 'T043', 0, 5, '19-02-2020 10:58:35', 0, ''),
(52, 'SS001', 'T044', 0, 5, '19-02-2020 10:58:35', 0, ''),
(53, 'SS001', 'T045', 0, 5, '19-02-2020 10:58:35', 0, ''),
(54, 'SS001', 'T046', 0, 5, '19-02-2020 10:58:35', 0, ''),
(55, 'SS001', 'T047', 0, 5, '19-02-2020 10:58:35', 0, ''),
(56, 'SS001', 'T048', 0, 5, '19-02-2020 10:58:35', 0, ''),
(57, 'SS001', 'T049', 0, 5, '19-02-2020 10:58:35', 0, ''),
(58, 'SS001', 'T050', 0, 5, '19-02-2020 10:58:35', 0, ''),
(59, 'SS001', 'T001', 0, 5, '07-06-2020 12:59:06', 0, 'Bahan Baru');

-- --------------------------------------------------------

--
-- Table structure for table `tabel_sumber_biaya`
--

CREATE TABLE `tabel_sumber_biaya` (
  `id` int(1) NOT NULL,
  `nm_sumber_biaya` varchar(25) NOT NULL,
  `saldo` int(16) NOT NULL,
  `tgl` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tabel_sumber_biaya`
--

INSERT INTO `tabel_sumber_biaya` (`id`, `nm_sumber_biaya`, `saldo`, `tgl`) VALUES
(1, 'Transfer', -1100000, '2020-06-06 13:34:31'),
(2, 'Cash (Patty Cash)', 2430000, '2020-05-22 09:43:16');

-- --------------------------------------------------------

--
-- Table structure for table `tabel_supplier`
--

CREATE TABLE `tabel_supplier` (
  `kd_supplier` varchar(15) NOT NULL,
  `nm_supplier` varchar(25) NOT NULL,
  `almt_supplier` varchar(150) NOT NULL,
  `tlp_supplier` varchar(15) NOT NULL,
  `fax_supplier` varchar(15) NOT NULL,
  `atas_nama` varchar(25) NOT NULL,
  `nm_bank` varchar(25) NOT NULL,
  `no_rek` varchar(35) NOT NULL,
  `atas_nm_bank` varchar(35) NOT NULL,
  `pic` varchar(35) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tabel_supplier`
--

INSERT INTO `tabel_supplier` (`kd_supplier`, `nm_supplier`, `almt_supplier`, `tlp_supplier`, `fax_supplier`, `atas_nama`, `nm_bank`, `no_rek`, `atas_nm_bank`, `pic`) VALUES
('S001', 'Achitofel Siahaan', 'Harapan Indah, Bekasi', '081311068519', '', 'Achi ', '-', '-', '-', '-'),
('S002', 'Festo Tool Technic', 'Gajah Mada', '081252039499', '', 'Panji ', '', '', '', ''),
('S003', 'PT. Guntur Mandiri Pratam', 'Kav. Sahara Indah Permai Blok D No.42 Kaliabang, Bekasi', '0856694736008', '', 'Alam', '', '', '', ''),
('S004', 'Benny Printing', 'Ciracas', '085697395553', '', 'Benny', '', '', '', ''),
('S005', 'Fujita Vastar', 'Perjuangan, Bekasi', ' 085215787307', '', 'Fujita', '', '', '', ''),
('S006', 'ABC', 'tes', '087', '', 'abc', 'BCA', '1', 'abc', 'tes');

-- --------------------------------------------------------

--
-- Table structure for table `tabel_toko`
--

CREATE TABLE `tabel_toko` (
  `kd_toko` varchar(15) NOT NULL,
  `nm_toko` varchar(30) NOT NULL,
  `almt_toko` varchar(150) NOT NULL,
  `tlp_toko` varchar(15) NOT NULL,
  `fax_toko` varchar(15) NOT NULL,
  `logo` varchar(50) NOT NULL,
  `password` varchar(35) NOT NULL,
  `status` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tabel_toko`
--

INSERT INTO `tabel_toko` (`kd_toko`, `nm_toko`, `almt_toko`, `tlp_toko`, `fax_toko`, `logo`, `password`, `status`) VALUES
('SS001', 'JAYA NITROGEN', 'Jakarta Pusat', '', '', 'mini.png', 'e10adc3949ba59abbe56e057f20f883e', 'pusat');

-- --------------------------------------------------------

--
-- Table structure for table `tabel_unit`
--

CREATE TABLE `tabel_unit` (
  `id_unit` int(11) NOT NULL,
  `no_area` int(3) NOT NULL,
  `nm_cabang` varchar(30) NOT NULL,
  `no_spbu` varchar(11) NOT NULL,
  `alamat` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tabel_unit`
--

INSERT INTO `tabel_unit` (`id_unit`, `no_area`, `nm_cabang`, `no_spbu`, `alamat`) VALUES
(1, 0, 'Pusat', '00-00000', 'Pusat'),
(2, 1, 'Jatiwaringin', '34-17403', 'Jln. Raya Jatiwaringin No.10, Pondok Gede'),
(3, 1, 'Cibubur', '34-17404', 'Jln, Alternatif Cibubur N0.22'),
(4, 1, 'Buaran', '34-13409', 'Jln. Raden Inten No.17, Buaran, Jakarta Timur'),
(5, 1, 'Cipinang', '34-13901', 'Jln. Raya Bekasi Timur, Cipinang'),
(6, 1, 'Cilandak', '34-12609', 'Jln, Moh Kahfi 1 No.51, Cilandak'),
(7, 1, 'Leuwinanggung', '34-16937', 'Jln. Raya Leuwinanggung, Tapos, Depok'),
(8, 1, 'Ciputat', '34-15401', 'Jln. Otista Raya No.53, Ciputat, Tanerang Selatan, Banten'),
(9, 1, 'Kelapa Nunggal', '34-16720', 'Jln. Raya Klapanunggal, Cikahuripan, Bogor'),
(10, 1, 'Depok', '34-16414', 'Jati Mulya, Cilodong, Depok, Jawa Barat'),
(11, 2, 'Narogong', '34-17113', 'Jln. Raya Narogong KM.7, Bekasi'),
(12, 2, 'Rawa Panjang', '34-17145', 'Jln, Raya Narogong, Rawa Panjang'),
(13, 2, 'Cut Mutia', '34-17124', 'Jln. Raya Cut Mutia No. 77, Bekasi'),
(14, 2, 'Bulan-Bulan', '34-17106', 'Jln. H Juanda No.58, Bekasi'),
(15, 2, 'Kartini', '34-17144', 'Jln. RA Kartini, Margahayu, Bekasi'),
(16, 2, 'Perjuangan', '34-17139', 'Jln. Perjuangan, Bekasi'),
(17, 2, 'Ujung Harapan', '34-17603', 'Jln Raya Ujung Harapan, Karang Bahagia, Bekasi'),
(18, 2, 'Babelan', '34-17604', 'Jln. Raya Kebalen, Babelan, Bekasi'),
(19, 2, 'Setu', '34-17150', 'JLn. Pedurenan, Setu, Bekasi'),
(20, 2, 'Marunda', '34-14106', 'Jln. Bidara Makmur No.5 Marunda, Cilincing, Jakarta Utara'),
(21, 3, 'Toll Km39', '34-17531', 'Rest Area KM.39 Jln Tol Jakarta - Cikampek'),
(22, 3, 'Cikarang', '34-17520', 'Jln. Raya Industri Pasir Gombong, Cikarang Utara'),
(23, 3, 'Jarakosta', '34-17549', 'Jln. Jarakosta, Danau Indah, Cikarang Barat'),
(24, 3, 'Mm2100', '34-17514', 'Kawasan Industri MM2100, Cibitung'),
(25, 3, 'Lemah Abang', '34-17509', 'Jln. Urip Sumoharjo, Tanjungb sari, Lemah Abang, Cikarang Utara'),
(26, 3, 'Sukatani', '34-17533', 'Jln. KH Mas`ud Tridaya Sakti, Tambun Selatan, Bekasi'),
(27, 3, 'Kh. Fudholi', '34-17551', 'Jln. KH. Fudholi No.68, Bekasi'),
(28, 3, 'Bosih', '34-17548', 'Jln Raya Bosih Selang, Wanasari, Cibitung, Bekasi'),
(29, 3, 'Gajah Mungkur', '33-17801', 'Jln. Raya Teuku Umar, Telaga Asih, Bekasi'),
(30, 3, 'Cabang Bungin', '34-17701', 'Jln. Lenggah Jaya, Cabang Bungin, Bekasi');

-- --------------------------------------------------------

--
-- Table structure for table `tabel_user`
--

CREATE TABLE `tabel_user` (
  `id_user` varchar(10) NOT NULL,
  `nm_user` varchar(25) NOT NULL,
  `password` varchar(35) NOT NULL,
  `akses` varchar(15) NOT NULL,
  `kd_toko` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tabel_user`
--

INSERT INTO `tabel_user` (`id_user`, `nm_user`, `password`, `akses`, `kd_toko`) VALUES
('admin', 'Admin', '21232f297a57a5a743894a0e4a801fc3', 'admin', 'SS001'),
('investor', 'investor', 'b4f18f5b05307bd1e3cc00e0802d641b', 'investor', 'SS001'),
('manajer', 'Manajer', '69b731ea8f289cf16a192ce78a37b4f0', 'manajer', 'SS001');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tabel_agent`
--
ALTER TABLE `tabel_agent`
  ADD PRIMARY KEY (`id_agent`);

--
-- Indexes for table `tabel_bank`
--
ALTER TABLE `tabel_bank`
  ADD PRIMARY KEY (`id_bank`);

--
-- Indexes for table `tabel_barang`
--
ALTER TABLE `tabel_barang`
  ADD PRIMARY KEY (`kd_barang`),
  ADD KEY `kd_barang` (`kd_barang`),
  ADD KEY `kd_satuan` (`kd_satuan`,`kd_kategori`,`kd_supplier`);

--
-- Indexes for table `tabel_bayar`
--
ALTER TABLE `tabel_bayar`
  ADD PRIMARY KEY (`id_bayar`);

--
-- Indexes for table `tabel_biaya`
--
ALTER TABLE `tabel_biaya`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tabel_kartu_stok`
--
ALTER TABLE `tabel_kartu_stok`
  ADD PRIMARY KEY (`idkartu`);

--
-- Indexes for table `tabel_kategori_barang`
--
ALTER TABLE `tabel_kategori_barang`
  ADD PRIMARY KEY (`kd_kategori`);

--
-- Indexes for table `tabel_kliring`
--
ALTER TABLE `tabel_kliring`
  ADD PRIMARY KEY (`id_kliring`);

--
-- Indexes for table `tabel_kliring_bayar`
--
ALTER TABLE `tabel_kliring_bayar`
  ADD PRIMARY KEY (`id_kliring`);

--
-- Indexes for table `tabel_menu`
--
ALTER TABLE `tabel_menu`
  ADD PRIMARY KEY (`id_menu`),
  ADD KEY `kode_menu` (`kode_menu`);

--
-- Indexes for table `tabel_mutasi`
--
ALTER TABLE `tabel_mutasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tabel_operator`
--
ALTER TABLE `tabel_operator`
  ADD PRIMARY KEY (`id_operator`);

--
-- Indexes for table `tabel_pembelian`
--
ALTER TABLE `tabel_pembelian`
  ADD PRIMARY KEY (`no_faktur_pembelian`);

--
-- Indexes for table `tabel_penjualan`
--
ALTER TABLE `tabel_penjualan`
  ADD PRIMARY KEY (`no_faktur_penjualan`),
  ADD KEY `no_faktur_penjualan` (`no_faktur_penjualan`);

--
-- Indexes for table `tabel_retur`
--
ALTER TABLE `tabel_retur`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tabel_rinci_kliring`
--
ALTER TABLE `tabel_rinci_kliring`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tabel_rinci_kliringbayar`
--
ALTER TABLE `tabel_rinci_kliringbayar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tabel_rinci_menu`
--
ALTER TABLE `tabel_rinci_menu`
  ADD PRIMARY KEY (`id_rinci_menu`);

--
-- Indexes for table `tabel_rinci_pembelian`
--
ALTER TABLE `tabel_rinci_pembelian`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tabel_rinci_penjualan`
--
ALTER TABLE `tabel_rinci_penjualan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `no_faktur_penjualan` (`no_faktur_penjualan`),
  ADD KEY `kd_barang` (`kd_barang`);

--
-- Indexes for table `tabel_rinci_unit`
--
ALTER TABLE `tabel_rinci_unit`
  ADD PRIMARY KEY (`id_rinci_unit`);

--
-- Indexes for table `tabel_satuan_barang`
--
ALTER TABLE `tabel_satuan_barang`
  ADD PRIMARY KEY (`kd_satuan`);

--
-- Indexes for table `tabel_setor`
--
ALTER TABLE `tabel_setor`
  ADD PRIMARY KEY (`id_setor`);

--
-- Indexes for table `tabel_stok_cabang`
--
ALTER TABLE `tabel_stok_cabang`
  ADD PRIMARY KEY (`idstok`);

--
-- Indexes for table `tabel_stok_toko`
--
ALTER TABLE `tabel_stok_toko`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kd_barang` (`kd_barang`);

--
-- Indexes for table `tabel_sumber_biaya`
--
ALTER TABLE `tabel_sumber_biaya`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tabel_supplier`
--
ALTER TABLE `tabel_supplier`
  ADD PRIMARY KEY (`kd_supplier`);

--
-- Indexes for table `tabel_toko`
--
ALTER TABLE `tabel_toko`
  ADD PRIMARY KEY (`kd_toko`);

--
-- Indexes for table `tabel_unit`
--
ALTER TABLE `tabel_unit`
  ADD PRIMARY KEY (`id_unit`);

--
-- Indexes for table `tabel_user`
--
ALTER TABLE `tabel_user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tabel_agent`
--
ALTER TABLE `tabel_agent`
  MODIFY `id_agent` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tabel_bank`
--
ALTER TABLE `tabel_bank`
  MODIFY `id_bank` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tabel_bayar`
--
ALTER TABLE `tabel_bayar`
  MODIFY `id_bayar` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tabel_biaya`
--
ALTER TABLE `tabel_biaya`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tabel_kartu_stok`
--
ALTER TABLE `tabel_kartu_stok`
  MODIFY `idkartu` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tabel_menu`
--
ALTER TABLE `tabel_menu`
  MODIFY `id_menu` int(64) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `tabel_mutasi`
--
ALTER TABLE `tabel_mutasi`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tabel_retur`
--
ALTER TABLE `tabel_retur`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tabel_rinci_kliring`
--
ALTER TABLE `tabel_rinci_kliring`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tabel_rinci_kliringbayar`
--
ALTER TABLE `tabel_rinci_kliringbayar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tabel_rinci_menu`
--
ALTER TABLE `tabel_rinci_menu`
  MODIFY `id_rinci_menu` int(16) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `tabel_rinci_pembelian`
--
ALTER TABLE `tabel_rinci_pembelian`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tabel_rinci_penjualan`
--
ALTER TABLE `tabel_rinci_penjualan`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tabel_rinci_unit`
--
ALTER TABLE `tabel_rinci_unit`
  MODIFY `id_rinci_unit` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=98;

--
-- AUTO_INCREMENT for table `tabel_setor`
--
ALTER TABLE `tabel_setor`
  MODIFY `id_setor` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tabel_stok_cabang`
--
ALTER TABLE `tabel_stok_cabang`
  MODIFY `idstok` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=170;

--
-- AUTO_INCREMENT for table `tabel_stok_toko`
--
ALTER TABLE `tabel_stok_toko`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `tabel_unit`
--
ALTER TABLE `tabel_unit`
  MODIFY `id_unit` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tabel_rinci_penjualan`
--
ALTER TABLE `tabel_rinci_penjualan`
  ADD CONSTRAINT `fk_kd_barang` FOREIGN KEY (`kd_barang`) REFERENCES `tabel_menu` (`kode_menu`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
