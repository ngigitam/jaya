<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penjualan extends CI_Controller {
	 private $filename = "import_data";

	function __construct() {
		parent::__construct();
		//validasi jika user belum login
		if ($this->session->userdata('masuk') != TRUE) {
			$url = base_url();
			redirect($url);
		}

		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('kasir_model');
		$this->load->model('gudang_model');
		$this->load->model('Penjualan_model','penjualan_model');
		$this->load->helper('random');
	}
	
	 public function index()
    {
		$data['bank'] = $this->gudang_model->get_bank();
		$data['unit'] = $this->kasir_model->getUnit();
		$this->load->view('header',$data);
		$this->load->view('kasir/v_penjualan');
    }
	public function ajax_list()
    {
        $list = $this->penjualan_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $penjualan) {
            $no++;
            $row = array();
            $row[] = $penjualan->no_faktur_penjualan;
            $row[] = $penjualan->tgl_penjualan;
            $row[] = $penjualan->no_spbu;
			$row[] = $penjualan->id_operator;
			$row[] = number_format($penjualan->total_penjualan, 0, ',', '.');
 
            //add html for action
            $row[] = '<!--a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_penjualan('."'".$penjualan->no_faktur_penjualan."'".')"><i class="glyphicon glyphicon-pencil"></i></a-->
                  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_penjualan('."'".$penjualan->no_faktur_penjualan."'".')"><i class="glyphicon glyphicon-trash"></i></a>';
 
            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->penjualan_model->count_all(),
                        "recordsFiltered" => $this->penjualan_model->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }
 
    public function ajax_edit($id)
    {
        $data = $this->penjualan_model->get_by_id($id);
        echo json_encode($data);
    }
 
    public function ajax_add()
    {
        $data = array(
                'tgl_penjualan' => $this->input->post('tgl_penjualan'),
                'no_spbu' => $this->input->post('no_spbu'),
                'id_operator' => $this->input->post('id_operator'),
				'total_penjualan' => str_replace(".", "", $this->input->post('total_penjualan')),
				
            );
        $insert = $this->penjualan_model->save($data);
        echo json_encode(array("status" => TRUE));
    }
 
    public function ajax_update()
    {
        $data = array(
			    'tgl_penjualan' => $this->input->post('tgl_penjualan'),
                'no_spbu' => $this->input->post('no_spbu'),
                'id_operator' => $this->input->post('id_operator'),
				'total_penjualan' => str_replace(".", "", $this->input->post('total_penjualan')),
				
            );
        $this->penjualan_model->update(array('no_faktur_penjualan' => $this->input->post('id')), $data);
        echo json_encode(array("status" => TRUE));
    }
 
    public function ajax_delete($id)
    {
        $this->penjualan_model->delete_by_id($id);
		$this->db->query("DELETE FROM tabel_rinci_penjualan WHERE no_faktur_penjualan='$id'");
        echo json_encode(array("status" => TRUE));
    }
 
	
	


}

/* End of file penjualan.php */
/* Location: ./application/controllers/penjualan.php */