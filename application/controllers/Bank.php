<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bank extends CI_Controller {
	 
	function __construct() {
		parent::__construct();
		//validasi jika user belum login
		if ($this->session->userdata('masuk') != TRUE) {
			$url = base_url();
			redirect($url);
		}

		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('kasir_model');
		$this->load->model('gudang_model');
		$this->load->model('bank_model','bank_model');
		$this->load->helper('random');
	}
	
	 public function index()
    {
       // $this->load->helper('url');
        //$this->load->view('setor_view');
		$data['unit'] = $this->kasir_model->getUnit();
		$this->load->view('header',$data);
		$this->load->view('gudang/v_bank');
    }
	public function ajax_list()
    {
        $list = $this->bank_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $bank) {
            $no++;
            $row = array();
            $row[] = $bank->id_bank;
            $row[] = $bank->nm_bank;
            $row[] = $bank->no_rek;
			//$row[] = $bank->ket;
            //add html for action
            $row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_bank('."'".$bank->id_bank."'".')"><i class="glyphicon glyphicon-pencil"></i></a>
                  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_bank('."'".$bank->id_bank."'".')"><i class="glyphicon glyphicon-trash"></i></a>';
 
            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->bank_model->count_all(),
                        "recordsFiltered" => $this->bank_model->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }
 
    public function ajax_edit($id)
    {
        $data = $this->bank_model->get_by_id($id);
        echo json_encode($data);
    }
 
    public function ajax_add()
    {
        $data = array(
                'nm_bank' => $this->input->post('bank'),
                'no_rek' => $this->input->post('no_rek'),
				'ket' => $this->input->post('ket'),
            );
        $insert = $this->bank_model->save($data);
        echo json_encode(array("status" => TRUE));
    }
 
    public function ajax_update()
    {
        $data = array(
                'nm_bank' => $this->input->post('bank'),
                'no_rek' => $this->input->post('no_rek'),
				'ket' => $this->input->post('ket'),
            );
        $this->bank_model->update(array('id_bank' => $this->input->post('id')), $data);
        echo json_encode(array("status" => TRUE));
    }
 
    public function ajax_delete($id)
    {
        $this->bank_model->delete_by_id($id);
        echo json_encode(array("status" => TRUE));
    }
 
	
	


}

/* End of file bank.php */
/* Location: ./application/controllers/bank.php */