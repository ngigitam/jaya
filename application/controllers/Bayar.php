<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bayar extends CI_Controller {
	 private $filename = "import_data";

	function __construct() {
		parent::__construct();
		//validasi jika user belum login
		if ($this->session->userdata('masuk') != TRUE) {
			$url = base_url();
			redirect($url);
		}

		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('kasir_model');
		$this->load->model('gudang_model');
		$this->load->model('bayar_model','bayar_model');
		$this->load->helper('random');
	}
	
	 public function index()
    {
       // $this->load->helper('url');
        //$this->load->view('bayar_view');
		$data['bank'] = $this->gudang_model->get_bank();
		$data['unit'] = $this->kasir_model->getUnit();
		$data['supplier'] = $this->gudang_model->getSupplier1();
		$this->load->view('header',$data);
		$this->load->view('gudang/v_bayar');
    }
	public function ajax_list()
    {
        $list = $this->bayar_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $bayar) {
            $no++;
            $row = array();
            $row[] = $bayar->tgl_bayar;
            $row[] = $bayar->nm_bank;
            $row[] = $bayar->no_rek;
            $row[] = $bayar->kd_supplier;
			$row[] = number_format($bayar->nominal, 0, ',', '.');//number_format($key->saldo, 0, ',', '.')
			$row[] = $bayar->ket;
 
            //add html for action
            $row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_bayar('."'".$bayar->id_bayar."'".')"><i class="glyphicon glyphicon-pencil"></i></a>
                  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_bayar('."'".$bayar->id_bayar."'".')"><i class="glyphicon glyphicon-trash"></i></a>';
 
            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->bayar_model->count_all(),
                        "recordsFiltered" => $this->bayar_model->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }
 
    public function ajax_edit($id)
    {
        $data = $this->bayar_model->get_by_id($id);
        echo json_encode($data);
    }
 
    public function ajax_add()
    {
        $data = array(
                'tgl_bayar' => $this->input->post('tgl_bayar'),
                'nm_bank' => $this->input->post('bank'),
                'no_rek' => $this->input->post('no_rek'),
                'kd_supplier' => $this->input->post('kd_supplier'),
				'nominal' => str_replace(".", "", $this->input->post('nominal')),
				'ket' => $this->input->post('ket'),
            );
        $insert = $this->bayar_model->save($data);
        echo json_encode(array("status" => TRUE));
    }
 
    public function ajax_update()
    {
        $data = array(
			    'tgl_bayar' => $this->input->post('tgl_bayar'),
                'nm_bank' => $this->input->post('bank'),
                'no_rek' => $this->input->post('no_rek'),
                'kd_supplier' => $this->input->post('kd_supplier'),
				'nominal' => str_replace(".", "", $this->input->post('nominal')),
				'ket' => $this->input->post('ket'),
            );
        $this->bayar_model->update(array('id_bayar' => $this->input->post('id')), $data);
        echo json_encode(array("status" => TRUE));
    }
 
    public function ajax_delete($id)
    {
        $this->bayar_model->delete_by_id($id);
        echo json_encode(array("status" => TRUE));
    }
 
	
	


}

/* End of file bayar.php */
/* Location: ./application/controllers/bayar.php */