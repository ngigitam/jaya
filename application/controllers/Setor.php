<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setor extends CI_Controller {
	 private $filename = "import_data";

	function __construct() {
		parent::__construct();
		//validasi jika user belum login
		if ($this->session->userdata('masuk') != TRUE) {
			$url = base_url();
			redirect($url);
		}

		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('kasir_model');
		$this->load->model('gudang_model');
		$this->load->model('setor_model','setor_model');
		$this->load->helper('random');
	}
	
	 public function index()
    {
       // $this->load->helper('url');
        //$this->load->view('setor_view');
		$data['bank'] = $this->gudang_model->get_bank();
		$data['unit'] = $this->kasir_model->getUnit();
		$this->load->view('header',$data);
		$this->load->view('kasir/setoran');
    }
	public function ajax_list()
    {
        $list = $this->setor_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $setor) {
            $no++;
            $row = array();
            $row[] = $setor->tgl_setor;
            $row[] = $setor->nm_bank;
            $row[] = $setor->no_rek;
            $row[] = $setor->no_spbu;
            $row[] = $setor->shift;
			$row[] = number_format($setor->nominal, 0, ',', '.');//number_format($key->saldo, 0, ',', '.')
			$row[] = $setor->ket;
 
            //add html for action
            $row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_setor('."'".$setor->id_setor."'".')"><i class="glyphicon glyphicon-pencil"></i></a>
                  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_setor('."'".$setor->id_setor."'".')"><i class="glyphicon glyphicon-trash"></i></a>';
 
            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->setor_model->count_all(),
                        "recordsFiltered" => $this->setor_model->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }
 
    public function ajax_edit($id)
    {
        $data = $this->setor_model->get_by_id($id);
        echo json_encode($data);
    }
 
    public function ajax_add()
    {
        $data = array(
                'tgl_setor' => $this->input->post('tgl_setor'),
                'nm_bank' => $this->input->post('bank'),
                'no_rek' => $this->input->post('no_rek'),
                'no_spbu' => $this->input->post('no_spbu'),
                'shift' => $this->input->post('shift'),
				'nominal' => str_replace(".", "", $this->input->post('nominal')),
				'ket' => $this->input->post('ket'),
            );
        $insert = $this->setor_model->save($data);
        echo json_encode(array("status" => TRUE));
    }
 
    public function ajax_update()
    {
        $data = array(
			    'tgl_setor' => $this->input->post('tgl_setor'),
                'nm_bank' => $this->input->post('bank'),
                'no_rek' => $this->input->post('no_rek'),
                'no_spbu' => $this->input->post('no_spbu'),
                'shift' => $this->input->post('shift'),
				'nominal' => str_replace(".", "", $this->input->post('nominal')),
				'ket' => $this->input->post('ket'),
            );
        $this->setor_model->update(array('id_setor' => $this->input->post('id')), $data);
        echo json_encode(array("status" => TRUE));
    }
 
    public function ajax_delete($id)
    {
        $this->setor_model->delete_by_id($id);
        echo json_encode(array("status" => TRUE));
    }
 
	
	


}

/* End of file Setor.php */
/* Location: ./application/controllers/Setor.php */