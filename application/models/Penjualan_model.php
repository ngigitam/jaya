<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class penjualan_model extends CI_Model {

	var $table = 'tabel_penjualan';
    var $column_order = array('no_faktur_penjualan','tgl_penjualan','total_penjualan','no_spbu','id_operator',null); //set column field database for datatable orderable
    var $column_search = array('no_faktur_penjualan','tgl_penjualan','total_penjualan','no_spbu','id_operator'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('no_faktur_penjualan' => 'desc'); // default order 
 
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
 
    private function _get_datatables_query()
    {
         
        $this->db->from($this->table);
		/*$this->db->where('kliring','1');
		$this->db->select('
		a.no_faktur_penjualan AS no_faktur_penjualan,
		a.tgl_penjualan AS tgl_penjualan,
		a.total_penjualan AS total_penjualan,
		b.nm_cabang AS no_spbu,
		c.nm_operator AS id_operator
		');
		$this->db->from($this->table .' a');
		$this->db->join('tabel_unit b', 'a.no_spbu=b.no_spbu','left');
		$this->db->join('tabel_operator c', 'a.id_operator=c.id_operator','left');
	*/

      
	  $i = 0;
     
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
		
        return $query->result();
    }
 
    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
 
    public function get_by_id($id)
    {
        $this->db->from($this->table);
        $this->db->where('no_faktur_penjualan',$id);
		
        $query = $this->db->get();
 
        return $query->row();
    }
 
    public function save($data)
    {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }
 
    public function update($where, $data)
    {
        $this->db->update($this->table, $data, $where);
        return $this->db->affected_rows();
    }
 
    public function delete_by_id($id)
    {
        $this->db->where('no_faktur_penjualan', $id);
        $this->db->delete($this->table);
    }
	

}

/* End of file penjualan_model.php */
/* Location: ./application/models/penjualan_model.php */