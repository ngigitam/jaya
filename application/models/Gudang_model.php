<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gudang_model extends CI_Model {

	public function getKategory() {
		$this->db->order_by('kd_kategori');
		return $this->db->get('tabel_kategori_barang');
	}

	public function cekKodeKategori($kode) {
		$query = $this->db->query("SELECT kd_kategori FROM tabel_kategori_barang WHERE kd_kategori='$kode'");
		return $query;
	}

	public function getSatuan() {
		$this->db->order_by('kd_satuan');
		return $this->db->get('tabel_satuan_barang');
	}
	
	public function getSumber() {
		$this->db->order_by('id');
		return $this->db->get('tabel_sumber_biaya');
	}
	
	function get_unit2(){
      //  $query = $this->db->get('tabel_unit');
       // return $query;  
		$this->db->order_by('id_unit');
		return $this->db->get('tabel_unit');
    }
	function get_bank(){
		$this->db->order_by('id_bank');
		return $this->db->get('tabel_bank');
    }
	
	public function getUnit1() {
		$hasil=$this->db->query("
		SELECT a.nm_cabang AS nm_cabang,
		a.no_spbu AS no_spbu,
		b.no_spbu AS spbu,
		b.id_operator AS id_operator,
		c.nm_operator AS nm_operator
		FROM tabel_unit a
		Left Join tabel_rinci_unit b ON a.no_spbu=b.no_spbu
		Left Join tabel_operator c ON b.id_operator=c.id_operator
		Group By a.no_spbu
		");
        return $hasil;
	}
	
	function get_sumber($id){
        $hasil=$this->db->query("SELECT * FROM tabel_sumber_biaya WHERE id='$id'");
        return $hasil->result();
    }
	
	function get_operator1($id){
        $hasil=$this->db->query("SELECT * FROM tabel_unit WHERE no_spbu='$id'");
        return $hasil->result();
    }
	
	function get_operator2($no_spbu){
        $query = $this->db->get_where('tabel_unit', array('no_spbu' => $no_spbu));
        return $query;
    }
	
	function get_bank1($bank){
        $query = $this->db->get_where('tabel_bank', array('nm_bank' => $bank));
        return $query;
    }
	
	function get_operator($id){
        $hasil=$this->db->query("
		SELECT a.* ,b.no_spbu AS no_spbu,
		b.id_operator AS id_operator,
		c.nm_operator AS nm_operator
		FROM tabel_unit a
		Left Join tabel_rinci_unit b ON a.no_spbu=b.no_spbu
		Left Join tabel_operator c ON b.id_operator=c.id_operator
		WHERE a.no_spbu='$id'");
        return $hasil->result();
		
    }
	
	function get_operator3(){
		$this->db->select('
		a.* ,b.no_spbu AS no_spbu,
		c.id_operator AS id_operator,
		c.nm_operator AS nm_operator
		');
		$this->db->from('tabel_unit a');
		$this->db->join('tabel_rinci_unit b', 'a.no_spbu=b.no_spbu','left');
		$this->db->join('tabel_operator c', 'b.id_operator=c.id_operator','left');
		$this->db->group_by('c.id_operator','b.no_spbu');
		$query = $this->db->get();
		return $query;
		
    }

	public function cekKodeSatuan($kode) {
		$query = $this->db->query("SELECT kd_satuan FROM tabel_satuan_barang WHERE kd_satuan='$kode'");
		return $query;
	}
	
	public function cekIdSumber($id) {
		$query = $this->db->query("SELECT id FROM tabel_sumber_biaya WHERE id='$id'");
		return $query;
	}
	
	public function cekKodeOperator($kode) {
		$query = $this->db->query("SELECT id_operator FROM tabel_operator WHERE id_operator='$kode'");
		return $query;
	}

	public function getSupplier() {
		$this->db->order_by('kd_supplier');
		return $this->db->get('tabel_supplier');
	}
	
	public function getOperator() {
		$this->db->order_by('id_operator');
		return $this->db->get('tabel_operator');
	}

	public function cekKodeSupplier($kode) {
		$query = $this->db->query("SELECT kd_supplier FROM tabel_supplier WHERE kd_supplier='$kode'");
		return $query;
	}

	public function getProduk() {
		$this->load->library('datatables');
		$this->datatables->select('a.kd_barang,a.nm_barang,a.kd_satuan,a.kd_kategori,a.kd_supplier,a.hrg_jual,a.hrg_beli,a.kode_virtual,b.nm_kategori,c.nm_satuan,d.nm_supplier,a.estimasi_stok,a.modal_per_porsi');
		$this->datatables->from('tabel_barang AS a');
		$this->datatables->join('tabel_kategori_barang AS b', 'a.kd_kategori = b.kd_kategori', 'left');
		$this->datatables->join('tabel_satuan_barang AS c', 'a.kd_satuan = c.kd_satuan', 'left');
		$this->datatables->join('tabel_supplier AS d', 'a.kd_supplier = d.kd_supplier', 'left');
		$this->db->order_by('a.kd_kategori');
		$this->datatables->add_column('Aksi', '<a href="javascript:void(0);" class="edit_record" title="Edit data" data-kode="$1" data-nama="$2" data-satuan="$3" data-kategori="$4" data-supplier="$5" data-jual="$6" data-beli="$7" data-satuan="$8" data-porsi="$9"><i class="fa fa-pencil-square-o"></i></a> <a href="javascript:void(0);" class="hapus_record" title="Hapus data" data-kode="$1"><i class="fa fa-trash-o"></i></a>', 'kd_barang, nm_barang, kd_satuan, kd_kategori, kd_supplier, hrg_jual, hrg_beli, nm_satuan, estimasi_stok');
		return print_r($this->datatables->generate());
	}

	public function cekKodeBarang($kode) {
		return $this->db->query("SELECT kd_barang FROM tabel_barang WHERE kd_barang='$kode'");
	}

	public function getStok() {
		return $this->db->query("SELECT * FROM tabel_stok_toko,tabel_barang,tabel_kategori_barang,tabel_satuan_barang WHERE tabel_stok_toko.kd_barang=tabel_barang.kd_barang AND tabel_barang.kd_kategori=tabel_kategori_barang.kd_kategori AND tabel_barang.kd_satuan=tabel_satuan_barang.kd_satuan AND tabel_stok_toko.stok>0 ORDER BY tabel_barang.kd_barang ASC");
	}

	public function getStokAll() {
		return $this->db->query("SELECT * FROM tabel_stok_toko,tabel_barang,tabel_kategori_barang,tabel_satuan_barang WHERE tabel_stok_toko.kd_barang=tabel_barang.kd_barang AND tabel_barang.kd_kategori=tabel_kategori_barang.kd_kategori AND tabel_barang.kd_satuan=tabel_satuan_barang.kd_satuan ORDER BY tabel_barang.kd_barang ASC");
	}

	public function getStokAllEmpty() {
		return $this->db->query("SELECT * FROM tabel_stok_toko,tabel_barang,tabel_kategori_barang,tabel_satuan_barang WHERE tabel_stok_toko.kd_barang=tabel_barang.kd_barang AND tabel_barang.kd_kategori=tabel_kategori_barang.kd_kategori AND tabel_barang.kd_satuan=tabel_satuan_barang.kd_satuan AND tabel_stok_toko.stok=0 ORDER BY tabel_barang.kd_barang ASC");
	}

	public function getStokSort($kat) {
		return $this->db->query("SELECT * FROM tabel_stok_toko,tabel_barang,tabel_kategori_barang,tabel_satuan_barang WHERE tabel_stok_toko.kd_barang=tabel_barang.kd_barang AND tabel_barang.kd_kategori=tabel_kategori_barang.kd_kategori AND tabel_barang.kd_satuan=tabel_satuan_barang.kd_satuan AND tabel_kategori_barang.kd_kategori='" . $kat . "' ORDER BY tabel_barang.kd_barang ASC");
	}

	public function getStokEmpty($kat) {
		return $this->db->query("SELECT * FROM tabel_stok_toko,tabel_barang,tabel_kategori_barang,tabel_satuan_barang WHERE tabel_stok_toko.kd_barang=tabel_barang.kd_barang AND tabel_barang.kd_kategori=tabel_kategori_barang.kd_kategori AND tabel_barang.kd_satuan=tabel_satuan_barang.kd_satuan AND tabel_stok_toko.stok=0 AND tabel_kategori_barang.kd_kategori='" . $kat . "' ORDER BY tabel_barang.kd_barang ASC");
	}

	public function getStokMore($kat) {
		return $this->db->query("SELECT * FROM tabel_stok_toko,tabel_barang,tabel_kategori_barang,tabel_satuan_barang WHERE tabel_stok_toko.kd_barang=tabel_barang.kd_barang AND tabel_barang.kd_kategori=tabel_kategori_barang.kd_kategori AND tabel_barang.kd_satuan=tabel_satuan_barang.kd_satuan AND tabel_stok_toko.stok>0 AND tabel_kategori_barang.kd_kategori='" . $kat . "' ORDER BY tabel_barang.kd_barang ASC");
	}

	public function getStokMin() {
		return $this->db->query("SELECT * FROM tabel_stok_toko,tabel_barang,tabel_kategori_barang,tabel_satuan_barang WHERE tabel_stok_toko.kd_barang=tabel_barang.kd_barang AND tabel_barang.kd_kategori=tabel_kategori_barang.kd_kategori AND tabel_barang.kd_satuan=tabel_satuan_barang.kd_satuan AND tabel_stok_toko.stok<tabel_stok_toko.stok_min ORDER BY tabel_barang.kd_barang ASC");
	}

	public function getStokMaudiEdit() {
		$this->load->library('datatables');
		$this->datatables->select('a.kd_barang,a.nm_barang,a.kd_kategori,b.nm_kategori,e.stok,e.stok_min');
		$this->datatables->from('tabel_barang AS a');
		$this->datatables->join('tabel_kategori_barang AS b', 'a.kd_kategori = b.kd_kategori', 'left');
		$this->datatables->join('tabel_stok_toko AS e', 'a.kd_barang = e.kd_barang', 'left');
		$this->datatables->add_column('Aksi', '<a href="javascript:void(0);" class="edit_record" title="Edit data" data-kode="$1" data-nama="$2" data-kategori="$3" data-stok="$4" data-stok_min="$5"><i class="fa fa-pencil-square-o"></i></a>', 'kd_barang, nm_barang, kd_kategori, stok, stok_min');
		return print_r($this->datatables->generate());
	}

	public function getNoFakturPembelian($ymd) {
		$q = $this->db->query("SELECT MAX(RIGHT(no_faktur_pembelian,5)) AS id_max FROM tabel_pembelian WHERE substr(no_faktur_pembelian,6,6)='$ymd'");
		$kd = "";
		$kodeawal = "SS001";
		if ($q->num_rows() > 0) {
			foreach ($q->result() as $k) {
				$tmp = ((int) $k->id_max) + 1;
				$kd = sprintf("%05s", $tmp);
			}
		} else {
			$kd = "00001";
		}
		return $kodeawal . $ymd . $kd;
	}

	public function getDataPembelian($noresi, $username) {
		$this->db->where('tabel_pembelian.no_faktur_pembelian', $noresi);
		$this->db->where('tabel_pembelian.id_user', $username);
		$this->db->where('tabel_pembelian.selesai', '0');
		return $this->db->get('tabel_pembelian');
	}

	public function get_detail_produk($idbarang) {
		$hsl = $this->db->query("SELECT tabel_stok_toko.stok, tabel_barang.nm_barang, tabel_satuan_barang.nm_satuan, tabel_barang.hrg_beli, tabel_barang.hrg_jual, tabel_kategori_barang.nm_kategori FROM tabel_barang LEFT JOIN tabel_stok_toko ON tabel_barang.kd_barang = tabel_stok_toko.kd_barang LEFT JOIN tabel_kategori_barang ON tabel_barang.kd_kategori = tabel_kategori_barang.kd_kategori LEFT JOIN tabel_satuan_barang ON tabel_barang.kd_satuan = tabel_satuan_barang.kd_satuan WHERE tabel_barang.kd_barang='$idbarang'");
		if ($hsl->num_rows() > 0) {
			foreach ($hsl->result() as $data) {
				$hasil = array(
					'namaproduk' => $data->nm_barang,
					'stok' => $data->stok,
					'harga' => $data->hrg_jual,
					'kategori' => $data->nm_kategori,
					'harga_beli' => $data->hrg_beli,
					'satuan' => $data->nm_satuan,
				);
			}
		}
		return $hasil;
	}

	public function data_list_pembelian($nofak) {
		return $this->db->select('tabel_rinci_pembelian.*')
			->where('no_faktur_pembelian', $nofak)
			->get('tabel_rinci_pembelian')
			->result();
	}

	public function getbarang($idbarang) {
		$this->db->where('kd_barang', $idbarang);
		return $this->db->get('tabel_barang');
	}

	public function getPembelianSelesai($nofaktur, $id_user) {
		$this->db->where('no_faktur_pembelian', $nofaktur);
		$this->db->where('id_user', $id_user);
		return $this->db->get('tabel_pembelian');
	}

	public function getProdukDibeli($nofaktur) {
		$this->db->where('no_faktur_pembelian', $nofaktur);
		return $this->db->get('tabel_rinci_pembelian');
	}

	public function getStokBeli($kd_barang_item) {
		return $this->db->query("SELECT * FROM tabel_stok_toko WHERE kd_barang='$kd_barang_item'");
	}

	public function getPorsi($kd_barang_item) {
		return $this->db->query("SELECT * FROM tabel_barang WHERE kd_barang='$kd_barang_item'");
	}

	public function getDataMenu() {
		$this->db->order_by('kode_menu');
		return $this->db->get('tabel_menu');
	}

	public function getDetailMenu() {
		$this->db->select('a.*,if(b.kode_menu is null,0,COUNT(b.kode_menu)) AS item_bahan');
		$this->db->from('tabel_menu a');
		$this->db->join('tabel_rinci_menu b', 'a.kode_menu=b.kode_menu','left');
		$this->db->join('tabel_barang c', 'b.kode_bahan=c.kd_barang','left');
		$this->db->group_by('a.kode_menu');
		$query = $this->db->get();
		return $query;
	}
	
	public function getDetailUnit() {
		$this->db->select('a.*,if(b.id_operator is null,0,GROUP_CONCAT(c.nm_operator)) AS nm_operator');
		$this->db->from('tabel_unit a');
		$this->db->join('tabel_rinci_unit b', 'a.no_spbu=b.no_spbu','left');
		$this->db->join('tabel_operator c', 'b.id_operator=c.id_operator','left');
		$this->db->group_by('a.id_unit');
		$query = $this->db->get();
		return $query;
	}

	public function getBahanUtama() {
		$this->db->where('kd_kategori', 'K001');
		return $this->db->get('tabel_barang');
	}

	public function getBahanTambahan() {
		$this->db->where('kd_kategori', 'K002');
		return $this->db->get('tabel_barang');
	}
	
	public function save_unit($no_area, $nm_cabang, $no_spbu, $alamat, $operator) {
		$this->db->trans_start();
		$id_unit = $this->db->insert_id();
		$result = array();
		if($operator) {
		foreach ($operator AS $key => $val) {
			$result[] = array(
				'no_spbu' => $no_spbu,
				'id_operator' => $_POST['operator'][$key],
			);
		}
		$this->db->insert_batch('tabel_rinci_unit', $result);
		$data = array(
				'no_area' => $no_area,
				'nm_cabang' => $nm_cabang,
				'no_spbu' => $no_spbu,
				'alamat' => $alamat,
			);
		}
		$data = array(
				'no_area' => $no_area,
				'nm_cabang' => $nm_cabang,
				'no_spbu' => $no_spbu,
				'alamat' => $alamat,
			);
		$this->db->insert('tabel_unit', $data);
		$this->db->trans_complete();
		
	}
	
	public function save_edit_unit($no_area, $nm_cabang, $no_spbu, $alamat, $operator) {
		$this->db->trans_start();
		$this->db->delete('tabel_rinci_unit', array('no_spbu' => $no_spbu));

		$result = array();
		if($operator) {
		foreach ($operator AS $key => $val) {
			$result[] = array(
				'no_spbu' => $no_spbu,
				'id_operator' => $_POST['operator_e'][$key],
			);
		}
		$this->db->insert_batch('tabel_rinci_unit', $result);
		$data = array(
				'no_area' => $no_area,
				'nm_cabang' => $nm_cabang,
				'no_spbu' => $no_spbu,
				'alamat' => $alamat,
		);
		
		} 
		$data = array(
				'no_area' => $no_area,
				'nm_cabang' => $nm_cabang,
				'no_spbu' => $no_spbu,
				'alamat' => $alamat,
		);
		
		$this->db->where('no_spbu', $no_spbu);
		$this->db->update('tabel_unit',$data);

		$this->db->trans_complete();
	}
	
	public function delete_unit($no_spbu) {
		$this->db->trans_start();
		$this->db->delete('tabel_unit', array('no_spbu' => $no_spbu));
		$this->db->delete('tabel_rinci_unit', array('no_spbu' => $no_spbu));
		$this->db->trans_complete();
	}
	


	public function save_menu($kode_menu, $nama_menu, $bahan_utama, $bahan_tambahan, $harga_jual) {
		$this->db->trans_start();
		$id_menu = $this->db->insert_id();
		$result = array();
		if($bahan_utama) {
		foreach ($bahan_utama AS $key => $val) {
			$result[] = array(
				'kode_menu' => $kode_menu,
				'kode_bahan' => $_POST['bahan_utama'][$key],
			);
		}
		$this->db->insert_batch('tabel_rinci_menu', $result);

		
		if ($bahan_tambahan) {
			$result2 = array();
			foreach ($bahan_tambahan AS $key => $val) {
				$result2[] = array(
					'kode_menu' => $kode_menu,
					'kode_bahan' => $_POST['bahan_tambahan'][$key],
				);
			}
			$this->db->insert_batch('tabel_rinci_menu', $result2);
		}
		$jum = $this->db->query("SELECT SUM(a.modal_per_porsi) AS tot_mod FROM tabel_barang AS a JOIN tabel_rinci_menu AS b ON a.kd_barang=b.kode_bahan WHERE kode_menu='$kode_menu'");
		$x = $jum->row_array();
		$harga_modal = $x['tot_mod'];

		$data = array(
			'kode_menu' => $kode_menu,
			'nama_menu' => $nama_menu,
			'harga_jual' => $harga_jual,
			'harga_modal' => $harga_modal,
		);
		} else {
		$data = array(
			'kode_menu' => $kode_menu,
			'nama_menu' => $nama_menu,
			'harga_jual' => $harga_jual,
			);
		}
		$this->db->insert('tabel_menu', $data);
		$this->db->trans_complete();
		
	}

	public function save_edit_menu($kode_menu, $nama_menu, $bahan_utama, $bahan_tambahan, $harga_jual) {
		$this->db->trans_start();
		$this->db->delete('tabel_rinci_menu', array('kode_menu' => $kode_menu));

		$result = array();
		if ($bahan_utama) {
		foreach ($bahan_utama AS $key => $val) {
			$result[] = array(
				'kode_menu' => $kode_menu,
				'kode_bahan' => $_POST['bahan_utama_e'][$key],
			);
		}
		$this->db->insert_batch('tabel_rinci_menu', $result);
		if ($bahan_tambahan) {
			$result2 = array();
			foreach ($bahan_tambahan AS $key => $val) {
				$result2[] = array(
					'kode_menu' => $kode_menu,
					'kode_bahan' => $_POST['bahan_tambahan_e'][$key],
				);
			}
			$this->db->insert_batch('tabel_rinci_menu', $result2);
		}

		$jum = $this->db->query("SELECT SUM(a.modal_per_porsi) AS tot_mod FROM tabel_barang AS a JOIN tabel_rinci_menu AS b ON a.kd_barang=b.kode_bahan WHERE kode_menu='$kode_menu'");
		$x = $jum->row_array();
		$harga_modal = $x['tot_mod'];
		$data = array(
			'nama_menu' => $nama_menu,
			'harga_jual' => $harga_jual,
			'harga_modal' => $harga_modal,
		);
		} else {
		$data = array(
			'nama_menu' => $nama_menu,
			'harga_jual' => $harga_jual,
			);
		}
		$this->db->where('kode_menu', $kode_menu);
		$this->db->update('tabel_menu', $data);

		$this->db->trans_complete();
	}
	
	public function getUnit() {
		return $this->db->get('tabel_unit');
	}
	
	public function getSupplier1() {
		return $this->db->get('tabel_supplier');
	}
	
	public function cekUnit($no_spbu) {
		return $this->db->query("SELECT no_spbu FROM tabel_unit WHERE no_spbu='$no_spbu'");
	}

	public function cekKodeMenu($kode_menu) {
		$this->db->where('kode_menu', $kode_menu);
		return $this->db->get('tabel_menu');
	}

	public function get_bahan_by_menu($kode_menu) {
		$this->db->select('*');
		$this->db->from('tabel_barang AS a');
		$this->db->join('tabel_rinci_menu AS b', 'b.kode_bahan=a.kd_barang');
		$this->db->join('tabel_menu AS c', 'b.kode_menu=c.kode_menu');
		$this->db->where('c.kode_menu', $kode_menu);
		$query = $this->db->get();
		return $query;
	}
	
	public function get_operator_by_unit($no_spbu) {
		$this->db->select('*');
		$this->db->from('tabel_operator AS a');
		$this->db->join('tabel_rinci_unit AS b', 'b.id_operator=a.id_operator');
		$this->db->join('tabel_unit AS c', 'b.no_spbu=c.no_spbu');
		$this->db->where('c.no_spbu', $no_spbu);
		$query = $this->db->get();
		return $query;
	}

	public function delete_menu($kode_menu) {
		$this->db->trans_start();
		$this->db->delete('tabel_menu', array('kode_menu' => $kode_menu));
		$this->db->delete('tabel_rinci_menu', array('kode_menu' => $kode_menu));
		$this->db->trans_complete();
	}
	
	public function delete_satuan($kode) {
		$this->db->trans_start();
		$this->db->delete('tabel_satuan_barang', array('kd_satuan' => $kode));
		$this->db->trans_complete();
	}
	public function delete_sumber($id) {
		$this->db->trans_start();
		$this->db->delete('tabel_sumber_biaya', array('id' => $id));
		$this->db->trans_complete();
	}
	
	public function delete_operator($kode) {
		$this->db->trans_start();
		$this->db->delete('tabel_operator', array('id_operator' => $kode));
		$this->db->trans_complete();
	}

	public function get_detail_bahan($kd_bahan) {
		$hsl = $this->db->query("SELECT tabel_stok_toko.stok, tabel_barang.nm_barang, tabel_barang.hrg_beli, tabel_barang.hrg_jual FROM tabel_barang LEFT JOIN tabel_stok_toko ON tabel_barang.kd_barang = tabel_stok_toko.kd_barang WHERE tabel_barang.kd_barang='$kd_bahan'");
		if ($hsl->num_rows() > 0) {
			foreach ($hsl->result() as $data) {
				$hasil = array(
					'nm_barang' => $data->nm_barang,
					'stok' => $data->stok,
				);
			}
		}
		return $hasil;
	}

	public function cekStok($kd_bahan) {
		$this->db->where('kd_barang', $kd_bahan);
		return $this->db->get('tabel_stok_toko');
	}
	
	public function cekStokCabang($spbu2) {
		$this->db->where('no_spbu', $spbu2);
		return $this->db->get('tabel_stok_cabang');
	}
	
	public function getKDSupplier() {
		//return $this->db->query("SELECT * FROM tabel_pembelian WHERE  selesai=1 AND kliring=0");
	return $this->db->query("SELECT 
		a.no_faktur_pembelian AS no_faktur_pembelian,
		a.tgl_pembelian AS tgl_pembelian,
		a.total_pembelian AS total_pembelian,
		a.kd_supplier AS kd_supplier1,
		b.nominal AS nominal,
		b.kd_supplier AS kd_supplier2,
		b.tgl_bayar AS tgl_bayar,
		c.nm_supplier AS nm_supplier
		FROM tabel_pembelian a
		LEFT JOIN tabel_bayar b ON a.kd_supplier=b.kd_supplier
		LEFT JOIN tabel_supplier c ON b.kd_supplier=c.kd_supplier
		WHERE a.selesai=1 AND a.kliring=0 
		GROUP BY a.kd_supplier
		 "); 
	}
	
	public function getSortKDSupplier($kdsup) {
		return $this->db->query("SELECT 
		a.no_faktur_pembelian AS no_faktur_pembelian,
		a.tgl_pembelian AS tgl_pembelian,
		a.total_pembelian AS total_pembelian,
		a.kd_supplier AS kd_supplier1,
		b.id_bayar AS id_bayar,
		b.nominal AS nominal,
		b.kd_supplier AS kd_supplier2,
		b.tgl_bayar AS tgl_bayar
		FROM tabel_pembelian a
		LEFT JOIN tabel_bayar b ON a.kd_supplier=b.kd_supplier
		LEFT JOIN tabel_supplier c ON b.kd_supplier=c.kd_supplier
		WHERE c.kd_supplier='".$kdsup."' AND a.kliring=0 AND a.selesai=1
		GROUP BY a.no_faktur_pembelian
		");
	}
	
	public function getSortKDSupplier1($kdsup) {
		return $this->db->query("SELECT 
		a.no_faktur_pembelian AS no_faktur_pembelian,
		a.tgl_pembelian AS tgl_pembelian,
		a.total_pembelian AS total_pembelian,
		a.kd_supplier AS kd_supplier1,
		b.id_bayar AS id_bayar,
		b.nominal AS nominal,
		b.kd_supplier AS kd_supplier2,
		b.tgl_bayar AS tgl_bayar
		FROM tabel_pembelian a
		LEFT JOIN tabel_bayar b ON a.kd_supplier=b.kd_supplier
		LEFT JOIN tabel_supplier c ON b.kd_supplier=c.kd_supplier
		WHERE c.kd_supplier='".$kdsup."' AND b.kliring=0
		GROUP BY b.id_bayar
		");
	}
	
	public function getSortKDSupplier2($kdsup) {
		return $this->db->query("SELECT 
		a.no_faktur_pembelian AS no_faktur_pembelian,
		d.tgl_pembelian AS tgl_pembelian1,
		a.total_pembelian AS total_pembelian,
		a.kd_supplier AS kd_supplier1,
		b.id_bayar AS id_bayar,
		b.nominal AS nominal,
		b.kd_supplier AS kd_supplier2,
		d.tgl_bayar AS tgl_bayar1,
		d.id_kliring AS id_kliring,
		d.kd_supplier AS kd_supplier3,
		d.selisih AS selisih
		FROM tabel_pembelian a
		LEFT JOIN tabel_bayar b ON a.kd_supplier=b.kd_supplier
		LEFT JOIN tabel_supplier c ON b.kd_supplier=c.kd_supplier
		LEFT JOIN tabel_kliring_bayar d ON a.kd_supplier=d.kd_supplier
		WHERE c.kd_supplier='".$kdsup."' AND d.kliring=0
		GROUP BY d.id_kliring
		");
	}
	
	function getNoFaktur1($nofaktur){
        $hasil=$this->db->query("SELECT * FROM tabel_pembelian WHERE no_faktur_pembelian='$nofaktur'");
        return $hasil->result();
    }
	
	function getBayar1($id){
        $hasil=$this->db->query("SELECT * FROM tabel_bayar WHERE id_bayar='$id'");
        return $hasil->result();
    }
	
	function getKliring1($id){
        $hasil=$this->db->query("SELECT * FROM tabel_kliring_bayar WHERE id_kliring='$id'");
        return $hasil->result();
    }
}

/* End of file Gudang_model.php */
/* Location: ./application/models/Gudang_model.php */