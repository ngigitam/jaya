<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan_model extends CI_Model {

	public function get_toko() {
		$query = $this->db->query("SELECT * FROM tabel_toko LIMIT 1");
		return $query->row();
	}

	public function getDataRetur($tanggal) {
		$this->db->where('tgl', $tanggal);
		return $this->db->get('tabel_retur');
	}

	public function getDataReturFilter($tgl_awal, $tgl_akhir) {
		$this->db->where('tgl BETWEEN "' . date('Y-m-d', strtotime($tgl_awal)) . '" and "' . date('Y-m-d', strtotime($tgl_akhir)) . '"');
		return $this->db->get('tabel_retur');
	}

	public function getDataPengeluaranRekap($tgl_awal, $tgl_akhir) {
		return $this->db->select('*')
			->where('tgl BETWEEN "' . date('Y-m-d', strtotime($tgl_awal)) . '" and "' . date('Y-m-d', strtotime($tgl_akhir)) . '"')
			->group_by('tgl')
			->get('tabel_biaya');
	}

	public function getDataPengeluaranRinci($tgl_awal, $tgl_akhir) {
		return $this->db->select('*')
			->where('tgl BETWEEN "' . date('Y-m-d', strtotime($tgl_awal)) . '" and "' . date('Y-m-d', strtotime($tgl_akhir)) . '"')
			->get('tabel_biaya');
	}

	public function getNilaiPersediaan() {
		return $this->db->select('a.kd_barang,a.nm_barang,a.hrg_jual,a.hrg_beli,b.stok,a.modal_per_porsi')
			->join('tabel_stok_toko AS b', 'a.kd_barang = b.kd_barang', 'LEFT')
			->where('b.stok > 0')
			->order_by('a.kd_barang')
			->get('tabel_barang AS a');
	}

	public function getDataPembelian($tanggal) {
		return $this->db->select('a.no_faktur_pembelian,a.tgl_pembelian,b.kd_barang,b.nm_barang,b.satuan,b.jumlah,b.harga,b.sub_total_beli')
			->join('tabel_rinci_pembelian AS b', 'a.no_faktur_pembelian = b.no_faktur_pembelian')
			->where('a.tgl_pembelian', $tanggal)
			->where('a.selesai', '1')
			->order_by('a.no_faktur_pembelian')
			->get('tabel_pembelian AS a');
	}

	public function getDataPembelianFilter($tgl_awal, $tgl_akhir) {
		return $this->db->select('a.no_faktur_pembelian,a.tgl_pembelian,b.kd_barang,b.nm_barang,b.satuan,b.jumlah,b.harga,b.sub_total_beli')
			->join('tabel_rinci_pembelian AS b', 'a.no_faktur_pembelian = b.no_faktur_pembelian')
			->where('a.tgl_pembelian BETWEEN "' . date('Y-m-d', strtotime($tgl_awal)) . '" and "' . date('Y-m-d', strtotime($tgl_akhir)) . '"')
			->where('a.selesai', '1')
			->get('tabel_pembelian AS a');
	}

	public function getDataPenjualanTransaksiFilter($tgl_awal, $tgl_akhir) {
		return $this->db->select('a.*,b.*')
			->join('tabel_rinci_penjualan AS b', 'a.no_faktur_penjualan = b.no_faktur_penjualan')
			->where('a.tgl_penjualan BETWEEN "' . date('Y-m-d', strtotime($tgl_awal)) . '" and "' . date('Y-m-d', strtotime($tgl_akhir)) . '"')
			->where('EXISTS (SELECT 1 FROM tabel_rinci_penjualan AS b WHERE a.no_faktur_penjualan=b.no_faktur_penjualan)')
			->where('a.selesai', '1')
			->group_by('a.no_faktur_penjualan')
			->get('tabel_penjualan AS a');
	}

	public function getDataPenjualanTransaksi($tanggal) {
		return $this->db->select('a.*,b.*')
			->join('tabel_rinci_penjualan AS b', 'a.no_faktur_penjualan = b.no_faktur_penjualan')
			->where('a.tgl_penjualan', $tanggal)
			->where('EXISTS (SELECT 1 FROM tabel_rinci_penjualan AS b WHERE a.no_faktur_penjualan=b.no_faktur_penjualan)')
			->where('a.selesai', '1')
			->group_by('a.no_faktur_penjualan')
			->get('tabel_penjualan AS a');
	}
	
	public function getDataPembelian1Filter($tgl_awal, $tgl_akhir) {
		return $this->db->select('a.*,
		YEAR(a.tgl_pembelian) AS Y,MONTH(a.tgl_pembelian) AS M,
		b.*,c.*,d.*')
			->join('tabel_rinci_pembelian AS b', 'a.no_faktur_pembelian = b.no_faktur_pembelian')
			->join('tabel_unit AS c', 'b.no_spbu = c.no_spbu')
			->join('tabel_supplier AS d', 'a.kd_supplier = d.kd_supplier')
			->where('a.tgl_pembelian BETWEEN "' . date('Y-m-d', strtotime($tgl_awal)) . '" and "' . date('Y-m-d', strtotime($tgl_akhir)) . '"')
			//->where('EXISTS (SELECT 1 FROM tabel_rinci_pembelian AS b WHERE a.no_faktur_pembelian=b.no_faktur_pembelian)')
			->where('a.selesai', '1')
			->group_by('a.no_faktur_pembelian')
			->get('tabel_pembelian AS a');
	}
	
	public function getDataPembelian1($tanggal) {
		return $this->db->select('a.*,
		YEAR(a.tgl_pembelian) AS Y,MONTH(a.tgl_pembelian) AS M,
		b.*,c.*,d.*')
			->join('tabel_rinci_pembelian AS b', 'a.no_faktur_pembelian = b.no_faktur_pembelian')
			->join('tabel_unit AS c', 'b.no_spbu = c.no_spbu')
			->join('tabel_supplier AS d', 'a.kd_supplier = d.kd_supplier')
			->where('a.tgl_pembelian', $tanggal)
			->where('EXISTS (SELECT 1 FROM tabel_rinci_pembelian AS b WHERE a.no_faktur_pembelian=b.no_faktur_pembelian)')
			->where('a.selesai', '1')
			->group_by('a.no_faktur_pembelian')
			->get('tabel_pembelian AS a');
	}
	
	
	public function getDataPembelian2Filter($tgl_awal, $tgl_akhir) {
		return $this->db->select('a.*,
		sum(b.sub_total_beli) As total_pembelian,
		c.*')
			->join('tabel_rinci_pembelian AS b', 'a.no_faktur_pembelian = b.no_faktur_pembelian')
			->join('tabel_unit AS c', 'b.no_spbu = c.no_spbu')
			->where('a.tgl_pembelian BETWEEN "' . date('Y-m-d', strtotime($tgl_awal)) . '" and "' . date('Y-m-d', strtotime($tgl_akhir)) . '"')
			//->where('EXISTS (SELECT 1 FROM tabel_rinci_pembelian AS b WHERE a.no_faktur_pembelian=b.no_faktur_pembelian)')
			->where('a.selesai', '1')
			->group_by('b.no_spbu')
			->get('tabel_pembelian AS a');
	}
	
	public function getDataPembelian2($tanggal) {
		return $this->db->select('a.*,
		sum(b.sub_total_beli) As total_pembelian,
		c.*')
			->join('tabel_rinci_pembelian AS b', 'a.no_faktur_pembelian = b.no_faktur_pembelian')
			->join('tabel_unit AS c', 'b.no_spbu = c.no_spbu')
			->where('a.tgl_pembelian', $tanggal)
			->where('a.selesai', '1')
			->group_by('b.no_spbu')
			->get('tabel_pembelian AS a');
	}
	
	public function getDataPembelian3Filter($tgl_awal, $tgl_akhir) {
		return $this->db->select('a.*,
		sum(b.sub_total_beli) As total_pembelian,
		c.*,d.*')
			->join('tabel_rinci_pembelian AS b', 'a.no_faktur_pembelian = b.no_faktur_pembelian')
			->join('tabel_unit AS c', 'b.no_spbu = c.no_spbu')
			->join('tabel_supplier AS d', 'a.kd_supplier = d.kd_supplier')
			->where('a.tgl_pembelian BETWEEN "' . date('Y-m-d', strtotime($tgl_awal)) . '" and "' . date('Y-m-d', strtotime($tgl_akhir)) . '"')
			//->where('EXISTS (SELECT 1 FROM tabel_rinci_pembelian AS b WHERE a.no_faktur_pembelian=b.no_faktur_pembelian)')
			->where('a.selesai', '1')
			->group_by('a.kd_supplier')
			->get('tabel_pembelian AS a');
	}
	
	public function getDataPembelian3($tanggal) {
		return $this->db->select('a.*,
		sum(b.sub_total_beli) As total_pembelian,
		c.*,d.*')
			->join('tabel_rinci_pembelian AS b', 'a.no_faktur_pembelian = b.no_faktur_pembelian')
			->join('tabel_unit AS c', 'b.no_spbu = c.no_spbu')
			->join('tabel_supplier AS d', 'a.kd_supplier = d.kd_supplier')
			->where('a.tgl_pembelian', $tanggal)
			->where('a.selesai', '1')
			->group_by('a.kd_supplier')
			->get('tabel_pembelian AS a');
	}
	
	public function getDataHutang1Filter($tgl_awal, $tgl_akhir) {
		return $this->db->select('a.*,
		YEAR(a.tgl_pembelian) AS Y, MONTH(a.tgl_pembelian) AS M,
		b.*,c.*,d.*')
			->join('tabel_rinci_pembelian AS b', 'a.no_faktur_pembelian = b.no_faktur_pembelian')
			->join('tabel_unit AS c', 'b.no_spbu = c.no_spbu')
			->join('tabel_supplier AS d', 'a.kd_supplier = d.kd_supplier')
			->where('a.tgl_pembelian BETWEEN "' . date('Y-m-d', strtotime($tgl_awal)) . '" and "' . date('Y-m-d', strtotime($tgl_akhir)) . '"')
			->where('EXISTS (SELECT 1 FROM tabel_rinci_pembelian AS b WHERE a.no_faktur_pembelian=b.no_faktur_pembelian)')
			->where('a.selesai', '1')
			->where('a.kliring', '0')
			->group_by('a.no_faktur_pembelian')
			->get('tabel_pembelian AS a');
	}
	
	public function getDataHutang1($tanggal) {
		return $this->db->select('a.*,
		YEAR(a.tgl_pembelian) AS Y,MONTH(a.tgl_pembelian) AS M,
		b.*,c.*,d.*')
			->join('tabel_rinci_pembelian AS b', 'a.no_faktur_pembelian = b.no_faktur_pembelian')
			->join('tabel_unit AS c', 'b.no_spbu = c.no_spbu')
			->join('tabel_supplier AS d', 'a.kd_supplier = d.kd_supplier')
			->where('a.tgl_pembelian', $tanggal)
			->where('EXISTS (SELECT 1 FROM tabel_rinci_pembelian AS b WHERE a.no_faktur_pembelian=b.no_faktur_pembelian)')
			->where('a.selesai', '1')
			->where('a.kliring', '0')
			->group_by('a.no_faktur_pembelian')
			->get('tabel_pembelian AS a');
	}
	
	public function getDataHutang2Filter($tgl_awal, $tgl_akhir) {
		return $this->db->select('a.*,
		sum(b.sub_total_beli) As total_pembelian,
		c.*,d.*')
			->join('tabel_rinci_pembelian AS b', 'a.no_faktur_pembelian = b.no_faktur_pembelian')
			->join('tabel_unit AS c', 'b.no_spbu = c.no_spbu')
			->join('tabel_supplier AS d', 'a.kd_supplier = d.kd_supplier')
			->where('a.tgl_pembelian BETWEEN "' . date('Y-m-d', strtotime($tgl_awal)) . '" and "' . date('Y-m-d', strtotime($tgl_akhir)) . '"')
			//->where('EXISTS (SELECT 1 FROM tabel_rinci_pembelian AS b WHERE a.no_faktur_pembelian=b.no_faktur_pembelian)')
			->where('a.selesai', '1')
			->where('a.kliring', '0')
			->group_by('a.kd_supplier')
			->get('tabel_pembelian AS a');
	}
	
	public function getDataHutang2($tanggal) {
		return $this->db->select('a.*,
		sum(b.sub_total_beli) As total_pembelian,
		c.*,d.*')
			->join('tabel_rinci_pembelian AS b', 'a.no_faktur_pembelian = b.no_faktur_pembelian')
			->join('tabel_unit AS c', 'b.no_spbu = c.no_spbu')
			->join('tabel_supplier AS d', 'a.kd_supplier = d.kd_supplier')
			->where('a.tgl_pembelian', $tanggal)
			->where('a.selesai', '1')
			->where('a.kliring', '0')
			->group_by('a.kd_supplier')
			->get('tabel_pembelian AS a');
	}
	
	

	public function getDataPenjualan1Filter($tgl_awal, $tgl_akhir) {
		return $this->db->select('a.*,
		YEAR(a.tgl_penjualan) AS Y,MONTH(a.tgl_penjualan) AS M,
		b.*,c.*')
			->join('tabel_rinci_penjualan AS b', 'a.no_faktur_penjualan = b.no_faktur_penjualan')
			->join('tabel_unit AS c', 'a.no_spbu = c.no_spbu')
			->where('a.tgl_penjualan BETWEEN "' . date('Y-m-d', strtotime($tgl_awal)) . '" and "' . date('Y-m-d', strtotime($tgl_akhir)) . '"')
			->where('EXISTS (SELECT 1 FROM tabel_rinci_penjualan AS b WHERE a.no_faktur_penjualan=b.no_faktur_penjualan)')
			->where('a.selesai', '1')
			->group_by('a.no_faktur_penjualan')
			->get('tabel_penjualan AS a');
	}
	
	public function getDataPenjualan1($tanggal) {
		return $this->db->select('a.*,
		YEAR(a.tgl_penjualan) AS Y,MONTH(a.tgl_penjualan) AS M,
		b.*,c.*')
			->join('tabel_rinci_penjualan AS b', 'a.no_faktur_penjualan = b.no_faktur_penjualan')
			->join('tabel_unit AS c', 'a.no_spbu = c.no_spbu')
			->where('a.tgl_penjualan', $tanggal)
			->where('EXISTS (SELECT 1 FROM tabel_rinci_penjualan AS b WHERE a.no_faktur_penjualan=b.no_faktur_penjualan)')
			->where('a.selesai', '1')
			->group_by('a.no_faktur_penjualan')
			->get('tabel_penjualan AS a');
	}
	
	public function getDataPenjualan2Filter($tgl_awal, $tgl_akhir) {
		return $this->db->select('a.*,
		sum(a.total_penjualan) As total_penjualan,
		c.*')
			//->join('tabel_rinci_penjualan AS b', 'a.no_faktur_penjualan = b.no_faktur_penjualan')
			->join('tabel_unit AS c', 'a.no_spbu = c.no_spbu')
			->where('a.tgl_penjualan BETWEEN "' . date('Y-m-d', strtotime($tgl_awal)) . '" and "' . date('Y-m-d', strtotime($tgl_akhir)) . '"')
			//->where('EXISTS (SELECT 1 FROM tabel_rinci_penjualan AS b WHERE a.no_faktur_penjualan=b.no_faktur_penjualan)')
			->where('a.selesai', '1')
			->group_by('a.no_spbu')
			->get('tabel_penjualan AS a');
	}
	
	public function getDataPenjualan2($tanggal) {
		return $this->db->select('a.*,
		sum(a.total_penjualan) As total_penjualan,
		c.*')
			->join('tabel_unit AS c', 'a.no_spbu = c.no_spbu')
			->where('a.tgl_penjualan', $tanggal)
			->where('a.selesai', '1')
			->group_by('a.no_spbu')
			->get('tabel_penjualan AS a');
	}
	
	public function getDataPenjualan3Filter($tgl_awal, $tgl_akhir) {
		return $this->db->select('a.*,
		SUM(b.sub_total_jual) AS sub_total_jual1,
		b.*')
			->join('tabel_rinci_penjualan AS b', 'a.no_faktur_penjualan = b.no_faktur_penjualan')
			->where('a.tgl_penjualan BETWEEN "' . date('Y-m-d', strtotime($tgl_awal)) . '" and "' . date('Y-m-d', strtotime($tgl_akhir)) . '"')
			->where('EXISTS (SELECT 1 FROM tabel_rinci_penjualan AS b WHERE a.no_faktur_penjualan=b.no_faktur_penjualan)')
			->where('a.selesai', '1')
			->group_by('b.kd_barang')
			->get('tabel_penjualan AS a');
	}
	
	public function getDataPenjualan3($tanggal) {
		return $this->db->select('a.*,
		SUM(b.sub_total_jual) AS sub_total_jual1,
		b.*')
			->join('tabel_rinci_penjualan AS b', 'a.no_faktur_penjualan = b.no_faktur_penjualan')
			->where('a.tgl_penjualan', $tanggal)
			->where('EXISTS (SELECT 1 FROM tabel_rinci_penjualan AS b WHERE a.no_faktur_penjualan=b.no_faktur_penjualan)')
			->where('a.selesai', '1')
			->group_by('b.nm_barang')
			->get('tabel_penjualan AS a');
	}
	
	public function getHistoryPenjualan2Filter($tgl_awal, $tgl_akhir) {
		return $this->db->select('
		IFNULL(c.nm_cabang,"Grand Total") AS nm_c1, 
		SUM(IF(MONTH(tgl_penjualan)=1,a.total_penjualan,0)) AS bln_Januari,
		SUM(IF(MONTH(tgl_penjualan)=2,a.total_penjualan,0)) AS bln_Februari,
		SUM(IF(MONTH(tgl_penjualan)=3,a.total_penjualan,0)) AS bln_Maret,
		SUM(IF(MONTH(tgl_penjualan)=4,a.total_penjualan,0)) AS bln_April,
		SUM(IF(MONTH(tgl_penjualan)=5,a.total_penjualan,0)) AS bln_Mei,
		SUM(IF(MONTH(tgl_penjualan)=6,a.total_penjualan,0)) AS bln_Juni,
		SUM(IF(MONTH(tgl_penjualan)=7,a.total_penjualan,0)) AS bln_Juli,
		SUM(IF(MONTH(tgl_penjualan)=8,a.total_penjualan,0)) AS bln_Agustus,
		SUM(IF(MONTH(tgl_penjualan)=9,a.total_penjualan,0)) AS bln_September,
		SUM(IF(MONTH(tgl_penjualan)=10,a.total_penjualan,0)) AS bln_Oktober,
		SUM(IF(MONTH(tgl_penjualan)=11,a.total_penjualan,0)) AS bln_November,
		SUM(IF(MONTH(tgl_penjualan)=12,a.total_penjualan,0)) AS bln_Desember,
		sum(a.total_penjualan) As total_penjualan
		')
			->join('tabel_unit AS c', 'a.no_spbu = c.no_spbu')
			->where('a.tgl_penjualan BETWEEN "' . date('Y-m-d', strtotime($tgl_awal)) . '" and "' . date('Y-m-d', strtotime($tgl_akhir)) . '"')
			->where('a.selesai', '1')
			->group_by('c.nm_cabang')
			->order_by('sum(a.total_penjualan) Desc')
			->get('tabel_penjualan AS a');

	}
	
	public function getHistoryPenjualan2($tanggal) {
		return $this->db->select('
		IFNULL(c.nm_cabang,"Grand Total") AS nm_c1, 
		SUM(IF(MONTH(tgl_penjualan)=1,a.total_penjualan,0)) AS bln_Januari,
		SUM(IF(MONTH(tgl_penjualan)=2,a.total_penjualan,0)) AS bln_Februari,
		SUM(IF(MONTH(tgl_penjualan)=3,a.total_penjualan,0)) AS bln_Maret,
		SUM(IF(MONTH(tgl_penjualan)=4,a.total_penjualan,0)) AS bln_April,
		SUM(IF(MONTH(tgl_penjualan)=5,a.total_penjualan,0)) AS bln_Mei,
		SUM(IF(MONTH(tgl_penjualan)=6,a.total_penjualan,0)) AS bln_Juni,
		SUM(IF(MONTH(tgl_penjualan)=7,a.total_penjualan,0)) AS bln_Juli,
		SUM(IF(MONTH(tgl_penjualan)=8,a.total_penjualan,0)) AS bln_Agustus,
		SUM(IF(MONTH(tgl_penjualan)=9,a.total_penjualan,0)) AS bln_September,
		SUM(IF(MONTH(tgl_penjualan)=10,a.total_penjualan,0)) AS bln_Oktober,
		SUM(IF(MONTH(tgl_penjualan)=11,a.total_penjualan,0)) AS bln_November,
		SUM(IF(MONTH(tgl_penjualan)=12,a.total_penjualan,0)) AS bln_Desember,
		sum(a.total_penjualan) As total_penjualan
		')
			
			->join('tabel_unit AS c', 'a.no_spbu = c.no_spbu')
			->where('a.tgl_penjualan', $tanggal)
			->where('a.selesai', '1')
			->group_by('c.nm_cabang')
			->order_by('sum(a.total_penjualan) Desc')
			->get('tabel_penjualan AS a');
	}
	
	public function getHistoryPenjualan3Filter($tgl_awal, $tgl_akhir) {
		return $this->db->select('
		IFNULL(b.nm_barang,"Grand Total") AS nm_c1, 
		SUM(IF(MONTH(tgl_penjualan)=1,b.sub_total_jual,0)) AS bln_Januari,
		SUM(IF(MONTH(tgl_penjualan)=2,b.sub_total_jual,0)) AS bln_Februari,
		SUM(IF(MONTH(tgl_penjualan)=3,b.sub_total_jual,0)) AS bln_Maret,
		SUM(IF(MONTH(tgl_penjualan)=4,b.sub_total_jual,0)) AS bln_April,
		SUM(IF(MONTH(tgl_penjualan)=5,b.sub_total_jual,0)) AS bln_Mei,
		SUM(IF(MONTH(tgl_penjualan)=6,b.sub_total_jual,0)) AS bln_Juni,
		SUM(IF(MONTH(tgl_penjualan)=7,b.sub_total_jual,0)) AS bln_Juli,
		SUM(IF(MONTH(tgl_penjualan)=8,b.sub_total_jual,0)) AS bln_Agustus,
		SUM(IF(MONTH(tgl_penjualan)=9,b.sub_total_jual,0)) AS bln_September,
		SUM(IF(MONTH(tgl_penjualan)=10,b.sub_total_jual,0)) AS bln_Oktober,
		SUM(IF(MONTH(tgl_penjualan)=11,b.sub_total_jual,0)) AS bln_November,
		SUM(IF(MONTH(tgl_penjualan)=12,b.sub_total_jual,0)) AS bln_Desember,
		SUM(b.sub_total_jual) AS sub_total_jual1,
		')
			->join('tabel_rinci_penjualan AS b', 'a.no_faktur_penjualan = b.no_faktur_penjualan')
			->where('a.tgl_penjualan BETWEEN "' . date('Y-m-d', strtotime($tgl_awal)) . '" and "' . date('Y-m-d', strtotime($tgl_akhir)) . '"')
			//->where('EXISTS (SELECT 1 FROM tabel_rinci_penjualan AS b WHERE a.no_faktur_penjualan=b.no_faktur_penjualan)')
			->where('a.selesai', '1')
			->group_by('b.kd_barang')
			->order_by('sum(b.sub_total_jual) Desc')
			->get('tabel_penjualan AS a');
	}
	
	public function getHistoryPenjualan3($tanggal) {
		return $this->db->select('
		IFNULL(b.nm_barang,"Grand Total") AS nm_c1, 
		SUM(IF(MONTH(tgl_penjualan)=1,b.sub_total_jual,0)) AS bln_Januari,
		SUM(IF(MONTH(tgl_penjualan)=2,b.sub_total_jual,0)) AS bln_Februari,
		SUM(IF(MONTH(tgl_penjualan)=3,b.sub_total_jual,0)) AS bln_Maret,
		SUM(IF(MONTH(tgl_penjualan)=4,b.sub_total_jual,0)) AS bln_April,
		SUM(IF(MONTH(tgl_penjualan)=5,b.sub_total_jual,0)) AS bln_Mei,
		SUM(IF(MONTH(tgl_penjualan)=6,b.sub_total_jual,0)) AS bln_Juni,
		SUM(IF(MONTH(tgl_penjualan)=7,b.sub_total_jual,0)) AS bln_Juli,
		SUM(IF(MONTH(tgl_penjualan)=8,b.sub_total_jual,0)) AS bln_Agustus,
		SUM(IF(MONTH(tgl_penjualan)=9,b.sub_total_jual,0)) AS bln_September,
		SUM(IF(MONTH(tgl_penjualan)=10,b.sub_total_jual,0)) AS bln_Oktober,
		SUM(IF(MONTH(tgl_penjualan)=11,b.sub_total_jual,0)) AS bln_November,
		SUM(IF(MONTH(tgl_penjualan)=12,b.sub_total_jual,0)) AS bln_Desember,
		SUM(b.sub_total_jual) AS sub_total_jual1,
		')
			->join('tabel_rinci_penjualan AS b', 'a.no_faktur_penjualan = b.no_faktur_penjualan')
			->where('a.tgl_penjualan', $tanggal)
			->where('EXISTS (SELECT 1 FROM tabel_rinci_penjualan AS b WHERE a.no_faktur_penjualan=b.no_faktur_penjualan)')
			->where('a.selesai', '1')
			->group_by('b.nm_barang')
			->order_by('sum(b.sub_total_jual) Desc')
			->get('tabel_penjualan AS a');
	}
	
	public function getSetoranPenjualanFilter($tgl_awal, $tgl_akhir) {
		return $this->db->select('a.*,
		
		b.*')
			->join('tabel_unit b', 'a.no_spbu = b.no_spbu')
			->where('a.tgl_setor BETWEEN "' . date('Y-m-d', strtotime($tgl_awal)) . '" and "' . date('Y-m-d', strtotime($tgl_akhir)) . '"')
		//->where('EXISTS (SELECT 1 FROM tabel_rinci_penjualan AS b WHERE a.no_faktur_penjualan=b.no_faktur_penjualan)')
			//->where('a.kliring', '1')
			//->group_by('b.kd_barang')
			->get('tabel_setor AS a');
	}
	
	public function getSetoranPenjualan($tanggal) {
		return $this->db->select('a.*,
		
		b.*')
			->join('tabel_unit b', 'a.no_spbu = b.no_spbu')
			->where('a.tgl_setor', $tanggal)
			//->where('EXISTS (SELECT 1 FROM tabel_rinci_penjualan AS b WHERE a.no_faktur_penjualan=b.no_faktur_penjualan)')
			//->where('a.kliring', '1')
			//->group_by('b.nm_barang')
			->get('tabel_setor AS a');
	}
	
	public function getSetoranPenjualan2Filter($tgl_awal, $tgl_akhir) {
		return $this->db->select('a.*,
		c.*,
		sum(a.total_penjualan) AS total_penjualan1,
		d.selisih AS selisih
		')
			//->join('tabel_rinci_penjualan AS b', 'a.no_faktur_penjualan = b.no_faktur_penjualan')
			->join('tabel_unit c', 'a.no_spbu = c.no_spbu')
			->join('tabel_kliring d', 'a.no_spbu = d.no_spbu')
			->where('a.tgl_penjualan BETWEEN "' . date('Y-m-d', strtotime($tgl_awal)) . '" and "' . date('Y-m-d', strtotime($tgl_akhir)) . '"')
			//->where('EXISTS (SELECT 1 FROM tabel_rinci_penjualan AS b WHERE a.no_faktur_penjualan=b.no_faktur_penjualan)')
			//->where('EXISTS (SELECT 1 FROM tabel_kliring AS d WHERE a.no_faktur_penjualan=d.no_faktur_penjualan)')
			->where('a.selesai', '1')
			->where('a.kliring', '0')
			->where('d.kliring', '0')
			->group_by('a.no_spbu')
			->get('tabel_penjualan AS a');
	}
	
	public function getSetoranPenjualan2($tanggal) {
		return $this->db->select('a.*,
		c.*,
		sum(a.total_penjualan) AS total_penjualan1,
		d.selisih AS selisih
		')
			//->join('tabel_rinci_penjualan AS b', 'a.no_faktur_penjualan = b.no_faktur_penjualan')
			->join('tabel_unit c', 'a.no_spbu = c.no_spbu')
			->join('tabel_kliring d', 'a.no_spbu = d.no_spbu')
			->where('a.tgl_penjualan', $tanggal)
			//->where('EXISTS (SELECT 1 FROM tabel_rinci_penjualan AS b WHERE a.no_faktur_penjualan=b.no_faktur_penjualan)')
			->where('a.selesai', '1')
			->where('a.kliring', '0')
			->where('d.kliring', '0')
			->group_by('a.no_spbu')
			->get('tabel_penjualan AS a');
	}
	
	public function getSetoranPenjualan3Filter($tgl_awal, $tgl_akhir) {
		return $this->db->select('a.*,
		sum(a.nominal) AS nominal,
		b.*')
			->join('tabel_unit b', 'a.no_spbu = b.no_spbu')
			->where('a.tgl_setor BETWEEN "' . date('Y-m-d', strtotime($tgl_awal)) . '" and "' . date('Y-m-d', strtotime($tgl_akhir)) . '"')
		//->where('EXISTS (SELECT 1 FROM tabel_rinci_penjualan AS b WHERE a.no_faktur_penjualan=b.no_faktur_penjualan)')
			->where('a.kliring', '1')
			->group_by('a.no_spbu')
			->get('tabel_setor AS a');
	}
	
	public function getSetoranPenjualan3($tanggal) {
		return $this->db->select('a.*,
		sum(a.nominal) AS nominal,
		b.*')
			->join('tabel_unit b', 'a.no_spbu = b.no_spbu')
			->where('a.tgl_setor', $tanggal)
			//->where('EXISTS (SELECT 1 FROM tabel_rinci_penjualan AS b WHERE a.no_faktur_penjualan=b.no_faktur_penjualan)')
			->where('a.kliring', '1')
			->group_by('a.no_spbu')
			->get('tabel_setor AS a');
	}
	
	
	
	public function getOperator1Filter($tgl_awal, $tgl_akhir) {
		return $this->db->select('a.*,b.*,c.*,d.*')
			->join('tabel_rinci_penjualan AS b', 'a.no_faktur_penjualan = b.no_faktur_penjualan')
			->join('tabel_unit AS c', 'a.no_spbu = c.no_spbu')
			->join('tabel_operator AS d', 'a.id_operator = d.id_operator')
			->where('a.tgl_penjualan BETWEEN "' . date('Y-m-d', strtotime($tgl_awal)) . '" and "' . date('Y-m-d', strtotime($tgl_akhir)) . '"')
			->where('EXISTS (SELECT 1 FROM tabel_rinci_penjualan AS b WHERE a.no_faktur_penjualan=b.no_faktur_penjualan)')
			->where('a.selesai', '1')
			->group_by('a.no_faktur_penjualan')
			->get('tabel_penjualan AS a');
	}
	
	public function getOperator1($tanggal) {
		return $this->db->select('a.*,b.*,c.*,d.*')
			->join('tabel_rinci_penjualan AS b', 'a.no_faktur_penjualan = b.no_faktur_penjualan')
			->join('tabel_unit AS c', 'a.no_spbu = c.no_spbu')
			->join('tabel_operator AS d', 'a.id_operator = d.id_operator')
			->where('a.tgl_penjualan', $tanggal)
			->where('EXISTS (SELECT 1 FROM tabel_rinci_penjualan AS b WHERE a.no_faktur_penjualan=b.no_faktur_penjualan)')
			->where('a.selesai', '1')
			->group_by('a.no_faktur_penjualan')
			->get('tabel_penjualan AS a');
	
	}
	
	public function getOperator2Filter($tgl_awal, $tgl_akhir) {
		return $this->db->select('a.*,
		sum(a.total_penjualan) As total_penjualan,
		c.*,d.*')
			//->join('tabel_rinci_penjualan AS b', 'a.no_faktur_penjualan = b.no_faktur_penjualan')
			->join('tabel_unit AS c', 'a.no_spbu = c.no_spbu')
			->join('tabel_operator AS d', 'a.id_operator = d.id_operator')
			->where('a.tgl_penjualan BETWEEN "' . date('Y-m-d', strtotime($tgl_awal)) . '" and "' . date('Y-m-d', strtotime($tgl_akhir)) . '"')
			//->where('EXISTS (SELECT 1 FROM tabel_rinci_penjualan AS b WHERE a.no_faktur_penjualan=b.no_faktur_penjualan)')
			->where('a.selesai', '1')
			->group_by('(d.id_operator) Asc')
			->get('tabel_penjualan AS a');
	}
	
	public function getOperator2($tanggal) {
		return $this->db->select('a.*,
		sum(a.total_penjualan) As total_penjualan,
		c.*,d.*')
			//->join('tabel_rinci_penjualan AS b', 'a.no_faktur_penjualan = b.no_faktur_penjualan')
			->join('tabel_unit AS c', 'a.no_spbu = c.no_spbu')
			->join('tabel_operator AS d', 'a.id_operator = d.id_operator')
			->where('a.tgl_penjualan', $tanggal)
			//->where('EXISTS (SELECT 1 FROM tabel_rinci_penjualan AS b WHERE a.no_faktur_penjualan=b.no_faktur_penjualan)')
			->where('a.selesai', '1')
			->group_by('(d.id_operator) Asc')
			->get('tabel_penjualan AS a');
	
	}
	
	public function getOperator3Filter($tgl_awal, $tgl_akhir) {
		return $this->db->select('
		IFNULL(d.nm_operator,"Grand Total") AS nm_c1, 
		IFNULL(c.nm_cabang,"Total") AS nm_c2, 
		SUM(IF(shift=1,a.total_penjualan,0)) AS bln_1,
		SUM(IF(shift=2,a.total_penjualan,0)) AS bln_2,
		SUM(IF(shift=3,a.total_penjualan,0)) AS bln_3,
		sum(a.total_penjualan) As total_penjualan
		')
			->join('tabel_unit AS c', 'a.no_spbu = c.no_spbu')
			->join('tabel_operator AS d', 'a.id_operator = d.id_operator')
			->where('a.tgl_penjualan BETWEEN "' . date('Y-m-d', strtotime($tgl_awal)) . '" and "' . date('Y-m-d', strtotime($tgl_akhir)) . '"')
			->where('a.selesai', '1')
			->group_by('d.nm_operator')
			->order_by('sum(a.total_penjualan) Desc')
			->get('tabel_penjualan AS a');

	}
	
	public function getOperator3($tanggal) {
		return $this->db->select('
		IFNULL(d.nm_operator,"Grand Total") AS nm_c1, 
		IFNULL(c.nm_cabang,"Total") AS nm_c2, 
		SUM(IF(shift=1,a.total_penjualan,0)) AS bln_Shift1,
		SUM(IF(shift=2,a.total_penjualan,0)) AS bln_Shift2,
		SUM(IF(shift=3,a.total_penjualan,0)) AS bln_Shift3,
		sum(a.total_penjualan) As total_penjualan
		')
			
			->join('tabel_unit AS c', 'a.no_spbu = c.no_spbu')
			->join('tabel_operator AS d', 'a.id_operator = d.id_operator')
			->where('a.tgl_penjualan', $tanggal)
			->where('a.selesai', '1')
			->group_by('d.nm_operator')
			->order_by('sum(a.total_penjualan) Desc')
			->get('tabel_penjualan AS a');
	}

	public function getDataPenjualanBarangFilter($tgl_awal, $tgl_akhir) {
		return $this->db->select('a.no_faktur_penjualan, b.kd_barang,b.nm_barang,SUM(b.retur) AS jum_retur, SUM(b.jumlah) AS jum_item')
			->join('tabel_rinci_penjualan AS b', 'a.no_faktur_penjualan = b.no_faktur_penjualan')
			->where('a.tgl_penjualan BETWEEN "' . date('Y-m-d', strtotime($tgl_awal)) . '" and "' . date('Y-m-d', strtotime($tgl_akhir)) . '"')
			->where('EXISTS (SELECT 1 FROM tabel_rinci_penjualan AS b WHERE a.no_faktur_penjualan=b.no_faktur_penjualan)')
			->where('a.selesai', '1')
			->group_by('b.kd_barang')
			->get('tabel_penjualan AS a');
	}
	
	

	public function getDataPenjualanBarang($tanggal) {
		return $this->db->select('a.no_faktur_penjualan, b.kd_barang,b.nm_barang,SUM(b.retur) AS jum_retur, SUM(b.jumlah) AS jum_item')
			->join('tabel_rinci_penjualan AS b', 'a.no_faktur_penjualan = b.no_faktur_penjualan')
			->where('a.tgl_penjualan', $tanggal)
			->where('EXISTS (SELECT 1 FROM tabel_rinci_penjualan AS b WHERE a.no_faktur_penjualan=b.no_faktur_penjualan)')
			->where('a.selesai', '1')
			->group_by('b.kd_barang')
			->get('tabel_penjualan AS a');
	}

	public function getDataProfit($tgl_awal, $tgl_akhir) {
		return $this->db->select('a.no_faktur_penjualan, 
		b.kd_barang,b.nm_barang,
		b.harga_modal,b.harga,
		b.retur AS jum_retur, b.jumlah AS jum_item,
		sum(a.total_penjualan) As total_penjualan
		')
			->join('tabel_rinci_penjualan AS b', 'a.no_faktur_penjualan = b.no_faktur_penjualan')
			->where('a.tgl_penjualan BETWEEN "' . date('Y-m-d', strtotime($tgl_awal)) . '" and "' . date('Y-m-d', strtotime($tgl_akhir)) . '"')
			->where('EXISTS (SELECT 1 FROM tabel_rinci_penjualan AS b WHERE a.no_faktur_penjualan=b.no_faktur_penjualan)')
			->where('a.selesai', '1')
			->order_by('b.kd_barang')
			->get('tabel_penjualan AS a');
	}

	public function getDiskonBarang($tgl_awal, $tgl_akhir) {
		return $this->db->select('SUM(b.diskonrp) AS disk1')
			->join('tabel_rinci_penjualan AS b', 'a.no_faktur_penjualan = b.no_faktur_penjualan')
			->where('a.tgl_penjualan BETWEEN "' . date('Y-m-d', strtotime($tgl_awal)) . '" and "' . date('Y-m-d', strtotime($tgl_akhir)) . '"')
			->where('EXISTS (SELECT 1 FROM tabel_rinci_penjualan AS b WHERE a.no_faktur_penjualan=b.no_faktur_penjualan)')
			->where('a.selesai', '1')
			->get('tabel_penjualan AS a');
	}

	public function getDiskonAkhir($tgl_awal, $tgl_akhir) {
		return $this->db->select('SUM(diskon) AS diska')
			->where('a.tgl_penjualan BETWEEN "' . date('Y-m-d', strtotime($tgl_awal)) . '" and "' . date('Y-m-d', strtotime($tgl_akhir)) . '"')
			->where('EXISTS (SELECT 1 FROM tabel_rinci_penjualan AS b WHERE a.no_faktur_penjualan=b.no_faktur_penjualan)')
			->where('a.selesai', '1')
			->get('tabel_penjualan AS a');
	}

	public function getDataProfit1($tanggal) {
		return $this->db->select('a.no_faktur_penjualan, b.kd_barang,b.nm_barang,b.harga_modal,b.harga,b.retur AS jum_retur, b.jumlah AS jum_item')
			->join('tabel_rinci_penjualan AS b', 'a.no_faktur_penjualan = b.no_faktur_penjualan')
			->where('a.tgl_penjualan', $tanggal)
			->where('EXISTS (SELECT 1 FROM tabel_rinci_penjualan AS b WHERE a.no_faktur_penjualan=b.no_faktur_penjualan)')
			->where('a.selesai', '1')
			->order_by('b.kd_barang')
			->get('tabel_penjualan AS a');
	}

	public function getDiskonBarang1($tanggal) {
		return $this->db->select('SUM(b.diskonrp) AS disk1')
			->join('tabel_rinci_penjualan AS b', 'a.no_faktur_penjualan = b.no_faktur_penjualan')
			->where('a.tgl_penjualan', $tanggal)
			->where('EXISTS (SELECT 1 FROM tabel_rinci_penjualan AS b WHERE a.no_faktur_penjualan=b.no_faktur_penjualan)')
			->where('a.selesai', '1')
			->get('tabel_penjualan AS a');
	}

	public function getDiskonAkhir1($tanggal) {
		return $this->db->select('SUM(diskon) AS diska')
			->where('a.tgl_penjualan', $tanggal)
			->where('EXISTS (SELECT 1 FROM tabel_rinci_penjualan AS b WHERE a.no_faktur_penjualan=b.no_faktur_penjualan)')
			->where('a.selesai', '1')
			->get('tabel_penjualan AS a');
	}

	public function getDataPengeluaranRinci1($tanggal) {
		return $this->db->select('*')
			->where('tgl', $tanggal)
			->get('tabel_biaya');
	}

	public function getDataRekap($tahun, $bulan) {
		return $this->db->select('a.tgl_penjualan, SUM(b.harga*b.jumlah) AS tot_jual, SUM(b.jumlah*b.harga_modal) AS tot_modal, SUM(b.diskonrp) AS tot_diskon1')
			->join('tabel_rinci_penjualan AS b', 'a.no_faktur_penjualan = b.no_faktur_penjualan')
			->where('MONTH(a.tgl_penjualan)', $bulan)
			->where('YEAR(a.tgl_penjualan)', $tahun)
			->where('a.selesai', '1')
			->group_by('a.tgl_penjualan')
			->get('tabel_penjualan AS a');
	}

	public function getDiskon($tahun, $bulan) {
		return $this->db->select('SUM(diskon) AS tot_diskon2')
			->where('MONTH(a.tgl_penjualan)', $bulan)
			->where('YEAR(a.tgl_penjualan)', $tahun)
			->where('a.selesai', '1')
			->group_by('a.tgl_penjualan')
			->get('tabel_penjualan AS a');
	}

	public function getDataPengeluaranRekapitulasi($tahun, $bulan) {
		return $this->db->select('*')
			->where('MONTH(tgl)', $bulan)
			->where('YEAR(tgl)', $tahun)
			->group_by('tgl')
			->get('tabel_biaya');
	}

}

/* End of file Laporan_model.php */
/* Location: ./application/models/Laporan_model.php */