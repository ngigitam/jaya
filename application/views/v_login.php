<!DOCTYPE html>
<html lang="en" >
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Jaya Nitrogen</title>
      <!--link rel="icon" type="image/png" href="<?php echo base_url('assets/') ?>/img/favicon.png"/-->
      <link rel="stylesheet" href="<?php echo base_url('assets/') ?>/login/style.css">
</head>
<body>

<form action="<?php echo base_url('login/auth') ?>" method="post">
<h1>JAYA NITROGEN</h1>
<!--img height="100px" src="<?php echo base_url() ?>/assets/img/lg.png" /-->
<br><br>
  <div class="form-group">
    <input required="required" class="form-control" name="username" id="username" />
    <label class="form-label">Username    </label>
  </div>
  <div class="form-group">
    <input id="password" name="password" type="password" required="required" class="form-control"/>
    <label class="form-label">Password</label>
    <!--p class="alert">Login Gagal..!!</p-->
    <button class="btn" type="submit">Login </button>
  </div>
</form>
<!--div align="center" style="font-family: Comic Sans MS, cursive, sans-serif;">&copy; Copyright. Arlan</div-->
  <script src="<?php echo base_url() ?>/assets/js/jquery-3.3.1.js"></script></script>
  <script  src="<?php echo base_url('assets/') ?>login/script.js"></script>
  <script  src="<?php echo base_url('assets/') ?>js/sweetalert.min.js"></script>
  <script>
    $(document).ready(function(){document.getElementById("username").focus()});
    $('form').attr('autocomplete', 'off');
    var error="<?php echo $this->session->flashdata('msg'); ?>";error&&swal(error,{buttons:!1,timer:2e3});
  </script>
</body>
</html>
