<div class="content-wrapper">
  <div class="container">
    <div class="row pad-botm">
        <div class="col-md-12">
            <h4 class="header-line">Input Biaya</h4>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
          <form class="form-horizontal" action="<?php echo base_url('kasir/simpan_biaya') ?>" method="post">
            <div class="form-group">
              <label class="control-label col-sm-2" for="nm_toko">Jenis Biaya</label>
              <div class="col-sm-5">
                <select name="jenis_biaya" id="jenis_biaya" class="form-control" required>
                  <option value="">Pilih Jenis Biaya</option>
                  <?php if ($level == 'manager'): ?>
                    <option value="Listrik">Listrik</option>
                    <option value="Gaji">Gaji</option>
					<option value="Biaya Umum dan Administrasi">Biaya Umum dan Administrasi</option>
                  <?php endif?>
                    <option value="Listrik">Listrik</option>
                    <option value="Gaji">Gaji</option>
					<option value="Biaya Umum dan Administrasi">Biaya Umum dan Administrasi</option>
                </select>
              </div>
            </div>
			<div class="form-group">
				<label class="control-label col-sm-2" >Nama Cabang</label>
				<div class="col-sm-5">
				<select name="no_spbu" id="no_spbu" style="width: 17em" class="form-control" data-live-search="true" title="Pilih Unit" data-width="100%" required>
                        <option value="">Pilih Nama Cabang</option>
						<?php foreach ($unit->result_array() as $i) {
                            $nm_unit=$i['nm_cabang'];
                            $no_spbu=$i['no_spbu'];
                            $sess_id=$this->session->userdata('unit');
                            if($sess_id==$no_spbu)
                                echo "<option value='$no_spbu' selected>$no_spbu - $nm_unit</option>";
                            else
                                echo "<option value='$no_spbu'>$no_spbu - $nm_unit</option>";
                        }?>
                </select>
				</div>
				</div>
            <div class="form-group">
              <label class="control-label col-sm-2" for="biaya">Biaya (Rp)</label>
              <div class="col-sm-5">
                <input type="text" class="form-control" id="biaya" name="biaya" required>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-2" for="ket">Keterangan</label>
              <div class="col-sm-5">
                <input type="text" class="form-control" id="ket" name="ket" required>
              </div>
            </div>
			<div class="form-group">
              <label class="control-label col-sm-2" for="tgl">Tanggal Transaksi</label>
              <div class="col-sm-5">
                <input type="date" class="form-control" id="tgl" name="tgl" required>
              </div>
            </div>

			<div class="form-group">
				<label class="control-label col-sm-2" >Sumber Dana</label>
				<div class="col-sm-5">
				<select name="sumber" id="sumber" style="width: 20em" class="form-control" data-live-search="true" title="Pilih Sumber Biaya" data-width="100%" required>
                        <option value="">Pilih Sumber Dana</option>
						<?php foreach ($sumber->result_array() as $i) 
						{
                            $id=$i['id'];
                            $nama=$i['nm_sumber_biaya'];
                            $saldo=$i['saldo'];
                            $sess_id=$this->session->userdata('sumber');
                            if($sess_id==$id)
                                echo "<option value='$id' selected>$nama</option>";
                            else
                                echo "<option value='$id'>$nama</option>";
                        }?>
                </select>
				</div>
				</div>
					 <input type="hidden" name="sebelum" id="sebelum" class="sebelum">

            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default">Simpan</button>
              </div>
            </div>
          </form>
        </div>
    </div>
  </div>
</div>
     <!-- CONTENT-WRAPPER SECTION END-->
    <section class="footer-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                   &copy; Copyright <?php echo date('Y') ?>, 
                </div>
            </div>
        </div>
    </section>
    <!-- FOOTER SECTION END-->
    <!-- JAVASCRIPT FILES PLACED AT THE BOTTOM TO REDUCE THE LOADING TIME  -->
    <script src="<?php echo base_url() ?>/assets/js/jquery-1.10.2.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/dataTables/jquery.dataTables.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/bootstrap.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/custom.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/sweetalert.min.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/toastr.min.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/jquery.price_format.min.js"></script>
	<!--script src="<?php echo base_url()?>/assets/js/jquery-2.2.3.min.js"></script-->
    <script>
      $('form').attr('autocomplete', 'off');
     	$("ul.nav li.dropdown").hover(function(){
		$(this).find(".dropdown-menu").stop(!0,!0).delay(100).fadeIn(500)},
		function(){$(this).find(".dropdown-menu").stop(!0,!0).delay(100).fadeOut(500),
		$(this).find(".dropdown-submenu").stop(!0,!0).delay(100).fadeIn(500)},
		function(){$(this).find(".dropdown-submenu").stop(!0,!0).delay(100).fadeOut(500)
		});
      var pesan="<?php echo $this->session->flashdata('msg'); ?>";pesan&&(toastr.options={positionClass:"toast-top-right"},toastr.success(pesan));
	$("ul.nav li.dropdown-submenu").hover(function(){
		$(this).find(".dropdown-menu1").stop(!0,!0).delay(100).fadeIn(500)},
		function(){$(this).find(".dropdown-menu1").stop(!0,!0).delay(100).fadeOut(500)
	});

	$(document).ready(function(){
       $('#sumber').change(function(){
            var sumber=$(this).val();
            $.ajax({
                url : "<?php echo base_url('kasir/get_sumber') ;?>",
                method : "POST",
                data : {sumber: sumber},
                async : false,
				cache:false,
                dataType : 'json',
                success: function(data){
					var html = '';
                    var i;
                    for(i=0; i<data.length; i++){
						html += data[i].saldo;
                    }
                    $('.sebelum').val(html); 
                }
            });
			return false;
        });
    });

      $(function(){
          $('#biaya').priceFormat({
              prefix: '',
              centsLimit: 0,
              thousandsSeparator: '.'
          });
      });
    </script>

</body>
</html>