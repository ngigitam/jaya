<!-- JAVASCRIPT FILES PLACED AT THE BOTTOM TO REDUCE THE LOADING TIME >
    <script src="<?php echo base_url() ?>/assets/js/jquery.min.js"></script-->
   <script src="<?php echo base_url() ?>/assets/js/jquery-3.3.1.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/bootstrap.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/custom.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/sweetalert.min.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/toastr.min.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/jquery.price_format.min.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/bootstrap-select.min.js"></script>
  

<script>
  $(document).ready(function(){
    // Sembunyikan alert validasi kosong
    $("#kosong").hide();
  });
  </script>


 <div class="content-wrapper" style="margin-bottom: 20px">
    <div class="container">
      <div class="row pad-botm">
          <div class="col-md-12">
              <h4 class="header-line">Form Import <span class="pull-right"> <a href="<?php echo base_url("excel/import_data.xlsx"); ?>">Download Format</a></span></h4>
          </div>
	  </div>
      <div class="row">
        <div class="col-md-12">
  <!-- Buat sebuah tag form dan arahkan action nya ke controller ini lagi -->
  <form method="post" action="<?php echo base_url('kasir/form'); ?>" enctype="multipart/form-data">
    <!-- 
    -- Buat sebuah input type file
    -- class pull-left berfungsi agar file input berada di sebelah kiri
    -->
	<div class="form-group">
	 <div class="col-sm-5">
    <input type="file" name="file" class="form-control" required>
    </div>
	</div>
    <!--
    -- BUat sebuah tombol submit untuk melakukan preview terlebih dahulu data yang akan di import
    >
    <input type="submit" name="preview" value="Preview"-->
	<button type="submit" name="preview" class="btn btn-default">Preview</button>
	<br>
  </form>
  
 
  
  <?php
  if(isset($_POST['preview'])){ // Jika user menekan tombol Preview pada form 
    if(isset($upload_error)){ // Jika proses upload gagal
      echo "<div style='color: red;'>".$upload_error."</div>"; // Muncul pesan error upload
      die; // stop skrip
    }
    
    // Buat sebuah tag form untuk proses import data ke database
    echo "<form method='post' action='".base_url('kasir/import')."'>";
    
    // Buat sebuah div untuk alert validasi kosong
    echo "<div style='color: red;' id='kosong'>
    Semua data belum diisi, Ada <span id='jumlah_kosong'></span> data yang belum diisi.
    </div>";
    
    echo "
	 <div class='form-group'>
	 <div class='col-sm-12'>
	<table border='1' cellpadding='12' class='table table-bordered table-striped table-responsive'>
    <tr>
      <th colspan='7'><center>Preview Data</center></th>
    </tr>
    <tr>
      <th>Tanggal</th>
	  <th>Bank</th>
	  <th>No.Rekening</th>
      <th>Nomor SPBU</th>
	  <th>Shift</th>
      <th>Nominal</th>
	  <th>Keterangan</th>
    </tr>";
    
    $numrow = 1;
    $kosong = 0;
    
    // Lakukan perulangan dari data yang ada di excel
    // $sheet adalah variabel yang dikirim dari controller
    foreach($sheet as $row){ 
      // Ambil data pada excel sesuai Kolom
      $tgl_setor = $row['A']; 
	  $nm_bank = $row['B'];
	  $no_rek = $row['C'];
      $no_spbu = $row['D']; 
      $shift = $row['E']; 
	  $nominal = $row['F'];
	  $ket = $row['G'];	  
      
      // Cek jika semua data tidak diisi
      if($tgl_setor == "" && $nm_bank == "" && $no_rek == "" && $no_spbu == "" && $shift == "" && $nominal == "")
        continue; 
		         // $kosong++; 
	  
	// Lewat data pada baris ini (masuk ke looping selanjutnya / baris selanjutnya)

      // Cek $numrow apakah lebih dari 1
      // Artinya karena baris pertama adalah nama-nama kolom
      // Jadi dilewat saja, tidak usah diimport
      if($numrow > 1){
        // Validasi apakah semua data telah diisi
        $tgl_setor_td = ( ! empty($tgl_setor))? "" : " style='background: #E07171;'";
		$nm_bank_td = ( ! empty($nm_bank))? "" : " style='background: #E07171;'"; 
		$no_rek_td = ( ! empty($no_rek))? "" : " style='background: #E07171;'"; 
        $spbu_td = ( ! empty($no_spbu))? "" : " style='background: #E07171;'"; 
		$shift_td = ( ! empty($shift))? "" : " style='background: #E07171;'"; 
        $nominal_td = ( ! empty($nominal))? "" : " style='background: #E07171;'";
		//$ket_td = ( ! empty($ket))? "" : " style='background: #E07171;'"; 
        
        // Jika salah satu data ada yang kosong
        if($tgl_setor == "" or $nm_bank == "" or $no_rek== "" or $no_spbu == "" or $shift == ""  or $nominal == ""){
          $kosong++; // Tambah 1 variabel $kosong
        }
        
        echo "<tr>";
        echo "<td".$tgl_setor_td.">".$tgl_setor."</td>";
		echo "<td".$nm_bank_td.">".$nm_bank."</td>";
		echo "<td".$no_rek_td.">".$no_rek."</td>";
        echo "<td".$spbu_td.">".$no_spbu."</td>";
		echo "<td".$shift_td.">".$shift."</td>";
        echo "<td".$nominal_td.">".$nominal."</td>";
		echo "<td>".$ket."</td>";
        echo "</tr>";
      }
      
      $numrow++; // Tambah 1 setiap kali looping
    }
    
    echo "</table>";
    
    // Cek apakah variabel kosong lebih dari 0
    // Jika lebih dari 0, berarti ada data yang masih kosong
    if($kosong > 0){
    ?>  
      <script>
      $(document).ready(function(){
        // Ubah isi dari tag span dengan id jumlah_kosong dengan isi dari variabel kosong
        $("#jumlah_kosong").html('<?php echo $kosong; ?>');
        
        $("#kosong").show(); // Munculkan alert validasi kosong
      });
      </script>
    <?php
    }else{ // Jika semua data sudah diisi
      echo "<hr>";
      
      // Buat sebuah tombol untuk mengimport data ke database
      echo "<button type='submit' name='import'class='btn btn-default btn-success'>Import</button>";
      echo "<a href='".base_url('kasir/setoran')."' class='btn btn-defaul btn-warning'>Cancel</a>";
    }
    
    echo "</form>";
  }
  ?>

      </div>     
      </div>
    </div>
  </div>
     <!-- CONTENT-WRAPPER SECTION END-->
    <section class="footer-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                   &copy; Copyright <?php echo date('Y') ?>
                </div>
            </div>
        </div>
    </section>
</div>
    <!-- FOOTER SECTION END-->
    
    <script>
      $('#tbSetor').DataTable({
          "paging":   false,
          "ordering": false,
      });
      $('form').attr('autocomplete', 'off');
     	$("ul.nav li.dropdown").hover(function(){
		$(this).find(".dropdown-menu").stop(!0,!0).delay(100).fadeIn(500)},
		function(){$(this).find(".dropdown-menu").stop(!0,!0).delay(100).fadeOut(500),
		$(this).find(".dropdown-submenu").stop(!0,!0).delay(100).fadeIn(500)},
		function(){$(this).find(".dropdown-submenu").stop(!0,!0).delay(100).fadeOut(500)
		});
      var pesan="<?php echo $this->session->flashdata('msg'); ?>";pesan&&(toastr.options={positionClass:"toast-top-right"},toastr.success(pesan));
	$("ul.nav li.dropdown-submenu").hover(function(){
		$(this).find(".dropdown-menu1").stop(!0,!0).delay(100).fadeIn(500)},
		function(){$(this).find(".dropdown-menu1").stop(!0,!0).delay(100).fadeOut(500)
	});
	  
	
    </script>

</body>
</html>