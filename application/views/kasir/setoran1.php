  <div class="content-wrapper" style="margin-bottom: 20px">
    <div class="container">
      <div class="row pad-botm">
          <div class="col-md-12">
              <h4 class="header-line">DATA SETORAN PENJUALAN <span class="pull-right"><a href="" data-toggle="modal" data-target="#myModal">Tambah Data</a></span></h4>
          </div>
		  <div class="col-md-12">
		  <h4><b><u><a href="<?php echo base_url('kasir/form'); ?>">Import Data</a>
			<u></b></h4>
			
	  </div>
	  </div>
	    
      <div class="row">
        <div class="col-md-12">
            <table id="tbSetor" class="table table-bordered table-striped table-responsive">
              <thead>
                <tr>
                  <th>Tanggal Setor</th>
				  <th>Nama Bank </th>
				  <th>Nomor Rekening </th>
                  <th>Nama Cabang</th>
                  <th>Nomor SPBU</th>
				  <th>Shift </th>
				  <th>Nominal</th>
				  <th>Keterangan</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($setoran->result() as $key): ?>
                <tr>
				<?php $key->id_setor ?>
                  <td><?php echo $key->tgl_setor ?></td>
				  <td><?php echo $key->nm_bank ?></td>
				  <td><?php echo $key->no_rek ?></td>
                  <td><?php echo $key->nm_cabang?></td>
				  <td><?php echo $key->no_spbu ?></td>
				  <td><?php echo $key->shift ?></td>
				  <td><?php echo $key->nominal ?></td>
				  <td><?php echo $key->ket ?></td>
                  <td align="center"><a href="javascript:void(0);" class="edit_record" data-id_setor="<?php echo $key->id_setor ?>" data-tgl_setor="<?php echo $key->tgl_setor ?>" data-no_spbu="<?php echo $key->no_spbu ?>" data-shift="<?php echo $key->shift ?>" data-nm_bank="<?php echo $key->nm_bank ?>" data-no_rek="<?php echo $key->no_rek ?>" data-nominal="<?php echo $key->nominal ?>" data-ket="<?php echo $key->ket ?>">Edit</a> | <a href="javascript:void(0);" class="hapus_record" data-id_setor="<?php echo $key->id_setor ?>">Hapus</a></td>
                </tr>
                <?php endforeach?>
              </tbody>
            </table>
        </div>
      </div>
    </div>
  </div>
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Tambah Setoran Penjualan</h4>
        </div>
        <div class="modal-body">
          <form class="form-horizontal" action="<?php echo base_url('kasir/simpan_setor') ?>" method="post">
            <div class="form-group">
              <label class="control-label col-sm-4" for="tgl_setor">Tanggal</label>
              <div class="col-sm-5">
                <input type="date" class="form-control" id="tgl_setor" name="tgl_setor" required>
              </div>
            </div>
			<div class="form-group">
              <label class="control-label col-sm-4" for="bank">Nama Bank</label>
              <div class="col-sm-5">
                <input type="text" class="form-control" id="bank" name="bank" required>
              </div>
            </div>
			<div class="form-group">
              <label class="control-label col-sm-4" for="no_rek">Nomor Rekening</label>
              <div class="col-sm-5">
                <input type="text" class="form-control" id="no_rek" name="no_rek" required>
              </div>
            </div>
			<div class="form-group">
				<label class="control-label col-sm-4" >Nomor SPBU</label>
				<div class="col-sm-5">
				<select name="no_spbu" id="no_spbu" style="width: 11em" class="form-control" data-live-search="true" title="Pilih Unit" data-width="100%" required>
                        <option value="">Pilih No.SPBU</option>
						<?php foreach ($unit->result_array() as $i) {
                            $nm_unit=$i['nm_cabang'];
                            $no_spbu=$i['no_spbu'];
                            $sess_id=$this->session->userdata('unit');
                            if($sess_id==$no_spbu)
                                echo "<option value='$no_spbu' selected>$no_spbu - $nm_unit</option>";
                            else
                                echo "<option value='$no_spbu'>$no_spbu - $nm_unit</option>";
                        }?>
                </select>
				</div>
				</div>
		
			<div class="form-group">
              <label class="control-label col-sm-4" for="shift">Shift</label>
              <div class="col-sm-5">
                <select name="shift" id="shift" class="form-control" required>
                  <option value="">-Pilih Shift-</option>
                    <option value="1">Shift 1</option>
                    <option value="2">Shift 2</option>
					<option value="3">Shift 3</option>
                </select>
              </div>
            </div>
			
	
			
            <div class="form-group">
              <label class="control-label col-sm-4" for="nominal">Jumlah Setor</label>
              <div class="col-sm-5">
                <input type="text" class="form-control" id="nominal" name="nominal" required>
              </div>
            </div>
			<div class="form-group">
              <label class="control-label col-sm-4" for="ket">Keterangan</label>
              <div class="col-sm-5">
                <input type="text" class="form-control" id="ket" name="ket" required>
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" id="btnSimpan" class="btn btn-primary">Simpan</button>
        </div>
        </form>
      </div>
    </div>
  </div>
  <div class="modal fade" id="modalEdit" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Edit Setoran Penjualan</h4>
        </div>
        <div class="modal-body">
          <form class="form-horizontal" action="<?php echo base_url('kasir/simpan_setor_edit') ?>" method="post">
            <input type="hidden" id="id_setor_e" name="id_setor_e">
			<div class="form-group">
              <label class="control-label col-sm-4" for="tgl_setor_e">Tanggal Setor</label>
              <div class="col-sm-5">
                <input type="date" class="form-control" id="tgl_setor_e" name="tgl_setor_e" required>
              </div>
            </div>			
			<div class="form-group">
              <label class="control-label col-sm-4" for="nm_bank_e">Nama Bank</label>
              <div class="col-sm-5">
                <input type="text" class="form-control" id="nm_bank_e" name="nm_bank_e" required>
              </div>
            </div>
							
			<div class="form-group">
              <label class="control-label col-sm-4" for="no_rek_e">Nomor Rekening</label>
              <div class="col-sm-5">
                <input type="text" class="form-control" id="no_rek_e" name="no_rek_e" required>
              </div>
            </div>
			<div class="form-group">
				<label class="control-label col-sm-4" >Nomor SPBU</label>
				<div class="col-sm-5">
				<select name="no_spbu_e" id="no_spbu_e" style="width: 11em" class="form-control" data-live-search="true" title="Pilih Unit" data-width="100%" required>
                        <option value="">Pilih No.SPBU</option>
						<?php foreach ($unit->result_array() as $i) {
                            $nm_unit=$i['nm_cabang'];
                            $no_spbu=$i['no_spbu'];
                           $sess_id=$this->session->userdata('unit');
                           if($sess_id==$no_spbu)
                                echo "<option value='$no_spbu' selected>$no_spbu - $nm_unit</option>";
                            else
                                echo "<option value='$no_spbu'>$no_spbu - $nm_unit</option>";
                        }?>
                </select>
				</div>
				</div>
			<div class="form-group">
              <label class="control-label col-sm-4" for="shift_e">Shift</label>
              <div class="col-sm-5">
                <select name="shift_e" id="shift_e" class="form-control" required>
                  <option value="">-Pilih Shift-</option>
                    <option value="1">Shift 1</option>
                    <option value="2">Shift 2</option>
					<option value="3">Shift 3</option>
                </select>
              </div>
            </div>				
			<div class="form-group">
              <label class="control-label col-sm-4" for="nominal_e">Jumlah Setor</label>
              <div class="col-sm-5">
                <input type="text" class="form-control" id="nominal_e" name="nominal_e" required>
              </div>
            </div>
			<div class="form-group">
              <label class="control-label col-sm-4" for="ket">Keterangan</label>
              <div class="col-sm-5">
                <input type="text" class="form-control" id="ket_e" name="ket_e" required>
              </div>
            </div>
            </div>
			
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" id="btnSimpan" class="btn btn-primary">Simpan</button>
        </div>
        </form>
      </div>
    </div>
  </div>
    <!-- Modal Hapus -->
  <div class="modal fade" id="modalHapus" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Hapus Setoran Penjualan</h4>
        </div>
        <div class="modal-body">
          <form class="form-horizontal" action="<?php echo base_url('kasir/hapus_setoran') ?>" method="post">
           <h4>Apakah Kamu Yakin Menghapus Data Setoran Penjualan Ini?</h4>
        </div>
        <input type="hidden" id="id_setor_h" name="id_setor_h">
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
          <button type="submit" id="btnHapus" class="btn btn-primary">Ya</button>
        </div>
        </form>
      </div>
    </div>
  </div>
     <!-- CONTENT-WRAPPER SECTION END-->
    <section class="footer-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                   &copy; Copyright <?php echo date('Y') ?>
                </div>
            </div>
        </div>
    </section>
</div>
    <!-- FOOTER SECTION END-->
    <!-- JAVASCRIPT FILES PLACED AT THE BOTTOM TO REDUCE THE LOADING TIME  -->
    <script src="<?php echo base_url() ?>/assets/js/jquery-3.3.1.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/jquery.dataTables.min.js"></script>
	 <script src="<?php echo base_url() ?>/assets/js/dataTables.checkboxes.min.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/bootstrap.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/custom.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/sweetalert.min.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/toastr.min.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/jquery.price_format.min.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/bootstrap-select.min.js"></script>
    <script>

    $('form').attr('autocomplete', 'off');
	$("ul.nav li.dropdown").hover(function(){
		$(this).find(".dropdown-menu").stop(!0,!0).delay(100).fadeIn(500)},
		function(){$(this).find(".dropdown-menu").stop(!0,!0).delay(100).fadeOut(500),
		$(this).find(".dropdown-submenu").stop(!0,!0).delay(100).fadeIn(500)},
		function(){$(this).find(".dropdown-submenu").stop(!0,!0).delay(100).fadeOut(500)
		});
      var pesan="<?php echo $this->session->flashdata('msg'); ?>";pesan&&(toastr.options={positionClass:"toast-top-right"},toastr.success(pesan));
	$("ul.nav li.dropdown-submenu").hover(function(){
		$(this).find(".dropdown-menu1").stop(!0,!0).delay(100).fadeIn(500)},
		function(){$(this).find(".dropdown-menu1").stop(!0,!0).delay(100).fadeOut(500)
	});	
	$(document).ready(function() {
	
	$('.dropdown-submenu a.test').on("click", function(e){
    $(this).next('ul').toggle();
    e.stopPropagation();
    e.preventDefault();
	});
	$('#tbSetor').DataTable({
				//"searching": false,
        columnDefs: [ {
            orderable: false,
            //className: 'select-checkbox',
            targets:   0
        } ],
        select: {
            style:    'os',
            selector: 'td:first-child'
        },
        order: [[ 1, 'asc' ]]
      });
	//$('.edit_record').on('click',function(){
		 $('#tbSetor').on('click','.edit_record',function(){
              var id_setor=$(this).data('id_setor');
			  var tgl_setor=$(this).data('tgl_setor');
              var cabang=$(this).data('nm_cabang');
              var no_spbu=$(this).data('no_spbu');
			  var shift=$(this).data('shift');
			  var nominal=$(this).data('nominal');
			  var ket=$(this).data('ket');
			  var nm_bank=$(this).data('nm_bank');
			  var no_rek=$(this).data('no_rek');
              $('#modalEdit').modal('show');
			  $('[name="id_setor_e"]').val(id_setor);
              $('[name="tgl_setor_e"]').val(tgl_setor);
			  $('[name="nm_bank_e"]').val(nm_bank);
			  $('[name="no_rek_e"]').val(no_rek);
              $('[name="no_spbu_e"]').val(no_spbu);
              $('[name="shift_e"]').val(shift);
			  $('[name="nominal_e"]').val(nominal);
			  $('[name="ket_e"]').val(ket);

          });

 
		          //GET CONFIRM DELETE
          //  $('.hapus_record').on('click',function(){
			$('#tbSetor').on('click','.hapus_record',function(){
                var id_setor = $(this).data('id_setor');
                $('#modalHapus').modal('show');
                $('[name="id_setor_h"]').val(id_setor);
            });
      });
		
	  $(function(){
          $('#nominal').priceFormat({
              prefix: '',
              centsLimit: 0,
              thousandsSeparator: '.'
          });
      });

    </script>

</body>
</html>