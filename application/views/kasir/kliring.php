  <div class="content-wrapper" style="margin-bottom: 20px">
    <div class="container">
      <div class="row">
	    <div class="col-md-12">
		
              <h4 class="header-line">KLIRING SETORAN</h4>
          </div>
		 
		    <div  class="col-md-8">
			    
	    <form class="form-inline" action="" method="get" >
		<div class="panel-body">
		<div class="form-group">
			
				<label for="no_spbu">Nomor SPBU</label>
				<select name="no_spbu" id="no_spbu" style="width: 17em" class="form-control"  required>
                <option value="">Pilih No.SPBU</option>
				<?php foreach ($nospbu->result() as $key): ?>
					<option <?php if ($spbu == $key->no_spbu1) {echo 'selected';}?> value="<?php echo $key->no_spbu1?>"><?php echo $key->no_spbu1 ?></option>
				<?php endforeach?>
                </select>
				
				</div>
				
				<button type="submit" class="btn btn-default">Tampilkan</button>
				
				</form>
				</div>
				</div>
		
			
				 <div style="margin-left: 20px;margin-right: 20px">
				   <div class="row">
				   <div  class="col-md-6">
				   <div class="panel panel-default">
				   <div class="panel-body">
				   
			
			<div class="table-responsive">
			<center><label><u>PENJUALAN</u></label></center>
            <table id="tbKliring" class="table table-bordered table-responsive display">
              <thead>
                <tr>
				  <th><!--input type="checkbox" id="check-all"--></th>
                  <th>No.Faktur Penjualan</th>
                  <th>Tanggal Penjualan</th>
                  <th>Nomor SPBU</th>
				  <th>Nominal</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($nspbu->result() as $key): ?>
                <tr>
				  <!--td><?php echo "<input type='checkbox' class='check-item' name='no_faktur1'  id='no_faktur1' value='".$key->no_faktur_penjualan."'>" ?></td-->
                  <td><?php echo "<input type='checkbox' class='check-item' name='no_faktur1'  id='no_faktur1' value='".$key->no_faktur_penjualan."'>" ?></td>
				  <td><?php echo $key->no_faktur_penjualan ?></td>
				  <td><?php echo $key->tgl_penjualan ?></td>
				  <td><?php echo $key->no_spbu1 ?></td>
                  <td align="right"><?php echo number_format($key->total_penjualan, 0, ',', '.') ?></td>
                  
                </tr>
		<?php
			$spbu = $key->no_spbu2;
			$no_faktur = $key->no_faktur_penjualan;
			//$no_faktur_setor = $key->no_faktur_setor;
			$tgl_setor = $key->tgl_setor;
			$no_spbu2 = $key->no_spbu2;
			$nominal = $key->nominal;
			$tot_jual += $key->total_penjualan;
			$tot_setor += $key->nominal;
			if($tot_jual>$tot_setor){
				$kurang=$tot_jual-$tot_setor;
			}elseif($tot_jual<$tot_setor){
				$kurang=$tot_setor-$tot_jual;
			}
		?>
       <?php endforeach?>

				</div>
			</div>
			</div>
		</div>

<form>
<table>
</table>
</form>
</div>
</div>
 </div>
  </div>


  

		<div  class="col-md-6">
		  <div class="panel panel-default">
		  <div class="panel-body">
		<div class="form-horizontal">
		<div class="table-responsive">
		<center><label><u>SETORAN</u></label></center>
            <table id="tbKliring2" class="table table-bordered table-responsive display">
              <thead>
                <tr>
				  <th></th>
                  <th>ID Setoran</th>
                  <th>Tanggal Setoran</th>
                  <th>Nomor SPBU</th>
				  <th>Nominal</th>
                </tr>
              </thead>
              <tbody>
			  <?php foreach ($nspbu1->result() as $key): ?>
                <tr>
				  <td><?php echo "<input type='checkbox' class='check-item2' name='id_setor' id='id_setor' value='".$key->id_setor."'>" ?></td>
				  <td><?php echo $key->id_setor ?></td>
				  <td><?php echo $key->tgl_setor ?></td>
				  <td><?php echo $key->no_spbu2 ?></td>
				  <td align="right"><?php echo number_format($key->nominal, 0, ',', '.') ?></td>
                  
                </tr>
			  <?php endforeach?>
				</div>
				
				</div>
			</div>
			</div>
	    </div></div>
<form>
<table>
</table>
</form>
</div>
</div>
</div>
</div>
</div>
</div>
</div>



		
				 <div style="margin-left: 20px;margin-right: 20px">
				   <div class="row">
				   <div  class="col-md-6">
				   <div class="panel panel-default">
				   <div class="panel-body">
			<div class="table-responsive">
		<center><label><u>SISAH KLIRING</u></label></center>
            <table id="tbKliring3" class="table table-bordered table-responsive display">
              <thead>
                <tr>
				  <th></th>
                  <th>Tanggal Penjualan</th>
                  <th>Tanggal Setoran</th>
                  <th>Nomor SPBU</th>
				  <th>Selisih</th>
                </tr>
              </thead>
              <tbody>
			  <?php foreach ($nspbu2->result() as $key): ?>
                <tr>
				  <td><?php echo "<input type='checkbox' class='check-item3' name='id_kliring' id='id_kliring' value='".$key->id_kliring."'>" ?></td>
				  <td><?php echo $key->tgl_penjualan1 ?></td>
				  <td><?php echo $key->tgl_setor1 ?></td>
				  <td><?php echo $key->no_spbu3 ?></td>
				  <td align="right"><?php echo number_format($key->selisih, 0, ',', '.') ?></td>
                  
                </tr>
			  <?php endforeach?>
				</div>
				
				</div>
			</div>
			</div>
	    </div></div>
		
		
<form>
<table>
</table>
</form>
</div>
</div>
 </div>
 </div>

 
 <div class="form-group pull-left">
 
 		<div  class="col-md-12">
		  <div class="panel panel-default">
		  <div class="panel-body">
		  <div class="form-horizontal">
		<div class="table-responsive">

 <form action="<?php echo base_url().'kasir/simpan_kliring'?>" method="post">
           
				 <input type="hidden" id="no_faktur_penjualan" name="no_faktur_penjualan" class="no_faktur_penjualan">
				<input type="hidden" id="spbu" name="spbu" class="spbu">
				<input type="hidden" id="id_setor" name="id_setor" class="id_setor">
				<input type="hidden" id="tgl_penjualan" name="tgl_penjualan" class="tgl_penjualan">
				<input type="hidden" id="tgl_setor" name="tgl_setor" class="tgl_setor">
				<input type="hidden" id="id_k_s" name="id_k_s" class="id_k_s">
	
			<div class="table-responsive">
			
		   <table>

                <tr>
                    <th>Total Penjualan </th>
					<th> </th>
                    <th><input type="text" name="total_penjualan" class="total_penjualan form-control input-sm" style="text-align:right;margin-bottom:5px;" readonly  ></th>
                    <input type="hidden" id="total_penjualan" name="total_penjualan" class="total_penjualan form-control input-sm" >
                </tr>
				<tr>
                    <th>Total Setoran </th>
					<th> </th>
                   <th><input type="text" name="total_setor" class="total_setor form-control input-sm" style="text-align:right;margin-bottom:5px;" readonly ></th>
                    <input type="hidden" id="total_setor" name="total_setor" class="total_setor form-control input-sm"  >
              </tr>
			  <tr>
                    <th>Sisah Sebelumnya </th>
					<th> </th>
                   <th><input type="text" name="sisah" class="sisah form-control input-sm" style="text-align:right;margin-bottom:5px;" readonly ></th>
                    <input type="hidden" id="sisah" name="sisah" class="sisah form-control input-sm" >
                   </tr>
			 <tr>
               <tr>
                    <th>Selisih </th>
					<th> </th>
                   <th><input type="text" name="selisih" class="selisih form-control input-sm" style="text-align:right;margin-bottom:5px;" readonly ></th>
                    <input type="hidden" id="selisih" name="selisih" class="selisih form-control input-sm" >
                   </tr>
			 <tr>
				<td><button type='submit' class='simpan btn btn-info btn-lg'> Simpan</button></td>
				</tr>
				

            </table>
			  </div>
		
            </form>

	
				</div>
				
				</div>
			</div>
			</div>
	    </div></div>
		
			
	

     <!-- CONTENT-WRAPPER SECTION END-->
    <section class="footer-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                   &copy; Copyright <?php echo date('Y') ?>, 
                </div>
            </div>
        </div>
    </section>
    <!-- FOOTER SECTION END-->
    <!-- JAVASCRIPT FILES PLACED AT THE BOTTOM TO REDUCE THE LOADING TIME 

	-->
      <script src="<?php echo base_url() ?>/assets/js/jquery-3.3.1.js"></script>

	  <script src="<?php echo base_url() ?>/assets/js/jquery.dataTables.min.js"></script>
	<script src="<?php echo base_url() ?>/assets/js/dataTables.select.min.js"></script>
	 <script src="<?php echo base_url() ?>/assets/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/bootstrap.js"></script>
 <script src="<?php echo base_url() ?>/assets/js/dataTables.checkboxes.min.js"></script>
   
    <script src="<?php echo base_url() ?>/assets/js/sweetalert.min.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/toastr.min.js"></script>
	    <script src="<?php echo base_url() ?>/assets/js/jquery.price_format.min.js"></script>
		<script>
		$("ul.nav li.dropdown").hover(function(){
		$(this).find(".dropdown-menu").stop(!0,!0).delay(100).fadeIn(500)},
		function(){$(this).find(".dropdown-menu").stop(!0,!0).delay(100).fadeOut(500),
		$(this).find(".dropdown-submenu").stop(!0,!0).delay(100).fadeIn(500)},
		function(){$(this).find(".dropdown-submenu").stop(!0,!0).delay(100).fadeOut(500)
		});
      var pesan="<?php echo $this->session->flashdata('msg'); ?>";pesan&&(toastr.options={positionClass:"toast-top-right"},toastr.success(pesan));
	$("ul.nav li.dropdown-submenu").hover(function(){
		$(this).find(".dropdown-menu1").stop(!0,!0).delay(100).fadeIn(500)},
		function(){$(this).find(".dropdown-menu1").stop(!0,!0).delay(100).fadeOut(500)
		});
		
	$(document).ready(function() {
		$('.simpan').hide();
	$('#tbKliring').DataTable( {
		"searching": false,
        columnDefs: [ {
            orderable: false,
            //className: 'select-checkbox',
            targets:   0
        } ],
        select: {
            style:    'os',
            selector: 'td:first-child'
        },
        order: [[ 1, 'asc' ]]
    
	 });
	
       $('#tbKliring').on( 'change', '.check-item', function (e) {
			$('input[name=no_faktur1]').not(this).prop('checked', false);
			var no_faktur1   = $('input[name=no_faktur1]:checked').val();
			$('.simpan').show();
			  $.ajax({
                url : "<?php echo base_url('kasir/get_nofaktur1') ;?>",
                method : "POST",
                data : {no_faktur1: no_faktur1},
                async : true,
				//cache:false,
                dataType : 'json',
                success: function(data){
					var nofak= '';
					var total_penj= '';
					var nspu= '';
					var tgl_penj= '';
                    var i;
                    for(i=0; i<data.length; i++){
						nofak +=data[i].no_faktur_penjualan;
						total_penj +=data[i].total_penjualan;
						nspu +=data[i].no_spbu;
						tgl_penj +=data[i].tgl_penjualan;
                    }
					$('.no_faktur_penjualan').val(nofak);
                    $('.total_penjualan').val(total_penj);
					$('.spbu').val(nspu);
					$('.tgl_penjualan').val(tgl_penj);
					
			var topenj = $('.total_penjualan').val();
            var toset = $('.total_setor').val();
            var topenj1 = topenj.replace(/[^\d]/g,"");
			var toset1 = toset.replace(/[^\d]/g,"");
			  var selisih =(Number(topenj1)-Number(toset1));
			var sish = $('.sisah').val();
			var slish = $('.selisih').val();
        
			var sish1 = sish.replace(/[^\d]/g,"");
			var slish1 = sish.replace(/[^\d]/g,"");
			if(sish!=""){
			if(sish<0){
			var selisih =((Number(topenj1) - Number(toset1)) + (- Number(sish1)));
					if((Number(selisih)) !=0)
					{
						$('.simpan').hide();
					}else{
						$('.simpan').show();
					}
			}else{
					var selisih =((Number(topenj1) - Number(toset1)) + (Number(sish1)));
					if((Number(selisih)) !=0)
					{
						$('.simpan').hide();
					}else{
						$('.simpan').show();
					}
			}
			}
			
			$('.selisih').val(selisih);
            }
            });
			return false;	
        });	
		
		$('#tbKliring2').DataTable( {
		"searching": false,
        columnDefs: [ {
            orderable: false,
            //className: 'select-checkbox',
            targets:   0
        } ],
        select: {
            style:    'os',
            selector: 'td:first-child'
        },
        order: [[ 1, 'asc' ]]
		});
	 
		
		$('#tbKliring2').on( 'change', '.check-item2', function (e) {
			$('input[name=id_setor]').not(this).prop('checked', false);
			var id_setor = $('input[name=id_setor]:checked').val();
			$('.simpan').show();
			  $.ajax({
                url : "<?php echo base_url('kasir/get_setor1') ;?>",
                method : "POST",
                data : {id_setor: id_setor},
                async : true,
				//cache:false,
                dataType : 'json',
                success: function(data){
					var id_stor = '';
					var nomnal = '';
					var tgl_str = '';
                    var i;
                    for(i=0; i<data.length; i++){					
						id_stor +=data[i].id_setor;
						nomnal +=data[i].nominal;
						tgl_str +=data[i].tgl_setor;
                    }
					$('.id_setor').val(id_stor);
                    $('.total_setor').val(nomnal);
					$('.tgl_setor').val(tgl_str);

			var topenj = $('.total_penjualan').val();
            var toset = $('.total_setor').val();
            var topenj1 = topenj.replace(/[^\d]/g,"");
			var toset1 = toset.replace(/[^\d]/g,"");
			var selisih =(Number(topenj1)-Number(toset1));
			
			
			var sish = $('.sisah').val();
			var slish = $('.selisih').val();
        
			var sish1 = sish.replace(/[^\d]/g,"");
			var slish1 = sish.replace(/[^\d]/g,"");
			if(sish!=""){
			if(sish<0){
			var selisih =((Number(topenj1) - Number(toset1)) + (- Number(sish1)));
					if((Number(selisih)) !=0)
					{
						$('.simpan').hide();
					}else{
						$('.simpan').show();
					}
			}else{
					var selisih =((Number(topenj1) - Number(toset1)) + (Number(sish1)));
					if((Number(selisih)) !=0)
					{
						$('.simpan').hide();
					}else{
						$('.simpan').show();
					}
			}
			}
			
			$('.selisih').val(selisih);
			}	
            });
			return false;	
	});
	
	$('#tbKliring3').DataTable( {
		"searching": false,
        columnDefs: [ {
            orderable: false,
            //className: 'select-checkbox',
            targets:   0
        } ],
        select: {
            style:    'os',
            selector: 'td:first-child'
        },
        order: [[ 1, 'asc' ]]
    
	});

	$('#tbKliring3').on( 'change', '.check-item3', function (e) {
			$('input[name=id_kliring]').not(this).prop('checked', false);
			var id_kliring= $('input[name=id_kliring]:checked').val();
			  $.ajax({
                url : "<?php echo base_url('kasir/get_kliring1') ;?>",
                method : "POST",
                data : {id_kliring: id_kliring},
                async : true,
				//cache:false,
                dataType : 'json',
                success: function(data){
					var id_klring = '';
					var slsih = '';
					
                    var i;
                    for(i=0; i<data.length; i++){					
						id_klring +=data[i].id_kliring;
						slsih +=data[i].selisih;
                    }
					$('.id_k_s').val(id_klring);
                    $('.sisah').val(slsih);
				

			var topenj = $('.total_penjualan').val();
            var toset = $('.total_setor').val();
			var sish = $('.sisah').val();
			var slish = $('.selisih').val();
            var topenj1 = topenj.replace(/[^\d]/g,"");
			var toset1 = toset.replace(/[^\d]/g,"");
			var sish1 = sish.replace(/[^\d]/g,"");
			var slish1 = sish.replace(/[^\d]/g,"");
			if(sish<0){
			var selisih =((Number(topenj1) - Number(toset1)) + (- Number(sish1)));
					if((Number(selisih)) !=0)
					{
						$('.simpan').hide();
					}else{
						$('.simpan').show();
					}
			}else{
					var selisih =((Number(topenj1) - Number(toset1)) + (Number(sish1)));
					if((Number(selisih)) !=0)
					{
						$('.simpan').hide();
					}else{
						$('.simpan').show();
					}
			}
			$('.selisih').val(selisih);

			}	
            });
			return false;	
	});
	

			
});

	
	function convertToRupiah(angka){
          var rupiah = '';
          var angkarev = angka.toString().split('').reverse().join('');
          for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
          return rupiah.split('',rupiah.length-1).reverse().join('');
    };

		
  // Document ready cek start
</script>

</body>
</html>