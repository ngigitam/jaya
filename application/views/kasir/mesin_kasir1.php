<div class="col-md-12">
    <h4 class="header-line">Inputan Penjualan
	 <?php 
	 $hak = $this->session->userdata('akses');
	 if ($hak == 'manajer'): ?>
	<div class="pull-right"><a href="<?php echo base_url('penjualan') ?>" class="btn btn-sm btn-success"><span class="fa fa fa-file"></span> Data Penjualan</a></div>
     <?php endif?>
	  </h4>      
</div>
  
  <div class="content-wrapper" style="margin-bottom: 20px">
    <div style="margin-left: 20px;margin-right: 20px">
      <div class="row">
        <div class="col-md-4">
          <div class="panel panel-default">
           <div class="panel-heading">
			<b>
			
			Faktur : <?php echo $faktur->no_faktur_penjualan ?>
			 <span class="pull-right">
			 <?php echo date_indo($tgl) ?> &nbsp;<span id="waktu"></span>
			 </span>
            </b>
			</div>
          <div class="panel-body">
			<div class="form-horizontal">

			<input type="hidden" name="nofak" id="nofak" value="<?php echo $faktur->no_faktur_penjualan ?>">


			   <form  action="<?php echo base_url('kasir/go_to_bayar/') ?>" method="post">
                  <!--div class="form-group">
				  <label class="control-label col-sm-2" for="kd_unit">Kode Unit</label>
                  <div class="col-sm-4">
                  <input type="text" class="form-control" id="kd_unit" name="kd_unit" required>
				 
				
				 </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-sm-2" for="nm_unit">Nama Unit</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" id="nm_unit" name="nm_unit" placeholder="Cari Nama Unit">
                  </div>
                </div-->
				<div class="form-group">
				<label class="control-label col-sm-3" >Cabang</label>
				<div class="col-sm-8">
				<select name="unit" id="unit"  class="form-control" data-live-search="true" title="Pilih Unit" data-width="100%" required>
                        <option value="">-Pilih Cabang-</option>
						<?php 
						//$nfktr=$faktur->no_faktur_penjualan;
						foreach ($unit->result_array() as $i) 
						{
                            $id=$i['no_spbu'];
                            $nama=$i['nm_cabang'];
                            $nspbu=$faktur->no_spbu;
                            $sess_id=$this->session->userdata('unit');
                            if($nspbu==$id )
							{
                                echo "<option value='$id' selected>$id - $nama</option>";
                            }else{
                                echo "<option value='$id'>$id - $nama</option>";
							}
						}?>
					
                </select>
				</div>
			</div>	
				<div class="form-group">
                  <label class="control-label col-sm-3" for="tglj">Tanggal</label>
                  <div class="col-sm-8">
                    <input type="date" class="form-control" id="tglj" name="tglj" value="<?php echo $faktur->tgl_penjualan ?>" placeholder="">
                  </div>
                </div>
				
				
			<div class="form-group">
              <label class="control-label col-sm-3" for="shift">Shift</label>
              <div class="col-sm-8">
                <select name="shift" id="shift" class="form-control" required>
                  <option value="">-Pilih Shift-</option>
					<option value="1" <?php if(!empty($faktur->shift) && $faktur->shift == "1") echo 'selected'; ?>>Shift 1</option>
                    <option value="2" <?php if(!empty($faktur->shift) && $faktur->shift == "2") echo 'selected'; ?>>Shift 2</option>
					<option value="3" <?php if(!empty($faktur->shift) && $faktur->shift == "3") echo 'selected'; ?>>Shift 3</option>
                   
					<!--option value="1">Shift 1</option>
                    <option value="2">Shift 2</option>
					<option value="3">Shift 3</option-->
                </select>
              </div>
            </div>
			
					
			<div class="form-group">
              <label class="control-label col-sm-3" for="operator">Operator</label>
              <div class="col-sm-8">
                <select name="operator" id="operator" class="operator form-control" required>
                  <option value="">-Pilih operator-</option>
				  <?php 
					
						foreach ($optor->result_array() as $i) 
						{
                            $nospbu=$i['no_spbu'];
							$id_op=$i['id_operator'];
                            $nama_op=$i['nm_operator'];
                            $nspbu1=$faktur->no_spbu;
							$idoptr=$faktur->id_operator;
                            $sess_id=$this->session->userdata('optor');
                            if($idoptr==$id_op && $nospbu==$nspbu1)
							{
                                echo "<option value='$id_op' selected>$nama_op</option>";
                            }else{
                                echo "<option value='$id_op'>$nama_op</option>";
							}
						}?>
  
                </select>
              </div>
            </div>
			<div class="form-group">
                  <label class="control-label col-sm-3" for="kd_barang">Kode Jasa</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" id="kd_barang" name="kd_barang" required >
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-sm-3" for="nm_barang">Nama jasa</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" id="nm_barang" name="nm_barang" placeholder="Cari Nama Jasa">
                  </div>
                </div>
				
			
              <hr>
                
                <input type="hidden" name="nofak_bayar" id="nofak_bayar" value="<?php echo $faktur->no_faktur_penjualan ?>">
                <input type="hidden" name="total_belanja" id="total_belanja" value="<?php echo $belanja->tot_bel ?>">
                <input type="hidden" name="diskon_belanja" id="diskon_belanja" value="<?php echo $faktur->diskon ?>">
                <input type="hidden" name="diskon_ket" id="diskon_ket" value="<?php echo $faktur->ket_diskon ?>">
                <button type="submit" class="btn btn-danger pull-right">Simpan</button>
                </form>
              <!--table style="width: 100%;height: 120px;">
                <tr>
                  <td style="font-size: 18pt">TOTAL</td>
                  <?php if ($faktur->diskon > 0): ?>
                      <td align="right" style="font-size: 32pt"><strong><?php echo number_format($faktur->total_penjualan_sdiskon, 0, ',', '.') ?></strong></td>
                    <?php else: ?>
                      <td align="right" style="font-size: 32pt"><strong><?php echo number_format($belanja->tot_bel, 0, ',', '.') ?></strong></td>
                  <?php endif?>
                </tr>
                <!--tr>
                  <td style="font-size: 18pt;color: red;">DISKON</td>
                  <td align="right" style="font-size: 32pt;color: red;"><strong><?php echo number_format($faktur->diskon, 0, ',', '.') ?></strong></td>
                </tr>
              </table>
              <hr>
              <div>
               <div class="form-horizontal">
                <div class="form-group">
                  <label class="control-label col-sm-4" for="kd_unit">Kode Unit</label>
                  <div class="col-sm-8">
                    <input type="hidden" name="nofak" id="nofak" value="<?php echo $faktur->no_faktur_penjualan ?>">
                <form class="form-inline" action="<?php echo base_url('kasir/go_to_bayar/') ?>" method="post">    
					<input type="text" class="form-control" id="kd_unit" name="kd_unit" required>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-sm-4" for="nm_unit">Nama Unit</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" id="nm_unit" name="nm_unit" placeholder="Cari Nama Unit">
                  </div>
                </div>
				<div class="form-group">
                  <label class="control-label col-sm-4" for="tglj">Tanggal</label>
                  <div class="col-sm-8">
                    <input type="date" class="form-control" id="tglj" name="tglj" placeholder="">
                  </div>
                </div>
				<div class="form-group">
                  <label class="control-label col-sm-4" for="kd_barang">Kode Jasa</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" id="kd_barang" name="kd_barang" required>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-sm-4" for="nm_barang">Nama jasa</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" id="nm_barang" name="nm_barang" placeholder="Cari Nama Jasa">
                  </div>
                </div>
              </div>
              </div>
              <hr>
              <!--div>
                <form class="form-inline" action="<?php echo base_url('kasir/hitung-diskon/') ?>" method="post">
                  <input type="hidden" name="nofak_dis" id="nofak_dis" value="<?php echo $faktur->no_faktur_penjualan ?>">
                  <input type="hidden" name="sum_belanja" id="sum_belanja" value="<?php echo $belanja->tot_bel ?>">
                  <input style="text-align: right;" type="text" class="form-control" id="diskon" placeholder="Diskon (Rp)" name="diskon" required="required">
                  <input  type="text" class="form-control" id="ket_dis" placeholder="KET" name="ket_dis">
                <br>
                <button type="submit" class="btn btn-success pull-right">Hitung Diskon</button>
                </form>
              </div>
              <br><hr>
                <div>
                
                <input type="hidden" name="nofak_bayar" id="nofak_bayar" value="<?php echo $faktur->no_faktur_penjualan ?>">
                <input type="hidden" name="total_belanja" id="total_belanja" value="<?php echo $belanja->tot_bel ?>">
                <input type="hidden" name="diskon_belanja" id="diskon_belanja" value="<?php echo $faktur->diskon ?>">
                <input type="hidden" name="diskon_ket" id="diskon_ket" value="<?php echo $faktur->ket_diskon ?>">
                <button type="submit" class="btn btn-danger pull-right">Simpan</button>
                </form-->
                </div>
            </div>
          </div>
		            
					  
							            
        </div>
        <div  class="col-md-8">
          <div class="panel panel-default">
            
               <div class="panel-body">

			<div class="table-responsive">
            <table id="tbUser" class="table table-bordered table-responsive">
              <thead>
                <tr>
                  <td align="center">Aksi</td>
                  <td>Kode</td>
                  <td>Nama Jasa</td>
                  <td align="right">Harga</td>
                  <td align="center">Jumlah</td>
                  <td align="center">Diskon %</td>
                  <td align="right">Subtotal</td>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($list->result() as $key): ?>
                <tr>
                  <td align="center"><a onclick="return confirm('Yakin hapus data ini?');" href="<?php echo base_url('kasir/hapus-barang-beli/') . $key->no_faktur_penjualan . "/" . $key->kd_barang ?>">Hapus</a></td>
                  <td><?php echo $key->kd_barang ?></td>
                  <td><?php echo substr($key->nm_barang, 0, 40) ?></td>
                  <td align="right"><?php echo number_format($key->harga, 0, ',', '.') ?></td>
                  <td align="center">
                  <form action="<?php echo base_url('kasir/edit_jumlah_beli/') ?>" method="post">
                    <input name="kd_barang_e" type="hidden" id="kd_barang_e" value="<?php echo $key->kd_barang ?>"/>
                    <input name="nofak_e" type="hidden" id="nofak_e" value="<?php echo $key->no_faktur_penjualan ?>"/>
                    <input style="text-align: center;" name="jml" type="text" id="jml" size="3" onkeypress="return isNumber(event)" value="<?php echo $key->jumlah ?>" />
                  </form>
                  </td>
                  <td align="center">
                  <form action="<?php echo base_url('kasir/edit_diskon_beli/') ?>" method="post">
                    <input name="kd_barang_d" type="hidden" id="kd_barang_d" value="<?php echo $key->kd_barang ?>"/>
                    <input name="nofak_d" type="hidden" id="nofak_d" value="<?php echo $key->no_faktur_penjualan ?>"/>
                    <input name="dis_d" type="text" style="text-align: center;" id="dis_d" size="2" onkeypress="return isNumber(event)" value="<?php echo $key->diskonpersen ?>" />%
                  </form>
                  </td>
                  <td align="right"><?php echo number_format($key->sub_total_jual, 0, ',', '.') ?></td>
                </tr>
<?php
$tot_item += $key->jumlah;
$tot_belanja += $key->sub_total_jual;
?>
                <?php endforeach?>
              </tbody>
              <tr>
                <td colspan="4" align="right"><strong>Total</strong></td>
                <td align="center"><strong><?php echo $tot_item ?> Items</strong></td>
                <td></td>
                <td align="right"><strong>Rp. <?php echo number_format($tot_belanja, 0, ',', '.') ?></strong></td>
              </tr>
            </table>
			</div>
            <!--a class="btn btn-danger pull-right" href="<?php echo base_url('kasir/penjualan-pending/') ?>">Pending</a-->
            </div>
			
          </div>
        </div>
    </div>
  </div>

    <!-- FOOTER SECTION END-->
    <!-- JAVASCRIPT FILES PLACED AT THE BOTTOM TO REDUCE THE LOADING TIME  -->
    <script src="<?php echo base_url() ?>/assets/js/jquery-3.3.1.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/bootstrap.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/custom.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/sweetalert.min.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/toastr.min.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/jquery-ui.min.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/jquery.price_format.min.js"></script>
	
   <script>
      $('form').attr('autocomplete', 'off');
      $('input').attr('autocomplete', 'off');
     	$("ul.nav li.dropdown").hover(function(){
		$(this).find(".dropdown-menu").stop(!0,!0).delay(100).fadeIn(500)},
		function(){$(this).find(".dropdown-menu").stop(!0,!0).delay(100).fadeOut(500),
		$(this).find(".dropdown-submenu").stop(!0,!0).delay(100).fadeIn(500)},
		function(){$(this).find(".dropdown-submenu").stop(!0,!0).delay(100).fadeOut(500)
		});
    
	$("ul.nav li.dropdown-submenu").hover(function(){
		$(this).find(".dropdown-menu1").stop(!0,!0).delay(100).fadeIn(500)},
		function(){$(this).find(".dropdown-menu1").stop(!0,!0).delay(100).fadeOut(500)
	});
	
   var pesan="<?php echo $this->session->flashdata('msg'); ?>",error="<?php echo $this->session->flashdata('error'); ?>";pesan?(toastr.options={positionClass:"toast-top-right"},toastr.success(pesan)):error&&swal(error,"","error");
   
 var pesan="<?php echo $this->session->flashdata('msg'); ?>";pesan&&(toastr.options={positionClass:"toast-top-right"},toastr.success(pesan));

	 function startTime() {
        var today = new Date();
        var h = today.getHours();
        var m = today.getMinutes();
        var s = today.getSeconds();
        m = checkTime(m);
        s = checkTime(s);
        document.getElementById('waktu').innerHTML = h + ":" + m + ":" + s;
        var t = setTimeout(startTime, 500);
      }

      function checkTime(i) {
          if (i < 10) {i = "0" + i};
          return i;
      }

    $(document).ready(function() {
		
			//	$(document).ready(function() {
		//$('#kd_unit1').on('input',function(){
		 $('#unit').change(function(){
              var unit=$(this).val();
			  $.ajax({
                url : "<?php echo base_url('kasir/get_operator1') ;?>",
                method : "POST",
                data : {unit: unit},
                async : true,
				//cache:false,
                dataType : 'json',
                success: function(data){
					var html = '';
                    var i;
                    for(i=0; i<data.length; i++){					
						html += '<option value='+data[i].id_operator+'>'+data[i].nm_operator+'</option>'; 
                    }
                    $('#operator').html(html);

                }
            });
			return false;
        });
		
        startTime();
        $('#kd_barang').focus();
        $('#nm_barang').autocomplete({
          source: "<?php echo base_url('kasir/get_autocomplete/?'); ?>",
          select: function (event, ui) {
            $('[name="nm_barang"]').val(ui.item.label);
            $('[name="kd_barang"]').val(ui.item.kode);
            $('#kd_barang').focus();
          }
        });
		$('#kd_unit').focus();
        $('#nm_unit').autocomplete({
          source: "<?php echo base_url('kasir/get_autocomplete1/?'); ?>",
          select: function (event, ui) {
            $('[name="nm_unit"]').val(ui.item.label);
            $('[name="kd_unit"]').val(ui.item.kode);
            $('#kd_unit').focus();
          }
        });
	//});

   
	
    $("#kd_barang").keypress(function(e){
        var kd_barang= $('#kd_barang').val();
        var nofak = $('#nofak').val();
		 var spbu = $('#unit').val();
		 var shift = $('#shift').val();
		 var operator = $('#operator').val();
        if(e.which==13){
          if (kd_barang) {
            window.top.location.href = "<?php echo base_url('kasir/cekbarang/') ?>"+nofak+"/"+kd_barang+"/"+spbu+"/"+shift+"/"+operator;
          }
          return false;
        }
    });
 });
    function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
      return true;
    }

    $(function(){
        $('#diskon').priceFormat({
                prefix: '',
                centsLimit: 0,
                thousandsSeparator: '.'
        });
    });
    </script>

</body>
</html>