<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="robots" content="noindex, nofollow">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="description" content="Aplikasi Point of Sales" />
    <meta name="author" content="Arlan" />
    <link rel="icon" type="image/png" href="<?php echo base_url('assets/') ?>/img/favicon.png"/>
    <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <title>Laporan Hutang</title>
    <link href="<?php echo base_url() ?>/assets/css/bootstrap.css" rel="stylesheet" />
    <link href="<?php echo base_url() ?>/assets/css/font-awesome.css" rel="stylesheet" />
    <link href="<?php echo base_url() ?>/assets/css/style.css" rel="stylesheet" />

</head>
<body>
    <div class="container">
        <div align="center" class="no-print" id="formFilter" style="background-color: #F5F5F5;padding: 4px">
          <form class="form-inline" action="" method="get">
            <input type="hidden" name="filter" id="filter" value="ok">
              <div class="form-group">
                <label for="a">Tanggal : </label>
                <select name="a" id="a" class="form-control">
                <?php for ($i = 1; $i <= 31; $i++) {?>
                  <option <?php if ($i == $tgl) {echo 'selected';}?> value="<?php echo sprintf('%02d', $i) ?>"><?php echo sprintf('%02d', $i) ?></option>
                <?php }?>
                </select>
                <select name="b" id="b" class="form-control">
                <?php for ($i = 1; $i <= 12; $i++) {?>
                  <option <?php if ($i == $bln) {echo 'selected';}?> value="<?php echo sprintf('%02d', $i) ?>"><?php echo sprintf('%02d', $i) ?></option>
                <?php }?>
                </select>
                <select name="c" id="c" class="form-control">
                <?php for ($i = 2016; $i <= date('Y'); $i++) {?>
                  <option <?php if ($i == $thn) {echo 'selected';}?> value="<?php echo $i ?>"><?php echo $i ?></option>
                <?php }?>
                </select>
              </div>
              <div class="form-group">
                <label for="pwd"> s/d </label>
                <select name="d" id="d" class="form-control">
                    <?php for ($i = 1; $i <= 31; $i++) {?>
                    <option <?php if ($i == $tgl) {echo 'selected';}?> value="<?php echo sprintf('%02d', $i) ?>"><?php echo sprintf('%02d', $i) ?></option>
                    <?php }?>
                </select>
                <select name="e" id="e" class="form-control">
                <?php for ($i = 1; $i <= 12; $i++) {?>
                    <option <?php if ($i == $bln) {echo 'selected';}?> value="<?php echo sprintf('%02d', $i) ?>"><?php echo sprintf('%02d', $i) ?></option>
                <?php }?>
                </select>
                <select name="f" id="f" class="form-control">
                <?php for ($i = 2016; $i <= date('Y'); $i++) {?>
                  <option <?php if ($i == $thn) {echo 'selected';}?> value="<?php echo $i ?>"><?php echo $i ?></option>
                <?php }?>
                </select>
              </div>
              <button type="submit" class="btn btn-danger">Filter</button>
              <a href=""><button type="button" class="btn btn-success" onclick="window.print()">Print</button></a>
          </form>
        </div>
            <h4 align="center">LAPORAN HUTANG</h4>
            <h5 align="center">TOKO : <?php echo $toko->nm_toko ?></h5>
            <?php if ($filter): ?>
                <h5 align="center">TANGGAL : <?php echo date_indo($awal) . " s/d " . date_indo($akhir) ?></h5>
            <?php else: ?>
            <h5 align="center">TANGGAL : <?php echo date_indo($tanggal) ?></h5>
            <?php endif?>
            <table id="tbhutang" class="table table-bordered table-striped table-responsive">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Nomor Faktur</th>
                  <th>Tanggal</th>
				  <th>Year</th>
				  <th>Mount</th>
				  <th>Supplier</th>
                  <th>Nilai</th>            
                </tr>
              </thead>
              <tbody>
                <?php foreach ($hutang->result() as $key): ?>
                <tr>
                  <td align="center"><?php echo $no++ ?></td>
                  <td><?php echo $key->no_faktur_pembelian ?></td>
                  <td><?php echo date_indo($key->tgl_pembelian) ?></td>
				  <td><?php echo $key->Y ?></td>
				  <td><?php echo $key->M ?></td>
				   <td><?php echo $key->nm_supplier ?></td>
                  <td align="right"><?php echo number_format($key->total_pembelian, 0, ',', '.') ?></td>
                </tr>
<?php
$subtot += $key->total_pembelian;

?>
                <?php endforeach?>
              </tbody>
                  <thead>
                    <tr>
                      <td colspan="6" align="center">Total</td>
                      <td align="right"><?php echo number_format($subtot, 0, ',', '.') ?></td>
                     
                    </tr>
                  </thead>
            </table>
			
		<br><br>
		
				   <div class="row row-centered">
				   <div  class="col-md-12">
				   <div class="panel panel-default">
				   <div class="panel-body">
				   
			
			<div class="table-responsive">
			<center><label><u>Resume Laporan Per Supplier</u></label></center>
<table id="tbhutang1" class="table table-bordered table-striped table-responsive">
              <thead>
                <tr>
                  <th>No</th>
				  <th>Supplier</th>
                  <th>Nilai</th>            
                </tr>
              </thead>
              <tbody>
                <?php foreach ($hutang2->result() as $key): ?>
                <tr>
                  <td align="center"><?php echo $no1++ ?></td>
				  <td><?php echo $key->nm_supplier ?></td>
                  <td align="right"><?php echo number_format($key->total_pembelian, 0, ',', '.') ?></td>
                </tr>
<?php
$subtot1 += $key->total_pembelian;
?>
                <?php endforeach?>
              </tbody>
                  <thead>
                    <tr>
                      <td colspan="2" align="center">Total</td>
                      <td align="right"><?php echo number_format($subtot1, 0, ',', '.') ?></td>
                     
                    </tr>
                  </thead>
            </table>
			</div></div>
			
			</div>
		</div>

	
	    </div>		</div>	
</div>
    <script src="<?php echo base_url() ?>/assets/js/jquery-3.3.1.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/bootstrap.js"></script>
    <script>
    $('#tbhutang').on('click','.lihat_record',function(){
        var faktur = $(this).data('faktur');
        var url = "<?php echo base_url('kasir/reprint_struk/') ?>"+faktur;
        window.open(url, '_blank', 'location=yes,height=400,width=500,scrollbars=yes,status=yes');
    });
    </script>
</body>
</html>