  <div class="content-wrapper" style="margin-bottom: 20px">
    <div class="container">
      <div class="row pad-botm">
          <div class="col-md-12">
              <h4 class="header-line">DATA BANK 
			  <span class="pull-right">
			  <button class="btn btn-success" onclick="add_bank()"><i class="glyphicon glyphicon-plus"></i> Tambah Data</button>
			  </span></h4>
		  </div>
	  </div>
	    
      <div class="row">
        <div class="col-md-12">
		<div class="table-responsive">
            <table id="table" class="table table-bordered table-responsive">
            <thead>
                <tr>
                    <th>ID Bank</th>
                    <th>Nama Bank</th>
                    <th>Nomor Rekening</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
            </tbody>

        </table>
        </div>
		</div>
      </div>
    </div>
  </div>
  
    </div>
  </div>
     <!-- CONTENT-WRAPPER SECTION END-->
    <section class="footer-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                   &copy; Copyright <?php echo date('Y') ?>
                </div>
            </div>
        </div>
    </section>
</div>
    <!-- FOOTER SECTION END-->
    <!-- JAVASCRIPT FILES PLACED AT THE BOTTOM TO REDUCE THE LOADING TIME  -->
    <script src="<?php echo base_url() ?>/assets/js/jquery-3.3.1.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/jquery.dataTables.min.js"></script>
	 <script src="<?php echo base_url() ?>/assets/js/dataTables.checkboxes.min.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/bootstrap.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/custom.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/sweetalert.min.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/toastr.min.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/jquery.price_format.min.js"></script>
    <!--script src="<?php echo base_url() ?>/assets/js/bootstrap-select.min.js"></script-->
    <script>


		
var save_method; //for save method string
var table;
 
$(document).ready(function() {
 
    //datatables
    table = $('#table').DataTable({ 
 
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('bank/ajax_list')?>",
            "type": "POST"
        },
 
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ -1 ], //last column
            "orderable": false, //set not orderable
        },
        ],
 
    });
 

});
 
function add_bank()
{
    save_method = 'add';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#modal_form').modal('show'); // show bootstrap modal
    $('.modal-title').text('Tambah Data'); // Set Title to Bootstrap modal title
}
 
function edit_bank(id)
{
    save_method = 'update';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
 
    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo site_url('bank/ajax_edit')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
 
            $('[name="id"]').val(data.id_bank);
            $('[name="bank"]').val(data.nm_bank);
            $('[name="no_rek"]').val(data.no_rek);
			$('[name="ket"]').val(data.ket);
            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Data'); // Set title to Bootstrap modal title
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}
 
function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax 
}
 
function save()
{
    $('#btnSave').text('saving...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
    var url;
 
    if(save_method == 'add') {
        url = "<?php echo site_url('bank/ajax_add')?>";
    } else {
        url = "<?php echo site_url('bank/ajax_update')?>";
    }
 
    // ajax adding data to database
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data)
        {
 
            if(data.status) //if success close modal and reload ajax table
            {
                $('#modal_form').modal('hide');
                reload_table();
            }
 
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 
 
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 
 
        }
    });
}
 
function delete_bank(id)
{
    if(confirm('Apakah Kamu Yakin Menghapus Data Bank Ini?'))
    {
        // ajax delete data to database
        $.ajax({
            url : "<?php echo site_url('bank/ajax_delete')?>/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                //if success reload ajax table
                $('#modal_form').modal('hide');
                reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });
 
    }
}
 

		
	  $(function(){
          $('#nominal').priceFormat({
              prefix: '',
              centsLimit: 0,
              thousandsSeparator: '.'
          });
      });
	  
	  $('form').attr('autocomplete', 'off');
     	$("ul.nav li.dropdown").hover(function(){
		$(this).find(".dropdown-menu").stop(!0,!0).delay(100).fadeIn(500)},
		function(){$(this).find(".dropdown-menu").stop(!0,!0).delay(100).fadeOut(500),
		$(this).find(".dropdown-submenu").stop(!0,!0).delay(100).fadeIn(500)},
		function(){$(this).find(".dropdown-submenu").stop(!0,!0).delay(100).fadeOut(500)
		});
      var pesan="<?php echo $this->session->flashdata('msg'); ?>";pesan&&(toastr.options={positionClass:"toast-top-right"},toastr.success(pesan));
	$("ul.nav li.dropdown-submenu").hover(function(){
		$(this).find(".dropdown-menu1").stop(!0,!0).delay(100).fadeIn(500)},
		function(){$(this).find(".dropdown-menu1").stop(!0,!0).delay(100).fadeOut(500)
	});

    </script>
	
	<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Bank Form</h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal">
                    <input type="hidden" value="" name="id"/> 
                    <div class="form-body">
			<div class="form-group">
              <label class="control-label col-sm-3" for="bank">Nama Bank</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" id="bank" name="bank" required>
              </div>
            </div>
			<div class="form-group">
              <label class="control-label col-sm-3" for="no_rek">Nomor Rekening</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" id="no_rek" name="no_rek" required>
              </div>
            </div>
			<div class="form-group">
              <label class="control-label col-sm-3" for="ket">Keterangan</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" id="ket" name="ket" required>
              </div>
            </div>
                        
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

</body>
</html>