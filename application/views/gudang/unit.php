  <div class="content-wrapper" style="margin-bottom: 20px">
    <div class="container">
      <div class="row pad-botm">
          <div class="col-md-12">
              <h4 class="header-line">LOKASI CABANG <span class="pull-right"><a href="" data-toggle="modal" data-target="#myModal">Tambah Unit</a></span></h4>
          </div>
      </div>
      <div class="row">
        <div class="col-md-12">
            <table id="tbUnit" class="table table-bordered table-striped table-responsive">
              <thead>
                <tr>
				  <th>Nomor SPBU</th>
                  <th>Nama Cabang</th>
				   <th>Alamat</th>
				  <th>Nama Operator</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($unit1->result() as $key): ?>
			
                <tr>
                  <td><?php echo $key->no_spbu ?></td>
                  <td><?php echo $key->nm_cabang ?></td>
                  <td><?php echo $key->alamat ?></td>
				 <td class="no-print" align="center"><?php echo $key->nm_operator ?></td>
                  <td align="center"><a href="javascript:void(0);" class="edit_record" data-id_unit="<?php echo $key->id_unit ?>" data-no_area="<?php echo $key->no_area ?>" data-nama_cabang="<?php echo $key->nm_cabang ?>" data-no_spbu="<?php echo $key->no_spbu ?>" data-alamat="<?php echo $key->alamat ?>">Edit</a> | <a href="javascript:void(0);" class="hapus_record" data-no_spbu="<?php echo $key->no_spbu ?>" data-nama_cabang="<?php echo $key->nm_cabang ?>">Hapus</a></td>
                </tr>
                <?php endforeach?>
              </tbody>
            </table>
        </div>
      </div>
    </div>
  </div>
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Tambah Unit</h4>
        </div>
        <div class="modal-body">
          <form class="form-horizontal" action="<?php echo base_url('gudang/simpan_unit') ?>" method="post">
            <div class="form-group">
              <label class="control-label col-sm-2" for="no_area">No. Area</label>
              <div class="col-sm-5">
                <input type="text" class="form-control" id="no_area" name="no_area" required>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-2" for="nm_cabang">Nama Cabang</label>
              <div class="col-sm-5">
                <input type="text" class="form-control" id="nm_cabang" name="nm_cabang" required>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-2" for="no_spbu">No. SPBU</label>
              <div class="col-sm-5">
                <input type="text" class="form-control" id="no_spbu" name="no_spbu" required>
              </div>
            </div>
			<div class="form-group">
              <label class="control-label col-sm-2" for="alamat">Alamat</label>
              <div class="col-sm-5">
                <input type="text" class="form-control" id="alamat" name="alamat" required>
              </div>
            </div>
			<!--div class="form-group">
              <label class="control-label col-sm-2" for="nm_op">Nama Operator</label>
              <div class="col-sm-5">
                <input type="text" class="form-control" id="nm_op" name="nm_op" required>
              </div>
            </div-->
			<div class="form-group">
              <label class="control-label col-sm-2" for="operator">Operator</label>
              <div class="col-sm-5">
                <select name="operator[]" id="operator" class="bootstrap-select" data-live-search="true" multiple>
                  <?php foreach ($operator->result() as $key): ?>
                      <option value="<?php echo $key->id_operator; ?>"><?php echo $key->nm_operator; ?></option>
                  <?php endforeach;?>
                </select>
              </div>
            </div>
            
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit"  class="btn btn-primary">Simpan</button>
        </div>
        </form>
      </div>
    </div>
  </div>
  <div class="modal fade" id="modalEdit" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Edit Unit</h4>
        </div>
        <div class="modal-body">
          <form class="form-horizontal" action="<?php echo base_url('gudang/simpan_unit_edit') ?>" method="post">
            <input  type="hidden" class="form-control" id="id_unit_e" name="id_unit_e" required readonly>

			<div class="form-group">
              <label class="control-label col-sm-3" for="no_area_e">Nomor Area</label>
              <div class="col-sm-5">
                <input type="text" class="form-control" id="no_area_e" name="no_area_e" required readonly>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-3" for="nm_cabang_e">Nama Cabang</label>
              <div class="col-sm-5">
                <input type="text" class="form-control" id="nm_cabang_e" name="nm_cabang_e" required>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-3" for="no_spbu_e">Nomor SPBU</label>
              <div class="col-sm-6">
                <input type="text" class="form-control" id="no_spbu_e"  name="no_spbu_e" required readonly>
              </div>
            </div>
			<div class="form-group">
              <label class="control-label col-sm-3" for="alamat_e">Alamat</label>
              <div class="col-sm-6">
                <input type="text" class="form-control" id="alamat_e"  name="alamat_e" required>
              </div>
            </div>
			<!--div class="form-group">
              <label class="control-label col-sm-3" for="nm_op_e">Nama Operator</label>
              <div class="col-sm-6">
                <input type="text" class="form-control" id="nm_op_e"  name="nm_op_e" required>
              </div>
            </div-->
			<div class="form-group">
              <label class="control-label col-sm-3" for="operator_e">Operator</label>
              <div class="col-sm-5">
                <select name="operator_e[]" id="operator_e" class="bootstrap-select strings" data-live-search="true" multiple>
                  <?php foreach ($operator->result() as $key): ?>
                      <option value="<?php echo $key->id_operator; ?>"><?php echo $key->nm_operator; ?></option>
                  <?php endforeach;?>
                </select>
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit"  class="btn btn-primary">Simpan</button>
        </div>
        </form>
      </div>
    </div>
  </div>
  <!-- Modal Hapus -->
  <div class="modal fade" id="modalHapus" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Hapus Menu</h4>
        </div>
        <div class="modal-body">
          <form class="form-horizontal" action="<?php echo base_url('gudang/hapus_unit') ?>" method="post">
           <h4>Apakah Kamu Yakin Menghapus Data Unit Ini?</h4>
        </div>
        <input type="hidden" id="no_spbu_h" name="no_spbu_h">
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
          <button type="submit" id="btnHapus" class="btn btn-primary">Ya</button>
        </div>
        </form>
      </div>
    </div>
  </div>
     <!-- CONTENT-WRAPPER SECTION END-->
    <section class="footer-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                   &copy; Copyright <?php echo date('Y') ?>
                </div>
            </div>
        </div>
    </section>
</div>
    <!-- FOOTER SECTION END-->
    <!-- JAVASCRIPT FILES PLACED AT THE BOTTOM TO REDUCE THE LOADING TIME  -->
    <script src="<?php echo base_url() ?>/assets/js/jquery-3.3.1.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/bootstrap.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/custom.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/sweetalert.min.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/toastr.min.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/sweetalert.min.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/toastr.min.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/jquery.price_format.min.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/bootstrap-select.min.js"></script>
  
	<script>
      $('#tbUnit').DataTable({
          "paging":   false,
          "ordering": false,
      });
      $('form').attr('autocomplete', 'off');
     	$("ul.nav li.dropdown").hover(function(){
		$(this).find(".dropdown-menu").stop(!0,!0).delay(100).fadeIn(500)},
		function(){$(this).find(".dropdown-menu").stop(!0,!0).delay(100).fadeOut(500),
		$(this).find(".dropdown-submenu").stop(!0,!0).delay(100).fadeIn(500)},
		function(){$(this).find(".dropdown-submenu").stop(!0,!0).delay(100).fadeOut(500)
		});
      var pesan="<?php echo $this->session->flashdata('msg'); ?>";pesan&&(toastr.options={positionClass:"toast-top-right"},toastr.success(pesan));
	$("ul.nav li.dropdown-submenu").hover(function(){
		$(this).find(".dropdown-menu1").stop(!0,!0).delay(100).fadeIn(500)},
		function(){$(this).find(".dropdown-menu1").stop(!0,!0).delay(100).fadeOut(500)
	});

      $(document).ready(function() {
		  $('.bootstrap-select').selectpicker();
          //$('#tbUnit').on('click','.edit_record',function(){
			$('.edit_record').on('click',function(){
              var id_unit=$(this).data('id_unit');
			  var no_area=$(this).data('no_area');
              var nm_cabang=$(this).data('nama_cabang');
              var no_spbu=$(this).data('no_spbu');
			  var alamat=$(this).data('alamat');
			  var nm_op=$(this).data('nm_op');
			  $(".strings").val('');
              $('#modalEdit').modal('show');
              $('[name="id_unit_e"]').val(id_unit);
			  $('[name="no_area_e"]').val(no_area);
              $('[name="nm_cabang_e"]').val(nm_cabang);
              $('[name="no_spbu_e"]').val(no_spbu);
			  $('[name="alamat_e"]').val(alamat);
			  
			  $.ajax({
                    url: "<?php echo base_url('gudang/get_operator_by_unit'); ?>",
                    method: "POST",
                    data :{no_spbu:no_spbu},
                    cache:false,
                    success : function(data){
                        var item=data;
                        var val1=item.replace("[","");
                        var val2=val1.replace("]","");
                        var values=val2;
                        var akhir = values.replace(/['"]+/g, '');
                        $.each(akhir.split(","), function(i,e){
                            $(".strings option[value='" + e + "']").prop("selected", true).trigger('change');
                            $(".strings").selectpicker('refresh');
                        });
                    }

                });
                return false;
          });

          /*$('#tbUnit').on('click','.hapus_record',function(){
              var no_spbu=$(this).data('no_spbu');
			  var nm_cabang=$(this).data('nama_cabang');
              var result = confirm("Apakah yakin akan menghapus cabang "+nm_cabang+" ini?");
              var akses = "<?php echo $this->session->userdata('akses') ?>";
              if (akses == 'manajer' || akses == 'admin') {
                  if (result) {
                    window.location.href = '<?php echo base_url('gudang/hapus_unit/') ?>'+no_spbu;
                  }
              }
          });
		  */
		          //GET CONFIRM DELETE
            $('.hapus_record').on('click',function(){
                var no_spbu = $(this).data('no_spbu');
                $('#modalHapus').modal('show');
                $('[name="no_spbu_h"]').val(no_spbu);
            });
      });

      $("#id_user").keypress(function(e){
          if(e.which==32){
              return false;
          }
      });

    </script>

</body>
</html>