  <div class="content-wrapper" style="margin-bottom: 20px">
    <div class="container">
      <div class="row">
	    <div class="col-md-12">
		
              <h4 class="header-line">KLIRING PEMBAYARAN</h4>
          </div>
		 
		    <div  class="col-md-8">
			    
	    <form class="form-inline" action="" method="get" >
		<div class="panel-body">
		<div class="form-group">
			
				<label for="kd_supplier">Supplier</label>
				<select name="kd_supplier" id="kd_supplier" style="width: 30em" class="form-control"  required>
                <option value="">Pilih Supplier</option>
				<?php foreach ($kdsupplier->result() as $key): ?>
					<option <?php if ($kdsup == $key->kd_supplier1) {echo 'selected';}?> value="<?php echo $key->kd_supplier1?>"><?php echo $key->kd_supplier1; echo " - ".$key->nm_supplier; ?></option>
				<?php endforeach?>
                </select>
				
				</div>
				
				<button type="submit" class="btn btn-default">Tampilkan</button>
				
				</form>
				</div>
				</div>
		
			
				 <div style="margin-left: 20px;margin-right: 20px">
				   <div class="row">
				   <div  class="col-md-6">
				   <div class="panel panel-default">
				   <div class="panel-body">
				   
			
			<div class="table-responsive">
			<center><label><u>PEMBELIAN</u></label></center>
            <table id="tbKliring" class="table table-bordered table-responsive display">
              <thead>
                <tr>
				  <th><!--input type="checkbox" id="check-all"--></th>
                  <th>No.Faktur Pembelian</th>
                  <th>Tanggal Pembelian</th>
                  <th>Kode Supplier</th>
				  <th>Nominal</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($kdsplr->result() as $key): ?>
                <tr>
				  <!--td><?php echo "<input type='checkbox' class='check-item' name='no_faktur1'  id='no_faktur1' value='".$key->no_faktur_pembelian."'>" ?></td-->
                  <td><?php echo "<input type='checkbox' class='check-item' name='no_faktur1'  id='no_faktur1' value='".$key->no_faktur_pembelian."'>" ?></td>
				  <td><?php echo $key->no_faktur_pembelian ?></td>
				  <td><?php echo $key->tgl_pembelian ?></td>
				  <td><?php echo $key->kd_supplier1 ?></td>
                  <td align="right"><?php echo number_format($key->total_pembelian, 0, ',', '.') ?></td>
                  
                </tr>
		<?php
			/*$spbu = $key->no_spbu2;
			$no_faktur = $key->no_faktur_pembelian;
			//$no_faktur_bayar = $key->no_faktur_bayar;
			$tgl_bayar = $key->tgl_bayar;
			$no_spbu2 = $key->no_spbu2;
			$nominal = $key->nominal;
			$tot_jual += $key->total_pembelian;
			$tot_bayar += $key->nominal;
			if($tot_jual>$tot_bayar){
				$kurang=$tot_jual-$tot_bayar;
			}elseif($tot_jual<$tot_bayar){
				$kurang=$tot_bayar-$tot_jual;
			}
			*/
		?>
       <?php endforeach?>

				</div>
			</div>
			</div>
		</div>

<form>
<table>
</table>
</form>
</div>
</div>
 </div>
  </div>


  

		<div  class="col-md-6">
		  <div class="panel panel-default">
		  <div class="panel-body">
		<div class="form-horizontal">
		<div class="table-responsive">
		<center><label><u>PEMBAYARAN</u></label></center>
            <table id="tbKliring2" class="table table-bordered table-responsive display">
              <thead>
                <tr>
				  <th></th>
                  <th>ID Pembayaran</th>
                  <th>Tanggal Pembayaran</th>
                  <th>Kode Supplier</th>
				  <th>Nominal</th>
                </tr>
              </thead>
              <tbody>
			  <?php foreach ($kdsplr1->result() as $key): ?>
                <tr>
				  <td><?php echo "<input type='checkbox' class='check-item2' name='id_bayar' id='id_bayar' value='".$key->id_bayar."'>" ?></td>
				  <td><?php echo $key->id_bayar ?></td>
				  <td><?php echo $key->tgl_bayar ?></td>
				  <td><?php echo $key->kd_supplier2 ?></td>
				  <td align="right"><?php echo number_format($key->nominal, 0, ',', '.') ?></td>
                  
                </tr>
			  <?php endforeach?>
				</div>
				
				</div>
			</div>
			</div>
	    </div></div>
<form>
<table>
</table>
</form>
</div>
</div>
</div>
</div>
</div>
</div>
</div>



		
				 <!--div style="margin-left: 20px;margin-right: 20px">
				   <div class="row">
				   <div  class="col-md-6">
				   <div class="panel panel-default">
				   <div class="panel-body">
			<div class="table-responsive">
		<center><label><u>SISAH KLIRING</u></label></center>
            <table id="tbKliring3" class="table table-bordered table-responsive display">
              <thead>
                <tr>
				  <th></th>
                  <th>Tanggal Pembelian</th>
                  <th>Tanggal Pembayaran</th>
                  <th>Kode Supplier</th>
				  <th>Selisih</th>
                </tr>
              </thead>
              <tbody>
			  <?php foreach ($kdsplr2->result() as $key): ?>
                <tr>
				  <td><?php echo "<input type='checkbox' class='check-item3' name='id_kliring' id='id_kliring' value='".$key->id_kliring."'>" ?></td>
				  <td><?php echo $key->tgl_pembelian1 ?></td>
				  <td><?php echo $key->tgl_bayar1 ?></td>
				  <td><?php echo $key->kd_supplier3 ?></td>
				  <td align="right"><?php echo number_format($key->selisih, 0, ',', '.') ?></td>
                  
                </tr>
			  <?php endforeach?>
				</div>
				
				</div>
			</div>
			</div>
	    </div></div>
		
		
<form>
<table>
</table>
</form>
</div>
</div>
 </div>
 </div-->

 
 <div class="form-group pull-left">
 
 		<div  class="col-md-12">
		  <div class="panel panel-default">
		  <div class="panel-body">
		  <div class="form-horizontal">
		<div class="table-responsive">

 <form action="<?php echo base_url().'gudang/simpan_kliring_bayar'?>" method="post">
           
				 <input type="hidden" id="no_faktur_pembelian" name="no_faktur_pembelian" class="no_faktur_pembelian">
				<input type="hidden" id="ksplr" name="ksplr" class="ksplr">
				<input type="hidden" id="id_bayar" name="id_bayar" class="id_bayar">
				<input type="hidden" id="tgl_pembelian" name="tgl_pembelian" class="tgl_pembelian">
				<input type="hidden" id="tgl_bayar" name="tgl_bayar" class="tgl_bayar">
				<input type="hidden" id="id_k_s" name="id_k_s" class="id_k_s">
	
			<div class="table-responsive">
			
		   <table>

                <tr>
                    <th>Total Pembelian </th>
					<th> </th>
                    <th><input type="text" name="total_pembelian" class="total_pembelian form-control input-sm" style="text-align:right;margin-bottom:5px;" readonly  ></th>
                    <input type="hidden" id="total_pembelian" name="total_pembelian" class="total_pembelian form-control input-sm" >
                </tr>
				<tr>
                    <th>Total Pembayaran </th>
					<th> </th>
                   <th><input type="text" name="total_bayar" class="total_bayar form-control input-sm" style="text-align:right;margin-bottom:5px;" readonly ></th>
                    <input type="hidden" id="total_bayar" name="total_bayar" class="total_bayar form-control input-sm"  >
              </tr>
			  <!--tr>
                    <th>Sisah Sebelumnya </th>
					<th> </th>
                   <th><input type="text" name="sisah" class="sisah form-control input-sm" style="text-align:right;margin-bottom:5px;" readonly ></th>
                    <input type="hidden" id="sisah" name="sisah" class="sisah form-control input-sm" >
                   </tr-->
				    <input type="hidden" id="sisah" name="sisah" class="sisah form-control input-sm" >
			
			 <tr>
                    <th>Potongan PPH</th>
					<th> </th>
                   <th><input type="text" id="pot_pph" name="pot_pph" class="pot_pph form-control input-sm" style="text-align:right;margin-bottom:5px;"></th>
                    <!--input type="hidden" id="pot_pph" name="pot_pph" class="pot_pph form-control input-sm" -->
                   </tr>
			 
			 <tr>
                    <th>Biaya Bank </th>
					<th> </th>
                   <th><input type="text" id="biaya_bank" name="biaya_bank" class="biaya_bank form-control input-sm" style="text-align:right;margin-bottom:5px;"></th>
                    <!--input type="hidden" id="biaya_bank" name="biaya_bank" class="biaya_bank form-control input-sm" -->
                   </tr>
			 <tr>
               <tr>
                    <th>Selisih </th>
					<th> </th>
                   <th><input type="text" name="selisih" class="selisih form-control input-sm" style="text-align:right;margin-bottom:5px;" readonly ></th>
                    <input type="hidden" id="selisih" name="selisih" class="selisih form-control input-sm" >
                   </tr>
			 <tr>
				<td><button type='submit' class='simpan btn btn-info btn-lg'> Simpan</button></td>
				</tr>
				

            </table>
			  </div>
		
            </form>

	
				</div>
				
				</div>
			</div>
			</div>
	    </div></div>
		
			
	

     <!-- CONTENT-WRAPPER SECTION END-->
    <section class="footer-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                   &copy; Copyright <?php echo date('Y') ?>, 
                </div>
            </div>
        </div>
    </section>
    <!-- FOOTER SECTION END-->
    <!-- JAVASCRIPT FILES PLACED AT THE BOTTOM TO REDUCE THE LOADING TIME 

	-->
      <script src="<?php echo base_url() ?>/assets/js/jquery-3.3.1.js"></script>

	  <script src="<?php echo base_url() ?>/assets/js/jquery.dataTables.min.js"></script>
	<script src="<?php echo base_url() ?>/assets/js/dataTables.select.min.js"></script>
	 <script src="<?php echo base_url() ?>/assets/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/bootstrap.js"></script>
 <script src="<?php echo base_url() ?>/assets/js/dataTables.checkboxes.min.js"></script>
   
    <script src="<?php echo base_url() ?>/assets/js/sweetalert.min.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/toastr.min.js"></script>
	    <script src="<?php echo base_url() ?>/assets/js/jquery.price_format.min.js"></script>
		<script>
		$("ul.nav li.dropdown").hover(function(){
		$(this).find(".dropdown-menu").stop(!0,!0).delay(100).fadeIn(500)},
		function(){$(this).find(".dropdown-menu").stop(!0,!0).delay(100).fadeOut(500),
		$(this).find(".dropdown-submenu").stop(!0,!0).delay(100).fadeIn(500)},
		function(){$(this).find(".dropdown-submenu").stop(!0,!0).delay(100).fadeOut(500)
		});
      var pesan="<?php echo $this->session->flashdata('msg'); ?>";pesan&&(toastr.options={positionClass:"toast-top-right"},toastr.success(pesan));
	$("ul.nav li.dropdown-submenu").hover(function(){
		$(this).find(".dropdown-menu1").stop(!0,!0).delay(100).fadeIn(500)},
		function(){$(this).find(".dropdown-menu1").stop(!0,!0).delay(100).fadeOut(500)
		});
		
	$(document).ready(function() {
		$('.simpan').hide();
	$('#tbKliring').DataTable( {
		"searching": false,
        columnDefs: [ {
            orderable: false,
            //className: 'select-checkbox',
            targets:   0
        } ],
        select: {
            style:    'os',
            selector: 'td:first-child'
        },
        order: [[ 1, 'asc' ]]
    
	 });
	
       $('#tbKliring').on( 'change', '.check-item', function (e) {
			$('input[name=no_faktur1]').not(this).prop('checked', false);
			var no_faktur1   = $('input[name=no_faktur1]:checked').val();
			$('.simpan').show();
			  $.ajax({
                url : "<?php echo base_url('gudang/get_nofaktur1') ;?>",
                method : "POST",
                data : {no_faktur1: no_faktur1},
                async : true,
				//cache:false,
                dataType : 'json',
                success: function(data){
					var nofak= '';
					var total_pemb= '';
					var ksplr= '';
					var tgl_pemb= '';
                    var i;
                    for(i=0; i<data.length; i++){
						nofak +=data[i].no_faktur_pembelian;
						total_pemb +=data[i].total_pembelian;
						ksplr +=data[i].kd_supplier;
						tgl_pemb +=data[i].tgl_pembelian;
                    }
					$('.no_faktur_pembelian').val(nofak);
                    $('.total_pembelian').val(total_pemb);
					$('.ksplr').val(ksplr);
					$('.tgl_pembelian').val(tgl_pemb);
					
			var topemb = $('.total_pembelian').val();
            var tobyr = $('.total_bayar').val();
			var sish = $('.sisah').val();
			var potpph = $('.pot_pph').val();
			var b_bank = $('.biaya_bank').val();
			var slish = $('.selisih').val();
            var topemb1 = topemb.replace(/[^\d]/g,"");
			var tobyr1 = tobyr.replace(/[^\d]/g,"");
			var sish1 = sish.replace(/[^\d]/g,"");
			var slish1 = sish.replace(/[^\d]/g,"");
			var b_bank1 = b_bank.replace(/[^\d]/g,"");
			var potpph1 = potpph.replace(/[^\d]/g,"");
			//var selisih =(Number(tobyr1)-Number(topemb1));
			//if(sish!=""){
			if(sish<0){
			var selisih =((Number(tobyr1) - Number(topemb1)) + (- Number(sish1))-(- Number(potpph1))-(- Number(b_bank1)));
					if((Number(selisih)) !=0)
					{
						$('.simpan').hide();
					}else{
						$('.simpan').show();
					}
			}else{
					var selisih =((Number(tobyr1) - Number(topemb1)) + (Number(sish1))-(Number(potpph1))-(Number(b_bank1)));
					if((Number(selisih)) !=0)
					{
						$('.simpan').hide();
					}else{
						$('.simpan').show();
					}
			}
			//}
			$('.selisih').val(selisih);	
            }
            });
			return false;	
        });	
		
		$('#tbKliring2').DataTable( {
		"searching": false,
        columnDefs: [ {
            orderable: false,
            //className: 'select-checkbox',
            targets:   0
        } ],
        select: {
            style:    'os',
            selector: 'td:first-child'
        },
        order: [[ 1, 'asc' ]]
		});
	 
		
		$('#tbKliring2').on( 'change', '.check-item2', function (e) {
			$('input[name=id_bayar]').not(this).prop('checked', false);
			var id_bayar = $('input[name=id_bayar]:checked').val();
			$('.simpan').show();
			  $.ajax({
                url : "<?php echo base_url('gudang/get_bayar1') ;?>",
                method : "POST",
                data : {id_bayar: id_bayar},
                async : true,
				//cache:false,
                dataType : 'json',
                success: function(data){
					var id_byr = '';
					var nomnal = '';
					var tgl_byr = '';
                    var i;
                    for(i=0; i<data.length; i++){					
						id_byr +=data[i].id_bayar;
						nomnal +=data[i].nominal;
						tgl_byr +=data[i].tgl_bayar;
                    }
					$('.id_bayar').val(id_byr);
                    $('.total_bayar').val(nomnal);
					$('.tgl_bayar').val(tgl_byr);

			var topemb = $('.total_pembelian').val();
            var tobyr = $('.total_bayar').val();
			var sish = $('.sisah').val();
			var potpph = $('.pot_pph').val();
			var b_bank = $('.biaya_bank').val();
			var slish = $('.selisih').val();
            var topemb1 = topemb.replace(/[^\d]/g,"");
			var tobyr1 = tobyr.replace(/[^\d]/g,"");
			var sish1 = sish.replace(/[^\d]/g,"");
			var slish1 = sish.replace(/[^\d]/g,"");
			var b_bank1 = b_bank.replace(/[^\d]/g,"");
			var potpph1 = potpph.replace(/[^\d]/g,"");
			//var selisih =(Number(tobyr1)-Number(topemb1));
			//if(sish!=""){
			if(sish<0){
			var selisih =((Number(tobyr1) - Number(topemb1)) + (- Number(sish1))-(- Number(potpph1))-(- Number(b_bank1)));
					if((Number(selisih)) !=0)
					{
						$('.simpan').hide();
					}else{
						$('.simpan').show();
					}
			}else{
					var selisih =((Number(tobyr1) - Number(topemb1)) + (Number(sish1))-(Number(potpph1))-(Number(b_bank1)));
					if((Number(selisih)) !=0)
					{
						$('.simpan').hide();
					}else{
						$('.simpan').show();
					}
			}
			//}
			$('.selisih').val(selisih);
			
			}	
            });
			return false;	
	});
	
	$('#tbKliring3').DataTable( {
		"searching": false,
        columnDefs: [ {
            orderable: false,
            //className: 'select-checkbox',
            targets:   0
        } ],
        select: {
            style:    'os',
            selector: 'td:first-child'
        },
        order: [[ 1, 'asc' ]]
    
	});

	$('#tbKliring3').on( 'change', '.check-item3', function (e) {
			$('input[name=id_kliring]').not(this).prop('checked', false);
			var id_kliring= $('input[name=id_kliring]:checked').val();
			  $.ajax({
                url : "<?php echo base_url('gudang/get_kliring1') ;?>",
                method : "POST",
                data : {id_kliring: id_kliring},
                async : true,
				//cache:false,
                dataType : 'json',
                success: function(data){
					var id_klring = '';
					var slsih = '';
					
                    var i;
                    for(i=0; i<data.length; i++){					
						id_klring +=data[i].id_kliring;
						slsih +=data[i].selisih;
                    }
					$('.id_k_s').val(id_klring);
                    $('.sisah').val(slsih);
				

var topemb = $('.total_pembelian').val();
            var tobyr = $('.total_bayar').val();
			var sish = $('.sisah').val();
			var potpph = $('.pot_pph').val();
			var b_bank = $('.biaya_bank').val();
			var slish = $('.selisih').val();
            var topemb1 = topemb.replace(/[^\d]/g,"");
			var tobyr1 = tobyr.replace(/[^\d]/g,"");
			var sish1 = sish.replace(/[^\d]/g,"");
			var slish1 = sish.replace(/[^\d]/g,"");
			var b_bank1 = b_bank.replace(/[^\d]/g,"");
			var potpph1 = potpph.replace(/[^\d]/g,"");
			if(sish<0){
			var selisih =((Number(tobyr1) - Number(topemb1)) + (- Number(sish1))-(- Number(potpph1))-(- Number(b_bank1)));
					if((Number(selisih)) !=0)
					{
						$('.simpan').hide();
					}else{
						$('.simpan').show();
					}
			}else{
					var selisih =((Number(tobyr1) - Number(topemb1)) + (Number(sish1))-(Number(potpph1))-(Number(b_bank1)));
					if((Number(selisih)) !=0)
					{
						$('.simpan').hide();
					}else{
						$('.simpan').show();
					}
			}
			$('.selisih').val(selisih);	

			}	
            });
			return false;	
	});
	
	$('.pot_pph').on("input",function(){	
			var topemb = $('.total_pembelian').val();
            var tobyr = $('.total_bayar').val();
			var sish = $('.sisah').val();
			var potpph = $('.pot_pph').val();
			var b_bank = $('.biaya_bank').val();
			var slish = $('.selisih').val();
            var topemb1 = topemb.replace(/[^\d]/g,"");
			var tobyr1 = tobyr.replace(/[^\d]/g,"");
			var sish1 = sish.replace(/[^\d]/g,"");
			var slish1 = sish.replace(/[^\d]/g,"");
			var b_bank1 = b_bank.replace(/[^\d]/g,"");
			var potpph1 = potpph.replace(/[^\d]/g,"");
			if(sish<0){
			var selisih =((Number(tobyr1) - Number(topemb1)) + (- Number(sish1))-(- Number(potpph1))-(- Number(b_bank1)));
					if((Number(selisih)) !=0)
					{
						$('.simpan').hide();
					}else{
						$('.simpan').show();
					}
			}else{
					var selisih =((Number(tobyr1) - Number(topemb1)) + (Number(sish1))-(Number(potpph1))-(Number(b_bank1)));
					if((Number(selisih)) !=0)
					{
						$('.simpan').hide();
					}else{
						$('.simpan').show();
					}
			}
			$('.selisih').val(selisih);				
            })
            
	
	
	 $('.biaya_bank').on("input",function(){	
			var topemb = $('.total_pembelian').val();
            var tobyr = $('.total_bayar').val();
			var sish = $('.sisah').val();
			var potpph = $('.pot_pph').val();
			var b_bank = $('.biaya_bank').val();
			var slish = $('.selisih').val();
            var topemb1 = topemb.replace(/[^\d]/g,"");
			var tobyr1 = tobyr.replace(/[^\d]/g,"");
			var sish1 = sish.replace(/[^\d]/g,"");
			var slish1 = sish.replace(/[^\d]/g,"");
			var b_bank1 = b_bank.replace(/[^\d]/g,"");
			var potpph1 = potpph.replace(/[^\d]/g,"");
			if(sish<0){
			var selisih =((Number(tobyr1) - Number(topemb1)) + (- Number(sish1))-(Number(potpph1))-(- Number(b_bank1)));
				if((Number(selisih)) !=0)
					{
						$('.simpan').hide();
					}else{
						$('.simpan').show();
					}
					//$('.simpan').show();
			}else{
					var selisih =((Number(tobyr1) - Number(topemb1)) + (Number(sish1))-(Number(potpph1))-(Number(b_bank1)));
					if((Number(selisih)) !=0)
					{
						$('.simpan').hide();
					}else{
						$('.simpan').show();
					}
					//$('.simpan').show();
			}
			$('.selisih').val(selisih);
            })
            
	

			
});

	
	function convertToRupiah(angka){
          var rupiah = '';
          var angkarev = angka.toString().split('').reverse().join('');
          for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
          return rupiah.split('',rupiah.length-1).reverse().join('');
    };

		
  // Document ready cek start
</script>

</body>
</html>