  <div class="content-wrapper" style="margin-bottom: 20px">
    <div class="container">
      <div class="row pad-botm">
          <div class="col-md-12">
              <h4 class="header-line">Satuan Bahan <span class="pull-right no-print"><a href="" data-toggle="modal" data-target="#myModal">Tambah Satuan</a></span></h4>
          </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <!--button type="button" class="btn btn-success no-print" onclick="window.print();return false;">Print</button-->
            <table id="tbUser" class="table table-bordered table-striped table-responsive">
              <thead>
                <tr>
                  <th>Kode Satuan</th>
                  <th>Nama Satuan</th>
                  <th class="no-print">Aksi</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($satuan->result() as $key): ?>
                <tr>
                  <!--td><?php echo $no++ ?></td-->
                  <td><?php echo $key->kd_satuan ?></td>
                  <td><?php echo $key->nm_satuan ?></td>
				 <td align="center"><a href="javascript:void(0);" class="edit_record"  data-kode="<?php echo $key->kd_satuan; ?>" data-nama="<?php echo $key->nm_satuan; ?>">Edit</a> | <a href="javascript:void(0);" class="delete-record" data-kode="<?php echo $key->kd_satuan ?>">Hapus</a></td>
                </tr>
                <?php endforeach?>
              </tbody>
            </table>
        </div>
      </div>
    </div>
  </div>
  <!-- Modal Tambah -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Tambah Satuan</h4>
        </div>
        <div class="modal-body">
          <form class="form-horizontal" action="<?php echo base_url('gudang/simpan_satuan') ?>" method="post">
            <div class="form-group">
              <label class="control-label col-sm-3" for="kd_satuan">Kode Satuan</label>
              <div class="col-sm-5">
                <input type="text" class="form-control" id="kd_satuan" name="kd_satuan" required>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-3" for="nm_satuan">Nama Satuan</label>
              <div class="col-sm-5">
                <input type="text" class="form-control" id="nm_satuan" name="nm_satuan" required>
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" id="btnSimpan" class="btn btn-primary">Simpan</button>
        </div>
        </form>
      </div>
    </div>
  </div>
  <!-- Modal Update -->
  <div class="modal fade" id="modalEdit" role="dialog">
    <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Edit Menu</h4>
        </div>
        <div class="modal-body">
          <form class="form-horizontal" action="<?php echo base_url('gudang/simpan_satuan_edit') ?>" method="post">
            <div class="form-group">
              <label class="control-label col-sm-3" for="kd_satuan_e">Kode Satuan</label>
              <div class="col-sm-5">
                <input type="text" class="form-control" id="kd_satuan_e" name="kd_satuan_e" readonly="readonly" required>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-3" for="nm_satuan_e">Nama Satuan</label>
              <div class="col-sm-5">
                <input type="text" class="form-control" id="nm_satuan_e" name="nm_satuan_e" required>
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" id="btnSimpan" class="btn btn-primary">Simpan</button>
        </div>
        </form>
      </div>
    </div>
  </div>
    <!-- Modal Hapus -->
  <div class="modal fade" id="modalHapus" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Hapus Satuan</h4>
        </div>
        <div class="modal-body">
          <form class="form-horizontal" action="<?php echo base_url('gudang/hapus_satuan') ?>" method="post">
           <h4>Apakah Kamu Yakin Menghapus Data Satuan Ini?</h4>
        </div>
        <input type="hidden" id="kd_satuan_h" name="kd_satuan_h">
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
          <button type="submit" id="btnHapus" class="btn btn-primary">Ya</button>
        </div>
        </form>
      </div>
    </div>
  </div>
     <!-- CONTENT-WRAPPER SECTION END-->
    <section class="footer-section no-print">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                   &copy; Copyright <?php echo date('Y') ?>, 
                </div>
            </div>
        </div>
    </section>
</div>
    <!-- FOOTER SECTION END-->
    <!-- JAVASCRIPT FILES PLACED AT THE BOTTOM TO REDUCE THE LOADING TIME  -->
    <script src="<?php echo base_url() ?>/assets/js/jquery-3.3.1.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/bootstrap.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/custom.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/sweetalert.min.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/toastr.min.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/jquery.price_format.min.js"></script>
    <!--script src="<?php echo base_url() ?>/assets/js/bootstrap-select.min.js"></script-->
    <script>
      $('#tbKategori').DataTable({
          "paging":   false,
          "ordering": false,
          "info": false,
      });
      $('form').attr('autocomplete', 'off');
     	$("ul.nav li.dropdown").hover(function(){
		$(this).find(".dropdown-menu").stop(!0,!0).delay(100).fadeIn(500)},
		function(){$(this).find(".dropdown-menu").stop(!0,!0).delay(100).fadeOut(500),
		$(this).find(".dropdown-submenu").stop(!0,!0).delay(100).fadeIn(500)},
		function(){$(this).find(".dropdown-submenu").stop(!0,!0).delay(100).fadeOut(500)
		});
      var pesan="<?php echo $this->session->flashdata('msg'); ?>";pesan&&(toastr.options={positionClass:"toast-top-right"},toastr.success(pesan));
	$("ul.nav li.dropdown-submenu").hover(function(){
		$(this).find(".dropdown-menu1").stop(!0,!0).delay(100).fadeIn(500)},
		function(){$(this).find(".dropdown-menu1").stop(!0,!0).delay(100).fadeOut(500)
	});
      function convertToRupiah(r){for(var e="",t=r.toString().split("").reverse().join(""),n=0;n<t.length;n++)n%3==0&&(e+=t.substr(n,3)+".");return e.split("",e.length-1).reverse().join("")};

      $(document).ready(function() {
		  $('.edit_record').on('click',function(){
              var kode= $(this).data('kode');
              var nama = $(this).data('nama');
              $('#modalEdit').modal('show');
				$('[name="kd_satuan_e"]').val(kode);
                $('[name="nm_satuan_e"]').val(nama);
          });
		  
		  $('.delete-record').on('click',function(){
                var kode = $(this).data('kode');
                $('#modalHapus').modal('show');
                $('[name="kd_satuan_h"]').val(kode);
            });



      });
    </script>

</body>
</html>