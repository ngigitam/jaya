<div class="content-wrapper">
  <div class="container">
    <div class="row pad-botm">
        <div class="col-md-12">
            <h4 class="header-line">Transfer Barang</h4>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
          <form class="form-horizontal" action="<?php echo base_url('gudang/simpan_transfer_barang') ?>" method="post">
            <div class="form-group">
              <label class="control-label col-sm-2" for="kd_bahan">Kode Barang</label>
              <div class="col-sm-2">
                <input type="text" class="form-control" id="kd_bahan" name="kd_bahan" required>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-2" for="nm_bahan">Nama Barang</label>
              <div class="col-sm-5">
                <input type="text" class="form-control" id="nm_bahan" name="nm_bahan" readonly="readonly">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-2" for="porsi_sekarang">Stok</label>
              <div class="col-sm-1">
                <input type="text" class="form-control" id="porsi_sekarang" name="porsi_sekarang" readonly="readonly">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-2" for="rusak">Jumlah Transfer Barang</label>
              <div class="col-sm-1">
                <input type="number" min="1" class="form-control" id="rusak" name="rusak" required>
              </div>
            </div>
			<div class="form-group">
              <label class="control-label col-sm-2" for="no_spbu_e">Dari Cabang</label>
              <div class="col-sm-3">
                <select name="spbu1" id="spbu1"  class="form-control" data-live-search="true" title="Pilih Unit" data-width="100%" required>
                        <option value="">-Pilih Cabang-</option>
						<?php 
						foreach ($unit->result_array() as $i) 
						{
                            $id=$i['no_spbu'];
                            $nama=$i['nm_cabang'];
                            $sess_id=$this->session->userdata('unit');
                            if($sess_id==$id )
							{
                                echo "<option value='$id' selected>$id - $nama</option>";
                            }else{
                                echo "<option value='$id'>$id - $nama</option>";
							}
						}?>
					
                </select>
              </div>
            </div>
			<div class="form-group">
              <label class="control-label col-sm-2" for="no_spbu_e">Ke Cabang</label>
              <div class="col-sm-3">
                <select name="spbu2" id="spbu2"  class="form-control" data-live-search="true" title="Pilih Unit" data-width="100%" required>
                        <option value="">-Pilih Cabang-</option>
						<?php 
						foreach ($unit->result_array() as $i) 
						{
                            $id=$i['no_spbu'];
                            $nama=$i['nm_cabang'];
                            $sess_id=$this->session->userdata('unit');
                            if($sess_id==$id )
							{
                                echo "<option value='$id' selected>$id - $nama</option>";
                            }else{
                                echo "<option value='$id'>$id - $nama</option>";
							}
						}?>
					
                </select>
              </div>
            </div>
            <!--div class="form-group">
              <label class="control-label col-sm-2" for="ket">Keterangan</label>
              <div class="col-sm-5">
                <input type="text" class="form-control" id="ket" name="ket" required>
              </div>
            </div-->
            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" onclick="return confirm('Apakah kamu yakin?')" class="btn btn-default">Simpan</button>
              </div>
            </div>
          </form>
        </div>
    </div>
  </div>
</div>
     <!-- CONTENT-WRAPPER SECTION END-->
    <section class="footer-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                   &copy; Copyright <?php echo date('Y') ?>, 
                </div>
            </div>
        </div>
    </section>
    <!-- FOOTER SECTION END-->
    <!-- JAVASCRIPT FILES PLACED AT THE BOTTOM TO REDUCE THE LOADING TIME  -->
    <script src="<?php echo base_url() ?>/assets/js/jquery-1.10.2.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/dataTables/jquery.dataTables.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/bootstrap.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/custom.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/sweetalert.min.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/toastr.min.js"></script>
    <script>
      $('form').attr('autocomplete', 'off');
     	$("ul.nav li.dropdown").hover(function(){
		$(this).find(".dropdown-menu").stop(!0,!0).delay(100).fadeIn(500)},
		function(){$(this).find(".dropdown-menu").stop(!0,!0).delay(100).fadeOut(500),
		$(this).find(".dropdown-submenu").stop(!0,!0).delay(100).fadeIn(500)},
		function(){$(this).find(".dropdown-submenu").stop(!0,!0).delay(100).fadeOut(500)
		});
      
	$("ul.nav li.dropdown-submenu").hover(function(){
		$(this).find(".dropdown-menu1").stop(!0,!0).delay(100).fadeIn(500)},
		function(){$(this).find(".dropdown-menu1").stop(!0,!0).delay(100).fadeOut(500)
	});
	
	 var pesan="<?php echo $this->session->flashdata('msg'); ?>",error="<?php echo $this->session->flashdata('error'); ?>";pesan?(toastr.options={positionClass:"toast-top-right"},toastr.success(pesan)):error&&swal(error);


      $(document).ready(function() {
          document.getElementById('kd_bahan').focus();
          $('#kd_bahan').on('input',function(){
          var kd_bahan=$('#kd_bahan').val();
              $.ajax({
                  type : "POST",
                  url  : "<?php echo base_url('gudang/get_detail_bahan') ?>",
                  dataType : "JSON",
                  data : {kd_bahan: kd_bahan},
                  cache: false,
                  success: function(data){
                      $.each(data,function(nm_barang){
                          $('[name="nm_bahan"]').val(data.nm_barang);
                          $('[name="porsi_sekarang"]').val(data.stok);
                      });
                  },
                  error: function(jqXHR, textStatus, errorThrown){
                      $('[name="nm_bahan"]').val("");
                      $('[name="porsi_sekarang"]').val("");
              }
              });
            return false;
            });
      });

    </script>

</body>
</html>