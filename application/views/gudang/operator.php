  <div class="content-wrapper" style="margin-bottom: 20px">
    <div class="container">
      <div class="row pad-botm">
          <div class="col-md-12">
              <h4 class="header-line">operator<span class="pull-right no-print"><a href="" data-toggle="modal" data-target="#myModal">Tambah operator</a></span></h4>
          </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <!--button type="button" class="btn btn-success no-print" onclick="window.print();return false;">Print</button-->
            <table id="tbOperator" class="table table-bordered table-striped table-responsive">
              <thead>
                <tr>
                  <th>ID operator</th>
                  <th>Nama operator</th>
				  <th>Alamat</th>
				  <th>Telepon</th>
                  <th class="no-print">Aksi</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($operator->result() as $key): ?>
				<?php $key->status ?>
                <tr>
                  <!--td><?php echo $no++ ?></td-->
                  <td><?php echo $key->id_operator ?></td>
                  <td><?php echo $key->nm_operator ?></td>
				  <td><?php echo $key->almt_operator ?></td>
				  <td><?php echo $key->tlp_operator ?></td>
				 <td align="center"><a href="javascript:void(0);" class="edit_record"  data-kode="<?php echo $key->id_operator; ?>" data-nama="<?php echo $key->nm_operator; ?>" data-alamat="<?php echo $key->almt_operator; ?>" data-tlp="<?php echo $key->tlp_operator; ?>" data-status="<?php echo $key->status; ?>">Edit</a> | <a href="javascript:void(0);" class="delete-record" data-kode="<?php echo $key->id_operator ?>">Hapus</a></td>
                </tr>
                <?php endforeach?>
              </tbody>
            </table>
        </div>
      </div>
    </div>
  </div>
  <!-- Modal Tambah -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Tambah operator</h4>
        </div>
        <div class="modal-body">
          <form class="form-horizontal" action="<?php echo base_url('gudang/simpan_operator') ?>" method="post">
            <div class="form-group">
              <label class="control-label col-sm-3" for="id_operator">ID operator</label>
              <div class="col-sm-5">
                <input type="text" class="form-control" id="id_operator" name="id_operator" required>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-3" for="nm_operator">Nama operator</label>
              <div class="col-sm-5">
                <input type="text" class="form-control" id="nm_operator" name="nm_operator" required>
              </div>
            </div>
			<div class="form-group">
              <label class="control-label col-sm-3" for="almt_operator">Alamat</label>
              <div class="col-sm-5">
                <input type="text" class="form-control" id="almt_operator" name="almt_operator" required>
              </div>
            </div>
			<div class="form-group">
              <label class="control-label col-sm-3" for="tlp_operator">Telepon</label>
              <div class="col-sm-5">
                <input type="text" class="form-control" id="tlp_operator" name="tlp_operator" required>
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit"  class="btn btn-primary">Simpan</button>
        </div>
        </form>
      </div>
    </div>
  </div>
  <!-- Modal Update -->
  <div class="modal fade" id="modalEdit" role="dialog">
    <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Edit Menu</h4>
        </div>
        <div class="modal-body">
          <form class="form-horizontal" action="<?php echo base_url('gudang/simpan_operator_edit') ?>" method="post">
            <div class="form-group">
              <label class="control-label col-sm-3" for="id_operator_e">ID operator</label>
              <div class="col-sm-5">
                <input type="text" class="form-control" id="id_operator_e" name="id_operator_e" readonly="readonly" required>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-3" for="nm_operator_e">Nama operator</label>
              <div class="col-sm-5">
                <input type="text" class="form-control" id="nm_operator_e" name="nm_operator_e" required>
              </div>
            </div>
			<div class="form-group">
              <label class="control-label col-sm-3" for="almt_operator_e">Alamat</label>
              <div class="col-sm-5">
                <input type="text" class="form-control" id="almt_operator_e" name="almt_operator_e" required>
              </div>
            </div>
			 <div class="form-group">
              <label class="control-label col-sm-3" for="tlp_operator_e">Telepon</label>
              <div class="col-sm-5">
                <input type="text" class="form-control" id="tlp_operator_e" name="tlp_operator_e" required>
              </div>
            </div>
			<div class="form-group">
                 <label class="control-label col-xs-3" >Status</label>
                  <div class="col-xs-9">
                    <select id="status_e" name="status_e" class="form-control" style="width:280px;" required >
                     <option value="0" <?php if(!empty($status) && $status == "Tidak Aktif") echo 'selected'; ?>>Tidak Aktif</option>
                     <option value="1" <?php if(!empty($status) && $status == "Aktif") echo 'selected'; ?>>Aktif</option>
                  </select>
              </div>
			</div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit"  class="btn btn-primary">Simpan</button>
        </div>
        </form>
      </div>
    </div>
  </div>
    <!-- Modal Hapus -->
  <div class="modal fade" id="modalHapus" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Hapus operator</h4>
        </div>
        <div class="modal-body">
          <form class="form-horizontal" action="<?php echo base_url('gudang/hapus_operator') ?>" method="post">
           <h4>Apakah Kamu Yakin Menghapus Data operator Ini?</h4>
        </div>
        <input type="hidden" id="id_operator_h" name="id_operator_h">
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
          <button type="submit" id="btnHapus" class="btn btn-primary">Ya</button>
        </div>
        </form>
      </div>
    </div>
  </div>
     <!-- CONTENT-WRAPPER SECTION END-->
    <section class="footer-section no-print">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                   &copy; Copyright <?php echo date('Y') ?>, 
                </div>
            </div>
        </div>
    </section>
</div>
    <!-- FOOTER SECTION END-->
    <!-- JAVASCRIPT FILES PLACED AT THE BOTTOM TO REDUCE THE LOADING TIME  -->
    <script src="<?php echo base_url() ?>/assets/js/jquery-3.3.1.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/bootstrap.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/custom.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/sweetalert.min.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/toastr.min.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/jquery.price_format.min.js"></script>
    <!--script src="<?php echo base_url() ?>/assets/js/bootstrap-select.min.js"></script-->
    <script>
      $('#tbOperator').DataTable({
          "paging":   false,
          "ordering": false,
          "info": false,
      });
      $('form').attr('autocomplete', 'off');
     	$("ul.nav li.dropdown").hover(function(){
		$(this).find(".dropdown-menu").stop(!0,!0).delay(100).fadeIn(500)},
		function(){$(this).find(".dropdown-menu").stop(!0,!0).delay(100).fadeOut(500),
		$(this).find(".dropdown-submenu").stop(!0,!0).delay(100).fadeIn(500)},
		function(){$(this).find(".dropdown-submenu").stop(!0,!0).delay(100).fadeOut(500)
		});
      var pesan="<?php echo $this->session->flashdata('msg'); ?>";pesan&&(toastr.options={positionClass:"toast-top-right"},toastr.success(pesan));
	$("ul.nav li.dropdown-submenu").hover(function(){
		$(this).find(".dropdown-menu1").stop(!0,!0).delay(100).fadeIn(500)},
		function(){$(this).find(".dropdown-menu1").stop(!0,!0).delay(100).fadeOut(500)
	});
      function convertToRupiah(r){for(var e="",t=r.toString().split("").reverse().join(""),n=0;n<t.length;n++)n%3==0&&(e+=t.substr(n,3)+".");return e.split("",e.length-1).reverse().join("")};

      $(document).ready(function() {
		  $('.edit_record').on('click',function(){
              var kode= $(this).data('kode');
              var nama = $(this).data('nama');
			  var alamat = $(this).data('alamat');
			  var tlp = $(this).data('tlp');
			  var status = $(this).data('status');
              $('#modalEdit').modal('show');
				$('[name="id_operator_e"]').val(kode);
                $('[name="nm_operator_e"]').val(nama);
				$('[name="almt_operator_e"]').val(alamat);
				$('[name="tlp_operator_e"]').val(tlp);
				$('[name="status_e"]').val(status);
          });
		  
		  $('.delete-record').on('click',function(){
                var kode = $(this).data('kode');
                $('#modalHapus').modal('show');
                $('[name="id_operator_h"]').val(kode);
            });



      });
    </script>

</body>
</html>