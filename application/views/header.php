<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="robots" content="noindex, nofollow">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="description" content="Aplikasi Manajemen" />
    <meta name="author" content="ferdy" />
    <!--link rel="icon" type="image/png" href="<?php echo base_url('assets/') ?>/img/favicon.png"/-->
    <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <title>JAYA NITROGEN</title>
    <link href="<?php echo base_url() ?>/assets/css/bootstrap.css" rel="stylesheet" />
    <link href="<?php echo base_url() ?>/assets/css/select.dataTables.min.css" rel="stylesheet" />
	<link href="<?php echo base_url() ?>/assets/css/font-awesome.css" rel="stylesheet" />
    <link href="<?php echo base_url() ?>/assets/css/style.css" rel="stylesheet" />
    <link href="<?php echo base_url() ?>/assets/css/toastr.min.css" rel="stylesheet" />
    <link href="<?php echo base_url() ?>/assets/css/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="<?php echo base_url() ?>/assets/css/buttons.dataTables.min.css" rel="stylesheet" />
    <link href="<?php echo base_url() ?>/assets/css/jquery-ui.css" rel="stylesheet" />
    <link href="<?php echo base_url() ?>/assets/css/bootstrap-select.min.css" rel="stylesheet" >
<style>
.dropdown-submenu {
  position: relative;
  
}
.dropdown-menu1 {
  position: absolute;
  top: 0 ;
  left: 100%;
  z-index: 1000;
  display: none;
  float: left;
  min-width: 160px;
  padding: 5px 0;
  margin: 2px 0 0;
  font-size: 14px;
  text-align: left;
  list-style: none;
  background-color: #fff;
  -webkit-background-clip: padding-box;
          background-clip: padding-box;
  border: 1px solid #ccc;
  border: 1px solid rgba(0, 0, 0, .15);
  border-radius: 4px;
  -webkit-box-shadow: 0 6px 12px rgba(0, 0, 0, .175);
          box-shadow: 0 6px 12px rgba(0, 0, 0, .175);
}

.dropdown-menu1 > li > a {
  display: block;
  padding: 1px 20px;
  clear: both;
  font-weight: normal;
  line-height: 1.42857143;
  color: #333;
  white-space: nowrap;
}
.dropdown-menu1 > li > a:hover,
.dropdown-menu1 > li > a:focus {
  color: #262626;
  text-decoration: none;
  background-color: #f5f5f5;
}
.dropdown-menu1 > .active > a,
.dropdown-menu1 > .active > a:hover,
.dropdown-menu1 > .active > a:focus {
  color: #fff;
  text-decoration: none;
  background-color: #428bca;
  outline: 0;
}
.dropdown-menu1 > .disabled > a,
.dropdown-menu1 > .disabled > a:hover,
.dropdown-menu1 > .disabled > a:focus {
  color: #777;
}
.dropdown-menu1 > .disabled > a:hover,
.dropdown-menu1 > .disabled > a:focus {
  text-decoration: none;
  cursor: not-allowed;
  background-color: transparent;
  background-image: none;
  filter: progid:DXImageTransform.Microsoft.gradient(enabled = false);
}
.open > .dropdown-menu1 {
  display: block;
}
.open > a {
  outline: 0;
}

.dropdown-submenu .dropdown-menu1 {
  top: 0;
  left: 100%;
  margin-top: -1px;
  
  
}
</style>
</head>
<body>
<?php
$query = $this->db->get('tabel_toko', 1, 0);
$toko = $query->row();
$namatoko = $toko->nm_toko;
?>
    <!--div class="navbar navbar-inverse set-radius-zero no-print">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="<?php echo base_url('dashboard/') ?>">
                    <!--img height="100px" src="<?php echo base_url() ?>/assets/img/lg.png" />
                </a>
				<br>
                <h2 style="display: inline; text-transform: uppercase;font-family: Arial,fantasy; margin-left: 10px; "><?php echo $namatoko ?></h2>
            </div>
        </div>
    </div-->
<?php
$hak = $this->session->userdata('akses');
?>
    <!-- LOGO HEADER END-->
	    <div class="navbar navbar-inverse set-radius-zero no-print">
        <div class="container">
		<div class="navbar-header set-radius-zero no-print">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                     <span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
				<a class="navbar-brand" href="<?php echo base_url().'dashboard'?>"><b>JAYA NITROGEN</b></a>
				</div></div></div>
				
    <section class="menu-section no-print">
        <div class="container">
			
            <div class="row ">
                <div class="col-md-12">
                    <div class="navbar-collapse collapse ">
                        <ul id="menu-top" class="nav navbar-nav navbar-left">
                            <!--li><a href="<?php echo base_url('dashboard/') ?>">DASHBOARD</a></li-->
                            <?php if ($hak == 'manajer'): ?>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" id="ddlmenuItem" data-toggle="dropdown">MASTER DATA<i class="fa fa-angle-down"></i></a>
                                <ul class="dropdown-menu" role="menu" aria-labelledby="ddlmenuItem">
								    <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo base_url('manajer/toko/') ?>">PROFIL</a></li>
                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo base_url('manajer/user/') ?>">USER</a></li>
									<li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo base_url('gudang/operator/') ?>">OPERATOR</a></li>
                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo base_url('gudang/unit/') ?>">CABANG</a></li>
									<li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo base_url('gudang/supplier/') ?>">SUPPLIER</a></li>
                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo base_url('gudang/sumber/') ?>">SUMBER DANA</a></li>
									<li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo base_url('bank') ?>">BANK</a></li>
									<li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo base_url('gudang/satuan/') ?>">SATUAN BARANG</a></li>
									<li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo base_url('gudang/barang/') ?>">BARANG</a></li>
                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo base_url('gudang/menu/') ?>">DAFTAR PENJUALAN BARANG / JASA</a></li>
                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo base_url('gudang/stok/') ?>">STOK BARANG</a></li>
								    <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo base_url('gudang/bahan-rusak/') ?>">BARANG RUSAK</a></li>
                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo base_url('gudang/stok-min/') ?>">BARANG MAU HABIS</a></li>
                                </ul>
                            </li>							
                            <?php endif?>
                            <?php if ($hak == 'admin' || $hak == 'manajer'): ?>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" id="ddlmenuItem" data-toggle="dropdown">TRANSAKSI<i class="fa fa-angle-down"></i></a>
                                <ul class="dropdown-menu" role="menu" aria-labelledby="ddlmenuItem">
                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo base_url('kasir/nomor-faktur/') ?>">PENJUALAN</a></li>
                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo base_url('setor') ?>">SETORAN PENJUALAN</a></li>
									<li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo base_url('kasir/kliring/') ?>">KLIRING SETORAN</a></li>
									<li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo base_url('kasir/input-biaya/') ?>">BIAYA</a></li>
									<li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo base_url('gudang/pembelian-start/') ?>">PEMBELIAN</a></li>
									<li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo base_url('bayar') ?>">PEMBAYARAN</a></li>
									<li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo base_url('gudang/kliring_bayar/') ?>">KLIRING PEMBAYARAN</a></li>
									<li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo base_url('gudang/transfer_barang/') ?>">TRANSFER BARANG</a></li>
                                   
								</ul>
                            </li>
                            <?php endif?>
                            <?php if ($hak == 'admin'): ?>
                            <!--li class="dropdown">
                                <a href="#" class="dropdown-toggle" id="ddlmenuItem" data-toggle="dropdown">MANAJER TOKO<i class="fa fa-angle-down"></i></a>
                                <ul class="dropdown-menu" role="menu" aria-labelledby="ddlmenuItem">
                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo base_url('gudang/stok/') ?>">STOK BARANG</a></li>
                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo base_url('manajer/kartu-stok/') ?>">KARTU STOK</a></li>
                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo base_url('gudang/stok-min/') ?>">BARANG MAU HABIS</a></li>
                                </ul>
                            </li-->
                            <!--li class="dropdown">
                                <a href="#" class="dropdown-toggle" id="ddlmenuItem" data-toggle="dropdown">GRAFIK<i class="fa fa-angle-down"></i></a>
                                <ul class="dropdown-menu" role="menu" aria-labelledby="ddlmenuItem">
                                    <li role="presentation"><a target="_blank" role="menuitem" tabindex="-1" href="<?php echo base_url('grafik/stok-barang/') ?>">GRAFIK PORSI BAHAN</a></li>
                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo base_url('grafik/profit-bulanan/') ?>">GRAFIK PROFIT BULANAN</a></li>
                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo base_url('grafik/penjualan-bulanan/') ?>">GRAFIK PENJUALAN BULANAN</a></li>
                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo base_url('grafik/penjualan-tahun/') ?>">GRAFIK PENJUALAN TAHUNAN</a></li>
                                </ul>
                            </li-->
							<?php endif?>
							<?php if ($hak == 'admin' || $hak == 'manajer' || $hak == 'investor'): ?>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" id="ddlmenuItem" data-toggle="dropdown">LAPORAN<i class="fa fa-angle-down"></i></a>
                                <ul class="dropdown-menu" role="menu" aria-labelledby="ddlmenuItem">
                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo base_url('laporan/biaya/') ?>">BIAYA-BIAYA</a></li>
                                    <!--li role="presentation"><a role="menuitem" tabindex="-1" target="_blank" href="<?php echo base_url('laporan/nilai-persediaan/') ?>">PERSEDIAAN BARANG</a></li-->
                                    <li role="presentation"><a role="menuitem" tabindex="-1" target="_blank" href="<?php echo base_url('laporan/pembelian/') ?>">PEMBELIAN</a></li>
									 <li role="presentation"><a role="menuitem" tabindex="-1" target="_blank" href="<?php echo base_url('laporan/hutang/') ?>">HUTANG</a></li>
                                    <!--li role="presentation"><a role="menuitem" tabindex="-1" target="_blank" href="<?php echo base_url('laporan/penjualan-transaksi/') ?>">PENJUALAN PER TRANSAKSI</a></li>
                                    <li role="presentation"><a role="menuitem" tabindex="-1" target="_blank" href="<?php echo base_url('laporan/penjualan-barang/') ?>">PENJUALAN PER MENU</a></li-->
									 <li class="dropdown-submenu">
									 <a href="#" class="test" tabindex="-1">PENJUALAN<span class="caret"></span></a>
									 <ul class="dropdown-menu1" >
										<li><a  tabindex="-1" target="_blank"  href="<?php echo base_url('laporan/penjualan/') ?>">PENJUALAN DETAIL</a></li>
										<li ><a  tabindex="-1" target="_blank" href="<?php echo base_url('laporan/history-penjualan/') ?>">HISTORY PENJUALAN</a></li>
										 <li ><a  tabindex="-1" href="<?php echo base_url('grafik/penjualan-bulanan/') ?>">GRAFIK PENJUALAN BULANAN</a></li>
										<li ><a tabindex="-1" href="<?php echo base_url('grafik/penjualan-tahun/') ?>">GRAFIK PENJUALAN TAHUNAN</a></li>
                                   
									</ul>
									</li>
									<li role="presentation"><a role="menuitem" tabindex="-1" target="_blank" href="<?php echo base_url('laporan/profit/') ?>">LABA RUGI</a></li>
									<li role="presentation"><a role="menuitem" tabindex="-1" target="_blank" href="<?php echo base_url('laporan/setoran-penjualan/') ?>">SETORAN PENJUALAN</a></li>
									<li role="presentation"><a role="menuitem" tabindex="-1" target="_blank" href="<?php echo base_url('laporan/operator/') ?>">OPERATOR</a></li>
									
                                   
									
								</ul>
                               
                            </li>
							<?php endif?>
                            
                            <li><a href="<?php echo base_url('login/logout/') ?>"><strong style="color: red">LOGOUT</strong></a></li>
                       
 <!--li class="dropdown">
 <a href="#" class="dropdown-toggle" id="ddlmenuItem" data-toggle="dropdown">LAPORAN<i class="fa fa-angle-down"></i></a>
                            
    <ul class="dropdown-menu">
      <li><a tabindex="-1" href="#">HTML</a></li>
      <li><a tabindex="-1" href="#">CSS</a></li>
	  
	                               
      <li class="dropdown-submenu">
	  <a href="#" class="dropdown-toggle" id="ddlmenuItem" data-toggle="dropdown">LAPORAN<i class="fa fa-angle-down"></i></a>
         <ul class="dropdown-menu1" role="menu" aria-labelledby="ddlmenuItem">
               <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo base_url('laporan/biaya/') ?>">BIAYA-BIAYA</a></li>
         </ul>                           
	  </li>
	  <li class="dropdown-submenu">
        <a class="test" tabindex="-1" href="#">New dropdown <span class="caret"></span></a>
        <ul class="dropdown-menu">
		                           
          <li><a tabindex="-1" href="#">2nd level dropdown</a></li>
          <li><a tabindex="-1" href="#">2nd level dropdown</a></li>
          <li class="dropdown-submenu">
            <a class="test" href="#">Another dropdown <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="#">3rd level dropdown</a></li>
              <li><a href="#">3rd level dropdown</a></li>
            </ul>
          </li>
        </ul>
      </li>
    </ul>
	 </li-->
  
						</ul>
                    </div>
                </div>
            </div>
        </div>
		
    </section>
	
	    <script>
/*	$("ul.nav li.dropdown").hover(function(){
		$(this).find(".dropdown-menu").stop(!0,!0).delay(100).fadeIn(500)},
		function(){$(this).find(".dropdown-menu").stop(!0,!0).delay(100).fadeOut(500),
		$(this).find(".dropdown-submenu").stop(!0,!0).delay(100).fadeIn(500)},
		function(){$(this).find(".dropdown-submenu").stop(!0,!0).delay(100).fadeOut(500)
		});
      var pesan="<?php echo $this->session->flashdata('msg'); ?>";pesan&&(toastr.options={positionClass:"toast-top-right"},toastr.success(pesan));
	$("ul.nav li.dropdown-submenu").hover(function(){
		$(this).find(".dropdown-menu1").stop(!0,!0).delay(100).fadeIn(500)},
		function(){$(this).find(".dropdown-menu1").stop(!0,!0).delay(100).fadeOut(500)
	});
	
*/
	  </script>